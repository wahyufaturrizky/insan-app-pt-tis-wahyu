import React from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import {widthPercentageToDP} from '../../utils';
import colors from '../../utils/colors';

function InputDate(props) {
  const {onChange, mode, format} = props;

  const defaultDate = moment().format(format);
  const [inputDate, setInputDate] = React.useState(defaultDate);
  const [visible, setVisible] = React.useState(false);

  function onPick(date) {
    const convert = moment(date).format(format);
    setInputDate(convert);
    setVisible(false);
    onChange(convert);
  }

  return (
    <View>
      <TouchableOpacity onPress={() => setVisible(true)}>
        <TextInput
          editable={false}
          placeholder="Please Select Date"
          value={inputDate}
          style={styles.input}
        />
      </TouchableOpacity>
      <DateTimePickerModal
        isVisible={visible}
        onConfirm={onPick}
        mode={mode}
        onCancel={() => setVisible(false)}
      />
    </View>
  );
}

InputDate.propTypes = {
  format: PropTypes.string,
  date: PropTypes.string,
};

InputDate.defaultProps = {
  format: 'YYYY-MM-DD',
  mode: 'date',
};

export default InputDate;

const styles = StyleSheet.create({
  input: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
});
