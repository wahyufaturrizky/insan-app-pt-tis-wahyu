import React from 'react';
import PropTypes from 'prop-types';
import {TextInput, Text, View, StyleSheet} from 'react-native';
import Color from '../../utils/colors';

function Input(props) {
  const {
    inputId,
    style,
    styleInput,
    styleLabel,
    label,
    disabled,
    innerRef,
    ...other
  } = props;

  const stylesInputDefault = disabled
    ? inputStyle.inputDisabled
    : inputStyle.inputActive;

  const stylesLabelInput = label
    ? inputStyle.inputLabel
    : inputStyle.inputLabelHidden;

  const createComposeStyle = StyleSheet.compose(stylesInputDefault, styleInput);
  const createComposeLabelStyle = StyleSheet.compose(
    stylesLabelInput,
    styleLabel,
  );

  return (
    <View style={style}>
      <Text style={createComposeLabelStyle}>{label}</Text>
      <TextInput
        nativeID={inputId}
        style={createComposeStyle}
        editable={!disabled}
        ref={innerRef}
        {...other}
      />
    </View>
  );
}

const inputStyle = StyleSheet.create({
  inputActive: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: Color.light_bluish_gray,
    borderRadius: 8,
    fontSize: 14,
    fontFamily: 'Roboto',
    fontWeight: '700',
    paddingHorizontal: 10,
  },
  inputDisabled: {
    backgroundColor: Color.light_bluish_gray,
    borderWidth: 1,
    borderColor: Color.light_bluish_gray,
    borderRadius: 8,
    fontSize: 14,
    fontFamily: 'Roboto',
    fontWeight: '700',
    paddingHorizontal: 10,
  },
  inputLabel: {
    color: '#000',
    // fontWeight: '700',
    fontFamily: 'Roboto sans',
    fontSize: 16,
    marginBottom: 10,
  },
  inputLabelHidden: {
    display: 'none',
  },
});

Input.propTypes = {
  inputId: PropTypes.number,
  style: PropTypes.object,
  styleLabel: PropTypes.object,
  styleInput: PropTypes.object,
  label: PropTypes.string,
  disabled: PropTypes.bool,
};

export default Input;
