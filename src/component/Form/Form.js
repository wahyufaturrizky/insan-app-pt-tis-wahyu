import React from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import Icon from "react-native-vector-icons/MaterialIcons";

export const LabelForm = (props) => {
  return (
    <Text
      style={props.style !== undefined ? [styles.labelStyle, props.style] : styles.labelStyle}
    >
        {props.value}
    </Text>
  );
}

export const InputForm = (props) => {
  return (
    <TextInput
      value={props.value}
      style={props.style 
        ? props.editable || props.editable === undefined
        ? [styles.inputStyle, props.style] 
        : [styles.inputStyle, styles.inputdisable, props.style]
        : props.editable 
        ? [styles.inputdisable, styles.inputStyle]
        : styles.inputStyle } autoCapitalize="none"
      placeholder= {props.placeholder}
      onChangeText={props.onChangeText}
      secureTextEntry={props.secureTextEntry}
      keyboardType={props.keyboardType ? props.keyboardType : "default"}
      editable={props.editable !== undefined ? props.editable : true}
    />
  );
}

export const Password = (props) => {
  return (
    <View style={styles.passwordContainer}>
      <TextInput
        value={props.value}
        style={styles.passwordStyle}
        autoCapitalize="none"
        placeholder= {props.placeholder}
        onChangeText={props.onChangeText}
        secureTextEntry={props.secureText}
      />
      <TouchableOpacity onPress={props.changeVisibility}>
        <Icon
          style={{paddingHorizontal: 10}}
          size={20}
          name={props.secureText ? "visibility" : "visibility-off"}
        />
      </TouchableOpacity>
    </View>
  );
}

export const TextArea  = (props) => {
  return (
    <TextInput value={props.value} style={props.style ? [styles.inputStyle, props.style] : styles.inputStyle} placeholder= {props.placeholder} onChangeText={props.onChangeText} multiline={true} numberOfLines={props.numberOfLines} />
  )
}

export const SAButton = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress} disabled={props.disabled ? props.disabled : false} >
      <View
      style={[
        styles.buttonStyle,
        props.backgroundColor ? {backgroundColor: props.backgroundColor} : {},
        props.style ? props.style : {},
        props.disabled ? styles.disableButton : {}
      ]}>
        <Text style={{fontFamily: "NunitoSans-SemiBold",color: "white", textAlign: "center", fontSize: props.fontSize? props.fontSize : 14}}>{props.title}</Text>
      </View>
    </TouchableOpacity>
  )
}

let styles = {
  labelStyle: {
    fontFamily: "NunitoSans-SemiBold",
    color: "white",
  },
  passwordContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 0,
    marginTop: widthPercentageToDP('1%'),
    marginBottom: widthPercentageToDP('2%'),
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  passwordStyle: {
    flex: 1,
    fontFamily: "NunitoSans-Regular",
    color: "#172344",
    backgroundColor: "white",
    paddingHorizontal: widthPercentageToDP('3%'),
    paddingVertical: widthPercentageToDP('2%'),
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
  },
  inputStyle: {
    fontFamily: "NunitoSans-Regular",
    color: "#172344",
    paddingHorizontal: widthPercentageToDP('3%'),
    paddingVertical: widthPercentageToDP('2%'),
    marginTop: widthPercentageToDP('1%'),
    marginBottom: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  inputdisable: {
    backgroundColor: "#D3D3D3"
  },
  buttonStyle: {
    justifyContent:"center",
    fontFamily: "NunitoSans-SemiBold",
    padding: widthPercentageToDP('3%'),
    marginVertical: widthPercentageToDP('3%'),
    backgroundColor: "#FB8B34",
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  disableButton: {
    backgroundColor: "#707070"
  }
}