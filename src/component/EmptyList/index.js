/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import PropTypes from 'prop-types';
import {View, Text} from 'react-native';
import {heightPercentageToDP, widthPercentageToDP} from '../../utils';
import Color from '../../utils/colors';

function EmptyContentList(props) {
  const {message} = props;
  return (
    <View
      style={{
        width: widthPercentageToDP('90%'),
        height: heightPercentageToDP('50%'),
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          color: Color.dope_blue,
          fontSize: widthPercentageToDP('5%'),
        }}>
        {message}
      </Text>
    </View>
  );
}

EmptyContentList.propTypes = {
  message: PropTypes.string,
};

export default EmptyContentList;
