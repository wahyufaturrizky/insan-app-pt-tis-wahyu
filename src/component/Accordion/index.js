import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
  UIManager,
  LayoutAnimation,
} from 'react-native';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';

import Color from '../../utils/colors';

function Accordion(props) {
  const {children, style, label} = props;
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }, []);

  function toggleExpand() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setVisible((prev) => !prev);
  }

  return (
    <View style={[styleAccordion.container, style]}>
      <TouchableOpacity
        style={styleAccordion.button}
        onPress={() => toggleExpand()}>
        <Text style={styleAccordion.title}>{label}</Text>
        <IconMaterial
          name="keyboard-arrow-down"
          color={Color.strong_grey}
          size={25}
          style={styleAccordion.iconStyle(visible)}
        />
      </TouchableOpacity>
      {visible && (
        <View style={styleAccordion.contentContainer(visible)}>
          {children}
        </View>
      )}
    </View>
  );
}

const styleAccordion = StyleSheet.create({
  container: {
    borderColor: Color.light_bluish_gray,
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderWidth: 1,
    borderRadius: 6,
  },
  contentContainer: (visible) => ({
    marginTop: 10,
  }),
  button: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontFamily: 'Roboto',
    fontWeight: '600',
  },
  iconStyle: (isRotate) => ({
    transform: [{rotate: isRotate ? '180deg' : '0deg'}],
  }),
});

Accordion.propTypes = {
  children: PropTypes.node,
  label: PropTypes.string,
};

export default Accordion;
