import React from 'react';
import { View, Text, Switch as SwitchRN, StyleSheet } from 'react-native';
import { heightPercentageToDP } from "../../utils";

function Switch(props) {
  const { label, onChange, value, style } = props;

  return (
    <View style={[styles.input, style]}>
      <Text style={styles.label}>{label}</Text>
      <SwitchRN onValueChange={onChange} value={value} />
    </View>
  );
}

export default Switch;

const styles = StyleSheet.create({
  input: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: heightPercentageToDP('1%'),
  },
  label: {
    fontSize: 16,
    fontWeight: '500',
  },
});