import React from 'react';
import {ToastAndroid} from 'react-native';

function Snackbar(message, time, position) {
  return ToastAndroid.showWithGravity(message, time, position);
}

export default Snackbar;
