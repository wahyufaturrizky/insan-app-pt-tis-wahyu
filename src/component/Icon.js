import React from 'react';
import {Image} from 'react-native';

const Icon = ({name, size, color}) => {
  const icons = {
    pencil: require('../assets/image/create.png'),
    eye: require('../assets/image/visibility.png'),
  };
  return (
    <Image
      source={icons[name]}
      resizeMode="contain"
      style={{width: size ?? 25, height: size ?? 25}}
    />
  );
};

export default Icon;
