import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import { connect } from "react-redux";
import { updateAppraisalData } from "../../actions/appraisal_actions";
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import AccidentButton from "./AccidentButton";
import LevelButton from "./LevelButton";
import colors from "../../utils/colors";

class InputFrame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataDoc: {},
    };
    this.selectLevel = this.selectLevel.bind(this);
  }

  selectAccident(value) {
    let data = this.props.photo;
    switch (value) {
      case "dent":
        data.dent === "Yes" ? (data.dent = "No") : (data.dent = "Yes");
        data.repaired !== "Disabled" ? (data.repaired = "No") : (data.repaired = "Disabled");
        data.replaced = "No";
        break;
      case "repaired":
        data.repaired === "Yes" ? (data.repaired = "No") : (data.repaired = "Yes");
        data.dent = "No";
        data.replaced = "No";
        break;
      case "replaced":
        console.log("masuk");
        data.replaced === "Yes" ? (data.replaced = "No") : (data.replaced = "Yes");
        data.dent !== "Disabled" ? (data.dent = "No") : (data.dent = "Disabled");
        data.repaired !== "Disabled" ? (data.repaired = "No") : (data.repaired = "Disabled");
        break;
      default:
        data;
    }
    this.props.updateAppraisalData("photoFrame", this.props.index, data);
  }

  selectClamp() {
    let data = this.props.photo;
    if (data.clamp === "No") {
      data.clamp = "Yes";
    } else {
      data.clamp = "No";
    }
    this.props.updateAppraisalData("photoFrame", this.props.index, data);
  }

  selectRust() {
    let data = this.props.photo;
    if (data.rust === "No") {
      data.rust = "Yes";
      data.replaced = "Disabled";
    } else {
      data.rust = "No";
      data.replaced = "No";
    }

    if (data.level === "S") {
      data.level = "";
    }
    this.props.updateAppraisalData("photoFrame", this.props.index, data);
  }

  selectLevel(value) {
    const { photo } = this.props;
    photo.level === value ? (photo.level = "") : (photo.level = value);
    this.props.updateAppraisalData("photoFrame", this.props.index, photo);
  }

  componentDidMount() {}

  render() {
    const { photo, onPressPhoto } = this.props;
    return (
      <View style={[styles.box, !photo.type ? styles.background : null]}>
        <View style={[styles.inputContainer, ]}>
        
          <View style={styles.photoContainer}>
            <Text style={styles.photoText}>{photo.name}</Text>
          </View>

          {!photo.type ? (
            <ScrollView horizontal={true} nestedScrollEnabled={true}>
              <View style={styles.frameButton}>
                <TouchableOpacity
                  onPress={() => this.selectAccident("dent")}
                  disabled={photo.dent === "Disabled"}
                  style={[
                    styles.iconContainer,
                    photo.dent === "Disabled" ? styles.iconDisabled : styles.iconActive,
                    photo.dent === "Yes" ? styles.iconSelected : {},
                  ]}
                >
                  <Text style={styles.buttonText}>Dent</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.selectAccident("repaired")}
                  disabled={photo.repaired === "Disabled"}
                  style={[
                    styles.iconContainer,
                    photo.repaired === "Disabled" ? styles.iconDisabled : styles.iconActive,
                    photo.repaired === "Yes" ? styles.iconSelected : {},
                  ]}
                >
                  <Text style={styles.buttonText}>Repaired</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => this.selectAccident("replaced")}
                  disabled={photo.replaced === "Disabled"}
                  style={[
                    styles.iconContainer,
                    photo.replaced === "Disabled" ? styles.iconDisabled : styles.iconActive,
                    photo.replaced === "Yes" ? styles.iconSelected : {},
                  ]}
                >
                  <Text style={styles.buttonText}>Replaced</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  disabled={photo.clamp === "Disabled"}
                  onPress={() => this.selectClamp()}
                  style={[
                    styles.iconContainer,
                    photo.clamp === "Disabled" ? styles.iconDisabled : styles.iconActive,
                    photo.clamp === "Yes" ? styles.iconSelected : {},
                  ]}
                >
                  <Text style={styles.buttonText}>Clamp</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  disabled={photo.rust === "Disabled"}
                  onPress={() => this.selectRust()}
                  style={[
                    styles.iconContainer,
                    photo.rust === "Disabled" ? styles.iconDisabled : styles.iconActive,
                    photo.rust === "Yes" ? styles.iconSelected : {},
                  ]}
                >
                  <Text style={styles.buttonText}>Rust</Text>
                </TouchableOpacity>

                <LevelButton level={photo.level} rust={photo.rust} selectLevel={this.selectLevel} />

                <TouchableOpacity
                  onPress={() => onPressPhoto(photo.name)}
                  style={[styles.iconContainer, photo.imageUrl !== "" ? styles.iconSelected : styles.iconActive]}
                >
                  <Icon name="camera" size={widthPercentageToDP('6.5%')} color="white" />
                </TouchableOpacity>
              </View>
            </ScrollView>
            
          ) : null}
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create( {
  inputContainer: {
    flexDirection: 'column',
    justifyContent: "center",
    borderColor: colors.light_sapphire_bluish_gray,
    borderBottomWidth: 2,
    paddingVertical: widthPercentageToDP('4%')
  },
  box: {
    paddingLeft: widthPercentageToDP("5%"),    
  },
  background: {
    backgroundColor: 'white'
  },
  photoContainer: {
    flex: 1,
    justifyContent: "center",
  },
  iconContainer: {
    width: widthPercentageToDP('12.4%'),
    height: widthPercentageToDP('12.4%'),
    alignItems: "flex-start",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "white",
    borderWidth: 1,
    borderRadius: 5
  },
  iconDisabled: {
    backgroundColor: colors.very_soft_blue,
  },
  iconActive: {
    backgroundColor: colors.cyan_blue,
  },
  iconSelected: {
    backgroundColor: colors.cyan_green,
  },
  photoText: {
    // fontWeight: "600",
    fontSize: widthPercentageToDP('4.5%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue
  },
  frameButton: {
    flexDirection: "row",
    marginTop: widthPercentageToDP('2%')
  },
  buttonText: {
    textAlign: "center",
    color: "white",
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('3.7%')
  },
});

const mapDispatchToProps = dispatch => {
  return {
    updateAppraisalData: (partName, index, data) => {
      dispatch(updateAppraisalData(partName, index, data));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(InputFrame);
