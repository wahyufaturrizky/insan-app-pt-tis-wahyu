import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';

import Color from '../../utils/colors';

function Button(props) {
  const {children, style, disabled, icon, color, ...other} = props;
  const stylesColorButton = {
    primary: {
      active: stylesButton.buttonPrimary,
      disabled: stylesButton.buttonPrimaryDisabled,
    },
    secondary: {
      active: stylesButton.buttonSecondary,
      disabled: stylesButton.buttonSecondaryDisabled,
    },
  };

  const selectColorButton = stylesColorButton[color];
  const applyColorButton = disabled
    ? selectColorButton.disabled
    : selectColorButton.active;

  const buttonComposeStyle = StyleSheet.compose(applyColorButton, style);

  return (
    <TouchableOpacity style={buttonComposeStyle} disabled={disabled} {...other}>
      {icon}
      <Text style={stylesButton.textButton}>{children}</Text>
    </TouchableOpacity>
  );
}

const stylesButton = StyleSheet.create({
  buttonPrimary: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 50,
    paddingVertical: 15,
    borderRadius: 5,
    backgroundColor: Color.strong_blue,
  },
  buttonPrimaryDisabled: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 50,
    paddingVertical: 15,
    borderRadius: 5,
    backgroundColor: Color.very_soft_blue,
  },
  buttonSecondary: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 50,
    paddingVertical: 15,
    borderRadius: 5,
    backgroundColor: Color.bright_red,
  },
  buttonSecondaryDisabled: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 50,
    paddingVertical: 15,
    borderRadius: 5,
    backgroundColor: Color.very_soft_red,
  },
  textButton: {
    fontSize: 15,
    fontWeight: '700',
    fontFamily: 'Roboto',
    color: '#fff',
  },
});

Button.propTypes = {
  children: PropTypes.object,
  color: PropTypes.oneOf(['primary', 'secondary']),
};

Button.defaultProps = {
  color: 'primary',
  disabled: false,
};

export default Button;
