import React, { Component } from "react";
import Modal from "react-native-modal";
import _ from 'lodash'
import { View, Text, Button, TouchableOpacity, TextInput, Image, ScrollView, Dimensions, StyleSheet } from "react-native";
import {connect} from 'react-redux'
import Icon from "react-native-vector-icons/dist/FontAwesome";
import ImagePicker from "react-native-image-picker";
import { widthPercentageToDP, heightPercentageToDP } from "../../utils";
import colors from '../../utils/colors'

class UploadModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _takePicture(field) {

    const options = {
      title: `Pilih ${field}`,
      storageOptions: {
        path: "images",
      },
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("Pick image cancelled");
      } else if (response.error) {
        console.log("Error pick image", response.error);
      } else {
        // if(this.state[field]) {
        //   let temp = {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName,
        //   }

        //   let tempField = this.state[field]
        //   tempField.push(temp)

        //   this.setState({
        //     [field]: tempField
        //   });

        // } else {
          this.setState({
            [field]: {
              uri: response.uri,
              type: "image/jpeg",
              name: response.fileName,
              path: response.path
            }
          });
        // }

      }
    });
  }

  handleOnScroll = event => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };

  handleScrollTo = p => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  checkingImage = () => {

    let { document, AppraisalData } = this.props
    let data = _.find(AppraisalData.data, {objectField: {name: document.name}})
    // console.log("--",document)
    if ( this.state[document.name] ) {
      return <Image source={{ uri: this.state[document.name].uri}} style={styles.modalImagePicker} />
    } else if (data){
      return <Image source={{ uri: data.image.uri}} style={styles.modalImagePicker} />
    } else if (document.imageUrl) {
      if ( typeof(document.imageUrl) === 'string' && document.imageUrl !== "" ) {
        return <Image source={{ uri: document.imageUrl}} style={styles.modalImagePicker}/>
      } else if (Array.isArray(document.imageUrl)) {
        if (document.imageUrl.length > 0) {
          return  document.imageUrl.map((image, index) => <Image source={{ uri: image}} style={styles.modalImagePicker} key={index}/>)
        } else {
          return null
        }
      } else {
        return null
      }
    } else {
      return null
    }
  }

  render() {
    const { displayPhotoModal, togglePhotoModal, onChangeText, onPressUpload, document, previewPhoto, AppraisalData } = this.props;

    let images = document.imageUrl
    let data = _.find(AppraisalData.data, {objectField: {name: document.name}})

    // console.log("aa", data)
    return (
      <Modal
        supportedOrientations={["portrait", "landscape"]}
        onBackdropPress={() => togglePhotoModal()}
        isVisible={displayPhotoModal}
        style={styles.photoModal}
        onModalShow={() => console.log('ini',document)}
      >
        <View style={Dimensions.get("window").height <= 360 ? styles.scrollableModal : {}}>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => togglePhotoModal()}>
                <Text style={{ color: "white", fontSize: widthPercentageToDP('8%'), fontFamily: 'Roboto' }}>x</Text>
              </TouchableOpacity>
            </View>
            <ScrollView ref={ref => (this.scrollViewRef = ref)} onScroll={this.handleOnScroll} scrollEventThrottle={16}>
              <View style={styles.modalContent}>
                <Text style={styles.modalPhotoName}>{document.name}</Text>

                <ScrollView horizontal={true} nestedScrollEnabled={true}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', width: '100%', height: heightPercentageToDP("23%")}}>
                      {
                        this.checkingImage()
                      }
                    </View>
                </ScrollView>

                <TextInput
                  onChangeText={text => onChangeText(text, document.name)}
                  editable={false}
                  multiline={true}
                  numberOfLines={5}
                  style={styles.modalNotesInput}
                  value={document.notes}
                  placeholderTextColor={colors.light_grayish_blue}
                />

                <View style={{ flexDirection: "row", justifyContent: "space-around", marginTop: 20 }}>
                  <TouchableOpacity
                    style={[styles.viewPhotoButton, document.imageUrl ? (document.imageUrl.length > 0 ? styles.viewPhotoButtonActive: styles.viewPhotoButtonInactive) : styles.viewPhotoButtonInactive]}
                    disabled={ document.imageUrl ? (document.imageUrl.length === 0 ? true : false) : false }
                    onPress={() => previewPhoto(images)}
                  >
                    <Text style={styles.buttonText} >Lihat Photo</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create ({
  flex1: {
    flex: 1,
  },
  containerModal: {
    flex: 1,
  },
  modalContainer: {
    // height: heightPercentageToDP("50%"),

  },
  modalHeader: {
    height: heightPercentageToDP("7%"),
    backgroundColor: "transparent",
    justifyContent: "flex-end",
    alignItems: 'flex-end',
    padding: 5,
  },
  modalContent: {
    paddingHorizontal: widthPercentageToDP("5%"),
    paddingVertical: heightPercentageToDP("1.5%"),
    backgroundColor: "#fff",
    borderRadius: 6
  },
  modalPhotoName: {
    fontSize: widthPercentageToDP('6%'),
    fontFamily: 'Roboto',
    color: colors.dope_blue
  },
  modalImagePicker: {
    height: widthPercentageToDP("25.3%"),
    width: widthPercentageToDP('25.3%'),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#778899",
    marginRight:widthPercentageToDP('2%'),
    borderRadius: 6,
  },
  modalNotesInput: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    textAlignVertical: "top",
    fontSize: widthPercentageToDP('4.5%'),
    borderRadius: 5,
  },
  scrollableModal: {
    height: 250,
  },
  photoModal: {
    paddingBottom: 20,
  },
  scrollableModalContent1: {
    height: 200,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
  },
  scrollableModalContent2: {
    height: 200,
    backgroundColor: "lightgreen",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Roboto',
    fontSize: widthPercentageToDP('4%'),
    fontWeight: 'bold'
  },
  viewPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    backgroundColor: colors.bright_red,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%')
  },
  viewPhotoButtonActive: {
    backgroundColor: colors.bright_red
  },
  viewPhotoButtonInactive: {
    backgroundColor: colors.very_soft_red
  },
  uploadPhotoButton: {
    width: widthPercentageToDP('37.5%'),
    borderRadius: 5,
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('3%')
  },
  uploadPhotoButtonActive: {
    backgroundColor: colors.strong_blue,
  },
  uploadPhotoButtonInactive: {
    backgroundColor: colors.very_soft_blue,
  }
});

const mapStateToProps = ({ AppraisalData }) => ({ AppraisalData });

export default connect(mapStateToProps)(UploadModal);
