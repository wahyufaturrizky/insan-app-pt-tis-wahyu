import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';

export default class TicketInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const {onChangeText, dataTicketDetail, name} = this.props;
    // console.log('dataTicketDetailasd', dataTicketDetail);
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>CLEANING INFO</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>Cleaning ID</Text>
            <TextInput
              value={`${dataTicketDetail.id}`}
              editable={false}
              keyboardType="numeric"
              onChangeText={(text) => onChangeText(text, 'ticket_id')}
              style={styles.inputStyleReadOnly}
            />

            <Text style={styles.label}>Cleaning Date</Text>
            <View style={styles.inputForm}>
              <DatePicker
                disabled
                date={dataTicketDetail.cleaning_date}
                style={{width: '100%'}}
                locale="id"
                mode="date"
                androidMode="spinner"
                placeholder="Pilih tanggal"
                format="YYYY-MM-DD HH:mm:ss"
                confirmBtnText="Oke"
                cancelBtnText="Batal"
                minuteInterval={15}
                onDateChange={(date) => {
                  onChangeText(date, 'open_date');
                }}
                // onDateChange={(date) => {
                //   moment(date, 'dddd, DD-MM-YYYY HH:mm');
                //   this.setState({handover});
                // }}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                    backgroundColor: colors.light_bluish_gray,
                  },
                  placeholderText: {
                    color: '#CAD9E4',
                  },
                  dateText: {
                    color: '#172344',
                    fontSize: 14,
                  },
                }}
              />
            </View>

            <Text style={styles.label}>Staff</Text>
            <TextInput
              editable={false}
              value={dataTicketDetail.user.name}
              placeholder="please input your name"
              keyboardType="numeric"
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'ticket_id')}
              style={styles.inputStyleReadOnly}
            />
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  modalNotesInput: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    textAlignVertical: 'top',
    fontSize: widthPercentageToDP('4.5%'),
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
});
