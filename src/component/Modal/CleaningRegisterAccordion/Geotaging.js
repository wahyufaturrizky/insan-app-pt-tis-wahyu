/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  SafeAreaView,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {WebView} from 'react-native-webview';
import html_script from '../../../utils/html_script';
import Geolocation from '@react-native-community/geolocation';
import {getDistance} from 'geolib';

export default class Geotaging extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
      initialPosition: '',
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentDidMount() {
    Geolocation.getCurrentPosition(
      (info) => {
        console.log('getCurrentPosition', info);
        this.setState({
          initialPosition: info,
        });
      },
      (error) => {
        console.error(error.response);
      },
      {
        enableHighAccuracy: true,
        distanceFilter: 1,
        timeout: 20000,
        maximumAge: 1000,
      },
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const {expanded} = this.state;
    if (expanded !== prevState.expanded) {
      Geolocation.getCurrentPosition(
        (info) => {
          this.setState({
            initialPosition: info,
          });
        },
        (error) => {
          console.error(error.response);
        },
        {
          enableHighAccuracy: true,
          distanceFilter: 1,
          // timeout: 20000,
          // maximumAge: 1000,
        },
      );
      // Geolocation.watchPosition(
      //   (info) => {
      //     console.log('watchPosition', info);
      //   },
      //   // (error) => alert(JSON.stringify(error)),
      //   {
      //     enableHighAccuracy: true,
      //     distanceFilter: 1,
      //     // timeout: 20000,
      //     // maximumAge: 1000,
      //   },
      // );
    }
  }
  
  async _getCurrentLocation() {
    const {initialPosition} = this.state;
	  await Geolocation.getCurrentPosition(
		(info) => {
			this.setState({
				initialPosition: info,
			});
		}, (error) => {
			console.error(error);
		},
		{
			enableHighAccuracy: true,
			distanceFilter: 1,
		}
	  )
	  
	  await this._goToMyPosition(
				initialPosition?.coords?.latitude,
				initialPosition?.coords?.longitude,
			);
  }

  _goToMyPosition = async (lat, lon) => {
    const {data_mesin_cleaning, captureDistance} = this.props;

    let latCheck = await lat;
    let lonCheck = await lon;

    let data_mesin_cleaning_latCheck = await data_mesin_cleaning.latitude;
    let data_mesin_cleaning_lonCheck = await data_mesin_cleaning.longitude;

    if (data_mesin_cleaning === '') {
      alert('Harap isi terlebih dahulu data cleaning machine');
    } else {
      this.refs.Map_Ref.injectJavaScript(`
      mymap.setView([${latCheck}, ${lonCheck}], 25)
      L.marker([${latCheck}, ${lonCheck}]).addTo(mymap)
    `);

      let dis = await getDistance(
        // lokasi user
        {latitude: latCheck, longitude: lonCheck},
        // lokasi ATM
        {
          latitude: data_mesin_cleaning_latCheck,
          longitude: data_mesin_cleaning_lonCheck,
        },
      );

	  const messageText = `Jarak anda dengan Machine adalah ${dis} Meter\n\nLokasi Machine\n\nLatitude\n${data_mesin_cleaning_latCheck}\nLongitude\n${data_mesin_cleaning_lonCheck}\n\nLokasi User\n\nLatitude\nlatCheck}\nLongitude\n${lonCheck}\n\n*jarak anda harus d bawah 50 Meter dengan Machine untuk bisa submit data`;
	  alert(messageText);

      captureDistance(dis);
    }
  };

  render() {
    const {onChangeText} = this.props;
    const {initialPosition} = this.state;
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>GEO-TAGGING</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>PM</Text>
            <SafeAreaView
              style={{
                marginTop: widthPercentageToDP('4%'),
                width: widthPercentageToDP('70%'),
                height: widthPercentageToDP('70%'),
                padding: 6,
                backgroundColor: colors.strong_grey,
              }}>
              {/* {console.log('html_script', html_script)} */}
              <WebView ref={'Map_Ref'} source={{html: html_script}} />
            </SafeAreaView>
            <TouchableOpacity
              onPress={() => this._getCurrentLocation()}
              style={styles.findLocationATMButton}>
              <Text style={styles.logoutTextFindLocationButton}>
                Update Location
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
  inputFormPicker: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  logoutTextFindLocationButton: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  findLocationATMButton: {
    backgroundColor: colors.strong_blue,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: colors.strong_blue,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
  },
});
