import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
  Picker,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DatePicker from 'react-native-datepicker';

export default class CleanedMechine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
      expanded: false,
    };

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  render() {
    const {
      onChangeText,
      dataTicketDetail,
      bank_id,
      bank_names,
      bank_branchs,
      bank_branch_id,
      bank_machine_types,
      bank_machine_type_id,
      bank_machine_sn,
      bank_machine_sn_name,
      data_mechine_id,
      bank_machine_sn_details_address,
    } = this.props;
    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this.toggleExpand()}>
          <Text style={[styles.title, styles.font]}>CLEANED MACHINE</Text>
          <Icon
            name={
              this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'
            }
            size={30}
            color={colors.strong_grey}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <Text style={styles.label}>Bank</Text>
            <View style={styles.inputFormPicker}>
              <Picker
                selectedValue={bank_id}
                mode="dropdown"
                onValueChange={(text) => onChangeText(text, 'bank_id')}>
                <Picker.Item label="Choose Bank" value="" />
                {bank_names.length > 0
                  ? bank_names.map((value, idx) =>
                      value.map((data, index) => (
                        <Picker.Item
                          key={data.id}
                          label={data.name_bank}
                          value={data.id}
                        />
                      )),
                    )
                  : null}
              </Picker>
            </View>
            <Text style={styles.label}>Branch</Text>
            <View style={styles.inputFormPicker}>
              <Picker
                selectedValue={bank_branch_id}
                mode="dropdown"
                onValueChange={(text) => onChangeText(text, 'bank_branch_id')}>
                <Picker.Item label="Choose Branch" value="" />

                {bank_branchs.length > 0
                  ? bank_branchs.map((value, idx) =>
                      value.map((data, index) => (
                        <Picker.Item
                          key={data.id}
                          label={data.branch.name_branch}
                          value={data}
                        />
                      )),
                    )
                  : null}
              </Picker>
            </View>
            <Text style={styles.label}>Type Machine</Text>
            <View style={styles.inputFormPicker}>
              <Picker
                selectedValue={bank_machine_type_id}
                mode="dropdown"
                onValueChange={(text) =>
                  onChangeText(text, 'bank_machine_type_id')
                }>
                <Picker.Item label="Choose Type Machine" value="" />

                {bank_machine_types.length > 0
                  ? bank_machine_types.map((value, idx) =>
                      value.map((data, index) => (
                        <Picker.Item
                          key={data.type.id}
                          label={data.type.name_type}
                          value={data}
                        />
                      )),
                    )
                  : null}
              </Picker>
            </View>
            <Text style={styles.label}>Machine SN</Text>
            <View style={styles.inputFormPicker}>
              <Picker
                selectedValue={bank_machine_sn_name}
                mode="dropdown"
                onValueChange={(text) =>
                  onChangeText(text, 'bank_machine_sn_name')
                }>
                <Picker.Item label="Choose Machine SN" value="" />

                {bank_machine_sn.length > 0
                  ? bank_machine_sn.map((value, idx) =>
                      value.map((data, index) => (
                        <Picker.Item
                          key={data.type.id}
                          label={data.machine_sn}
                          value={data}
                        />
                      )),
                    )
                  : null}
              </Picker>
            </View>

            <Text style={styles.label}>Machine ID</Text>
            <TextInput
              editable={false}
              value={
                data_mechine_id === undefined
                  ? 'please input your name'
                  : data_mechine_id
              }
              placeholder={
                data_mechine_id === undefined ? 'please input your name' : ''
              }
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'ticket_id')}
              style={styles.inputStyleReadOnly}
            />
            <Text style={styles.label}>Location</Text>
            <TextInput
              editable={false}
              value={bank_machine_sn_details_address}
              placeholder="please input your name"
              keyboardType="numeric"
              autoCapitalize="none"
              onChangeText={(text) => onChangeText(text, 'ticket_id')}
              style={styles.inputStyleReadOnly}
            />
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.setState({expanded: !this.state.expanded});
  };
}

const styles = StyleSheet.create({
  inputFormPicker: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
});
