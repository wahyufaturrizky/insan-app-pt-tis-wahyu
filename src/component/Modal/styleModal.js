import {StyleSheet} from 'react-native';
import Color from '../../utils/colors';

const styleModal = StyleSheet.create({
  modalContainer: {
    paddingHorizontal: 20,
    marginTop: 10,
    paddingBottom: 20,
    marginBottom: 10,
  },
  titleModal: {
    fontFamily: 'Roboto',
    fontWeight: '700',
    fontSize: 26,
  },
  modalHeader: {
    paddingHorizontal: 20,
  },
  modalStrip: {
    width: 100,
    height: 10,
    backgroundColor: Color.light_bluish_gray,
    borderRadius: 10,
    marginVertical: 20,
    alignSelf: 'center',
  },
  modalFooter: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: 20,
  },
  spaceTop10: {
    marginTop: 10,
  },
});

export default styleModal;
