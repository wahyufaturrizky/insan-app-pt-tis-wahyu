/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-alert */
import React, { Component } from 'react';
import Modal from 'react-native-modal';
import _ from 'lodash';
import moment from 'moment';
import {
  View,
  Text,
  Platform,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  StyleSheet,
  AsyncStorage,
  UIManager,
} from 'react-native';
import Axios from 'axios';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';
import { widthPercentageToDP, heightPercentageToDP } from '../../utils';
import colors from '../../utils/colors';
import Spinner from 'react-native-loading-spinner-overlay';
import TicketInfo from './TicketWorkPendingAccordion/TicketInfo';
import ProblemInfo from './TicketWorkPendingAccordion/ProblemInfo';
import EngineerInfo from './TicketWorkPendingAccordion/EngineerInfo';
import AtmEngineerInfo from './TicketWorkPendingAccordion/AtmEngineerInfo';
import TimeStamp from './TicketWorkPendingAccordion/TimeStamp';
import HealthCheck from './TicketWorkPendingAccordion/HealthCheck';
import ProblemAndAction from './TicketWorkPendingAccordion/ProblemAndAction';
import Miscellaneous from './TicketWorkPendingAccordion/Miscellaneous';
import Geotaging from './TicketWorkPendingAccordion/Geotaging';
import { API_URL } from 'react-native-dotenv';
import ReplacementPart from './TicketWorkPendingAccordion/ReplacementPart';
import checkFields from '../../utils/checkFields';
import styles from './styles';

class UploadModalTicketPending extends Component {
  constructor(props) {
    super(props);
    this.onChangeText = this.onChangeText.bind(this);
    this._onChangeReplacePart = this._onChangeReplacePart.bind(this);
    this.state = {
      name: '',
      email: '',
      no_telp: '',
      updated_at: '',
      username: '',
      api_token: '',
      img_user: '',
      image_problem: '',
      password: '',
      kelanjutan_problem: '',
      grounding: '',
      it: '',
      voltage: '',
      ac: '',
      ups: '',
      notes: '',
      cm: '',
      pm: '',
      charge: '',
      action: '',
      work_start: moment().format('YYYY-MM-DD HH:mm:ss'),
      work_finish: moment().format('YYYY-MM-DD HH:mm:ss'),
      spinner: false,
      expanded: false,
      replacementExpand: false,
      statusLocationUser: true,
      replacementPart: [],
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  captureDistance = (dis) => {
    if (dis <= 50) {
      this.setState({
        statusLocationUser: false,
      });
    }
  };

  async componentDidMount() {
    // [TEST] SPINNER
    // setInterval(() => {
    //   this.setState({
    //     spinner: !this.state.spinner,
    //   });
    // }, 3000);
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        let data = JSON.parse(userData);
        // console.log('userData', data);
        this.setState({
          api_token: data.api_token.api_token,
        });
      }
    } catch (err) {
      // console.log(err);
      // console.log('Failed local login');
    }
  }

  onChangeText(text, name) {
    this.setState({
      [name]: text,
    });
  }

  _onChangeReplacePart(objectReplacement) {
    this.setState((prevState) => ({
      replacementPart: [...prevState.replacementPart, objectReplacement],
    }));
  }

  _takePicture(field) {
    const options = {
      title: `Upload foto ${field} anda`,
      storageOptions: {
        path: 'images',
      },
      compressImageQuality: 0.1,
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
        console.error('Error pick image', response.error);
      } else {
        // if(this.state[field]) {
        //   let temp = {
        //     uri: response.uri,
        //     type: response.type,
        //     name: response.fileName,
        //   }

        //   let tempField = this.state[field]
        //   tempField.push(temp)

        //   this.setState({
        //     [field]: tempField
        //   });

        // } else {
        this.setState({
          [field]: {
            uri: response.uri,
            type: 'image/jpeg',
            name: response.fileName,
            path: response.path,
          },
        });
        // }
      }
    });
  }

  handleOnScroll = (event) => {
    this.setState({
      scrollOffset: event.nativeEvent.contentOffset.y,
    });
  };

  handleScrollTo = (p) => {
    if (this.scrollViewRef) {
      this.scrollViewRef.scrollTo(p);
    }
  };

  checkingImage = () => {
    let { image_problem } = this.state;
    let { document, AppraisalData } = this.props;
    let data = _.find(AppraisalData.data, {
      objectField: { name: image_problem.name },
    });
    // console.log("--",document)
    if (image_problem) {
      return (
        <Image
          source={{ uri: image_problem.uri }}
          style={styles.modalImagePicker}
        />
      );
    } else {
      return null;
    }
  };

  async _pushToDatabase(formData) {
    const { api_token } = this.state;
    const { dataWorkStart, togglePhotoModal } = this.props;
    const url = `${API_URL}/pending_array/${dataWorkStart.id}`;
    const config = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${api_token}`,
        'Access-Control-Allow-Origin': '*',
        Accept: 'multipart/form-data',
        'content-type': 'multipart/form-data',
      },
      data: formData,
    };

    try {
      const response = await Axios(url, config);
      await this.setState({ spinner: false });
      await togglePhotoModal();
      await alert('Sukses Submit Data');
      await this.setState({
        displayPhotoModal: !this.props.displayPhotoModal,
      });
      await this.props.onSuccess();
    } catch (error) {
      await this.setState({ spinner: false });
      await alert('Gagal Submit Data');
    }
  }

  onPressUpload = async () => {
    const {
      api_token,
      grounding,
      it,
      voltage,
      ac,
      ups,
      notes,
      kelanjutan_problem,
      pm,
      cm,
      charge,
      image_problem,
      replacementPart,
      work_start,
      work_finish,
      action,
    } = this.state;

    const { dataWorkStart, togglePhotoModal } = this.props;

    let poData = [
      { name: 'Grounding', value: grounding, required: false },
      { name: 'IT', value: it, required: false },
      { name: 'Voltage', value: voltage, required: false },
      { name: 'AC', value: ac, required: false },
      { name: 'UPS', value: ups, required: false },
      { name: 'Notes', value: notes, required: false },
      { name: 'PM', value: pm, required: true },
      { name: 'CM', value: cm, required: true },
      { name: 'Charge', value: charge, required: true },
      { name: 'Work Start', value: work_start, required: false },
      { name: 'Work Finish', value: work_finish, required: false },
      { name: 'Kelanjutan Problem', value: kelanjutan_problem, required: true },
      { name: 'Action', value: action, requierd: true },
      { name: 'Upload Foto', value: image_problem, required: false },
    ];

    let emptyField = checkFields(poData);

    const isEmptyFields = _.size(emptyField) === 0;

    if (isEmptyFields) {
      this.setState({
        spinner: true,
      });

      const isEmptyReplacePart = _.size(replacementPart) === 0;

      let formData = new FormData();
      formData.append('problem_id', `${dataWorkStart.id}`);
      formData.append('grounding', grounding || '');
      formData.append('it', it || '');
      formData.append('voltage', voltage || '');
      formData.append('ac', ac || '');
      formData.append('ups', ups || '');
      formData.append('notes', notes || '');
      formData.append('kelanjutan_problem', kelanjutan_problem);
      formData.append('pm', pm);
      formData.append('cm', cm);
      formData.append('charge', charge);
      formData.append('action', action);
      formData.append('work_start', work_start || moment().format('YYYY-MM-DD HH:mm:ss'));
      formData.append('work_finish', work_finish || moment().format('YYYY-MM-DD HH:mm:ss'));

      const uniqReplacement = _.uniqWith(replacementPart, function (o) {
        return o.numberPart;
      });

      console.log(uniqReplacement, 'Kaoata')

      for (let i = 0; i < uniqReplacement.length; i++) {
        formData.append('number_part[]', i + 1);
        formData.append('part_name[]', uniqReplacement[i] ? uniqReplacement[i].partName : '');
        formData.append('removed_serial[]', uniqReplacement[i] ? uniqReplacement[i].removeSerialNumber : '');
        formData.append('removed_part[]', uniqReplacement[i] ? uniqReplacement[i].removePartNumber : '');
        formData.append('replacing_part[]', uniqReplacement[i] ? uniqReplacement[i].replacingPartNumber : '');
        formData.append('replacing_serial[]', uniqReplacement[i] ? uniqReplacement[i].replacingSerialNumber : '');
      }

      formData.append('image_problem', image_problem);

      this._pushToDatabase(formData);
    } else {
      if (_.isArray(emptyField)) {
        const messageText = emptyField.map((itemField) => {
          return `Field ${itemField.name} wajib di isi.\n`;
        });
        alert(messageText.join(''));
      }
    }
  };

  render() {
    const { displayPhotoModal, togglePhotoModal, dataWorkStart } = this.props;

    const {
      spinner,
      kelanjutan_problem,
      grounding,
      it,
      voltage,
      ac,
      ups,
      pm,
      cm,
      charge,
      notes,
      action,
      replacementExpand,
    } = this.state;
    const partNumber = [1, 2, 3, 4, 5];
    return (
      <Modal
        supportedOrientations={['portrait', 'landscape']}
        onBackdropPress={() => togglePhotoModal()}
        isVisible={displayPhotoModal}
        style={styles.photoModal}>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={
            Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
          }>
          <View style={styles.modalContainer}>
            <View style={styles.modalHeader}>
              <TouchableOpacity onPress={() => togglePhotoModal()}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: widthPercentageToDP('8%'),
                    fontFamily: 'Roboto',
                  }}>
                  x
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              ref={(ref) => (this.scrollViewRef = ref)}
              onScroll={this.handleOnScroll}
              scrollEventThrottle={16}>
              <View style={styles.modalContent}>
                <Text style={styles.modalPhotoName}>Work Finish</Text>
                <View style={styles.containerAccordian}>
                  <TicketInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <ProblemInfo
                    kelanjutan_problem={kelanjutan_problem}
                    dataWorkStart={dataWorkStart}
                    onChangeText={this.onChangeText}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <EngineerInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <AtmEngineerInfo dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <TimeStamp onChangeText={this.onChangeText} dataWorkStart={dataWorkStart} />
                </View>
                <View style={styles.containerAccordian}>
                  <HealthCheck
                    onChangeText={this.onChangeText}
                    dataWorkStart={dataWorkStart}
                    it={it}
                    voltage={voltage}
                    grounding={grounding}
                    ac={ac}
                    ups={ups}
                  />
                </View>
                <View style={[styles.containerAccordian]}>
                  <TouchableOpacity
                    style={styles.headerAccordian}
                    onPress={() => this.setState(prevState => ({ replacementExpand: !prevState.replacementExpand }))}
                  >
                    <Text style={[styles.titleAccordian, styles.fontAccordian]}>
                      PART REMOVAL & REPLACEMENT
                    </Text>
                    <IconMaterial
                      name={ replacementExpand ? 'keyboard-arrow-up' : 'keyboard-arrow-down'}
                      size={30}
                      color={colors.strong_grey}
                    />
                  </TouchableOpacity>
                  {replacementExpand && (
                    <View>
                      {partNumber.map((itemPart, _index) => (
                        <View style={styles.containerAccordian}>
                          <ReplacementPart
                            numberPart={itemPart}
                            onChange={this._onChangeReplacePart}
                          />
                        </View>
                      ))}
                    </View>
                  )}
                </View>
                <View style={styles.containerAccordian}>
                  <ProblemAndAction
                    action={action}
                    notes={notes}
                    onChangeText={this.onChangeText}
                    dataWorkStart={dataWorkStart}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <Miscellaneous
                    onChangeText={this.onChangeText}
                    dataWorkStart={dataWorkStart}
                    pm={pm}
                    cm={cm}
                    charge={charge}
                  />
                </View>
                <View style={styles.containerAccordian}>
                  <Geotaging
                    captureDistance={this.captureDistance}
                    dataWorkStart={dataWorkStart}
                  />
                </View>

                <ScrollView horizontal={true} nestedScrollEnabled={true}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-around',
                      width: '100%',
                      height: heightPercentageToDP('23%'),
                    }}>
                    {this.checkingImage()}
                    <TouchableOpacity
                      onPress={() => this._takePicture('image_problem')}>
                      <View style={styles.modalImagePicker}>
                        <Icon name="camera" size={50} />
                      </View>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 20,
                    marginBottom: 30,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.viewPhotoButton,
                      styles.viewPhotoButtonActive,
                    ]}
                    onPress={() => togglePhotoModal()}>
                    <Icon
                      name="close"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Batalkan</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={[
                      styles.uploadPhotoButton,
                      styles.uploadPhotoButtonActive,
                    ]}
                    onPress={() => this.onPressUpload()}>
                    <Icon
                      name="save"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Simpan</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }
}

const mapStateToProps = ({ AppraisalData }) => ({ AppraisalData });

export default connect(mapStateToProps)(UploadModalTicketPending);
