import React, { useEffect } from 'react';
import { ScrollView, View, Text, Modal, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { useForm } from 'react-hook-form';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import Input from './../Input';

import styles from './styleModal';
import CustomButton from '../ButtonCustom';
import Accordion from '../Accordion';
import styleModal from './styleModal';

function NewUploadTicketPending(props) {
  const { togglePhotoModal, displayPhotoModal } = props;
  const { register, handleSubmit, setValue } = useForm();

  useEffect(() => {
    register('problemId');
  }, [register]);

  function onSubmit(data) {
    console.log(data);
  }

  return (
    <Modal
      visible={displayPhotoModal}
      onRequestClose={() => togglePhotoModal()}
      animationType="slide">
      <View style={styles.modalStrip} />
      <View style={styles.modalHeader}>
        <Text style={styles.titleModal}>Pending</Text>
      </View>
      <ScrollView style={styles.modalContainer}>
        <Accordion label="Ticket Info">
          <Input
            name="ticketNumber"
            style={styleModal.spaceTop10}
            label="Ticket Number :"
          // innerRef={input => console.log(input)}
          />
          <Input
            name="ticketStatus"
            style={styleModal.spaceTop10}
            label="Ticket Status :"
          // innerRef={input => console.log(input)}
          />
        </Accordion>
        <Accordion label="Problem Info" style={styleModal.spaceTop10}>
          <Input
            name="problemNumber"
            style={styleModal.spaceTop10}
            label="Problem Number :"
          />
          <Input
            name="problemStatus"
            style={styleModal.spaceTop10}
            label="Problem Status :"
          />
        </Accordion>
        <Accordion label="Enginner & PIC Info" style={styleModal.spaceTop10}>
          <Input
            name="enginner"
            style={styleModal.spaceTop10}
            label="Enginner :"
          />
          <Input
            name="enginnerTelp"
            style={styleModal.spaceTop10}
            label="Enginner Telp :"
          />
          <Input
            name="picBank"
            style={styleModal.spaceTop10}
            label="PIC Bank :"
          />
          <Input
            name="picTelp"
            style={styleModal.spaceTop10}
            label="PIC Telp :"
          />
        </Accordion>
      </ScrollView>
      <View style={styles.modalFooter}>
        <CustomButton
          onPress={() => togglePhotoModal()}
          color="secondary"
          style={{ marginRight: 8 }}
          icon={
            <IconMaterial
              name="close"
              size={20}
              color="#fff"
              style={{ marginRight: 8 }}
            />
          }>
          Batalkan
        </CustomButton>
        <CustomButton
          onPress={() => handleSubmit(onSubmit)}
          color="primary"
          icon={
            <IconMaterial
              name="done"
              size={20}
              color="#fff"
              style={{ marginRight: 8 }}
            />
          }>
          Submit
        </CustomButton>
      </View>
    </Modal>
  );
}

const mapStateToProps = (GlobalState) => {
  return {
    AppraisalData: GlobalState.AppraisalData,
  };
};

const withConnect = connect(mapStateToProps, null);

export default withConnect(NewUploadTicketPending);
