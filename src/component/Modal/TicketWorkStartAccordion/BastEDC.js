import React from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  UIManager,
  Platform,
  LayoutAnimation,
  TextInput,
} from 'react-native';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';

function BastEDC(props) {
  const [collapse, setCollapse] = React.useState(false);

  React.useEffect(() => {
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  });

  const {onChangeText, dataWorkStart} = props;

  function toggleExpand() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setCollapse((prev) => !prev);
  }

  return (
    <View>
      <TouchableOpacity style={styles.row} onPress={() => toggleExpand()}>
        <Text style={[styles.title, styles.font]}>BAST EDC</Text>

        <Icon
          name={collapse ? 'keyboard-arrow-up' : 'keyboard-arrow-down'}
          size={30}
          color={colors.strong_grey}
        />
      </TouchableOpacity>

      <View style={styles.parentHr} />

      {collapse && (
        <View style={styles.label}>
          <Text style={styles.label}>Nama Merchant</Text>
          <TextInput
            placeholder="please input the merchant name"
            style={styles.inputStyle}
            autoCapitalize="none"
            onChangeText={(text) => onChangeText(text, 'nameMerchant')}
          />

          <Text style={styles.label}>MID</Text>
          <TextInput
            style={styles.inputStyle}
            placeholder="please input the merchant MID"
            autoCapitalize="none"
            onChangeText={(text) => onChangeText(text, 'MID')}
          />

          <Text style={styles.label}>Catatan</Text>
          <TextInput
            placeholder="Catatan"
            multiline
            autoCapitalize="none"
            style={styles.inputStyle}
            onChangeText={(text) => onChangeText(text, 'MID')}
          />
        </View>
      )}
    </View>
  );
}

export default BastEDC;

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
});
