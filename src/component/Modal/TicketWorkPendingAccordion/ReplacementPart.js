import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../utils/colors';
import {widthPercentageToDP, heightPercentageToDP} from '../../../utils';
import {keys} from 'lodash';

class ReplacementPart extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      expand: false,
      allFill: false,
      partName: false,
      numberPart: false,
      removePartNumber: false,
      removeSerialNumber: false,
      replacingPartNumber: false,
      replacingSerialNumber: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const {onChange} = this.props;
    const {numberPart} = prevProps;
    const {allFill} = prevState;

    if (numberPart) {
      this._setNumberPart(this.props.numberPart);
    }

    this._setChangePartRemoval(prevState);

    const ObjectState = {
      partName: prevState.partName,
      numberPart: prevState.numberPart,
      removePartNumber: prevState.removePartNumber,
      removeSerialNumber: prevState.removeSerialNumber,
      replacingPartNumber: prevState.replacingPartNumber,
      replacingSerialNumber: prevState.replacingSerialNumber,
    };

    if (allFill) {
      onChange(ObjectState);
    }
  }

  _setChangePartRemoval(formState) {
    const ObjectState = {
      partName: formState.partName,
      numberPart: formState.numberPart,
      removePartNumber: formState.removePartNumber,
      removeSerialNumber: formState.removeSerialNumber,
      replacingPartNumber: formState.replacingPartNumber,
      replacingSerialNumber: formState.replacingSerialNumber,
    };
    const Keys = Object.keys(ObjectState);
    Keys.map((itemField) => {
      if (this.state[itemField] === false) {
        return this.setState({allFill: false});
      }

      return this.setState({allFill: true});
    });
  }

  _setNumberPart(numPart) {
    this.setState({numberPart: numPart});
  }

  _toggleExpand() {
    this.setState({expand: !this.state.expand});
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  }

  render() {
    const {
      expand,
      partName,
      numberPart,
      removePartNumber,
      removeSerialNumber,
      replacingPartNumber,
      replacingSerialNumber,
    } = this.state;

    const iconKeyBoard = expand ? 'keyboard-arrow-up' : 'keyboard-arrow-down';
    const placeHolderText = `Please fill ${numberPart}`;

    return (
      <View>
        <TouchableOpacity
          ref={this.accordian}
          style={styles.row}
          onPress={() => this._toggleExpand()}>
          <Text style={[styles.title, styles.font]}>
            Part #{this.props.numberPart}
          </Text>
          <Icon name={iconKeyBoard} size={30} color={colors.strong_grey} />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {expand && (
          <View>
            <Text style={styles.label}>Number Part</Text>
            <TextInput
              editable={false}
              // placeholder={placeHolderText}
              autoCapitalize="none"
              placeholder={placeHolderText}
              defaultValue={numberPart}
              value={numberPart.toString()}
              // keyboardType="numeric"
              style={styles.inputStyleReadOnly}
            />
            <Text style={styles.label}>Part Name</Text>
            <TextInput
              value={partName}
              placeholder="Part Name"
              autoCapitalize="none"
              onChangeText={(text) => this.setState({partName: text})}
              style={styles.inputStyle}
            />

            <Text style={styles.label}>Removed Part Number</Text>
            <TextInput
              value={removePartNumber}
              placeholder="Removed Part Number"
              autoCapitalize="none"
              // keyboardType="numeric"
              onChangeText={(text) => this.setState({removePartNumber: text})}
              style={styles.inputStyle}
            />

            <Text style={styles.label}>Removed Serial Number</Text>
            <TextInput
              value={removeSerialNumber}
              placeholder="Removed Serial Number"
              autoCapitalize="none"
              // keyboardType="numeric"
              onChangeText={(text) => this.setState({removeSerialNumber: text})}
              style={styles.inputStyle}
            />

            <Text style={styles.label}>Replacing Part Number</Text>
            <TextInput
              value={replacingPartNumber}
              placeholder="Replacing Part Number"
              autoCapitalize="none"
              // keyboardType="numeric"
              // eslint-disable-next-line prettier/prettier
            onChangeText={(text) => this.setState({ replacingPartNumber: text })}
              style={styles.inputStyle}
            />

            <Text style={styles.label}>Replacing Serial Number</Text>
            <TextInput
              value={replacingSerialNumber}
              placeholder="Replacing Serial Number"
              autoCapitalize="none"
              // keyboardType="numeric"
              // eslint-disable-next-line prettier/prettier
            onChangeText={(text) => this.setState({ replacingSerialNumber: text })}
              style={styles.inputStyle}
            />
          </View>
        )}
      </View>
    );
  }
}

ReplacementPart.propTypes = {
  numberPart: PropTypes.number,
};

export default ReplacementPart;

const styles = StyleSheet.create({
  label: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('1%'),
  },
  title: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.strong_grey,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 56,
    paddingLeft: 0,
    paddingRight: 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  parentHr: {
    height: 1,
    color: 'white',
    width: '100%',
  },
  child: {
    backgroundColor: colors.light_sapphire_bluish_gray,
    padding: 16,
  },
  inputStyle: {
    width: '100%',
    backgroundColor: 'white',
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputStyleReadOnly: {
    width: '100%',
    backgroundColor: colors.light_bluish_gray,
    borderRadius: 6,
    marginBottom: widthPercentageToDP('4%'),
    borderColor: colors.light_bluish_gray,
    borderWidth: 1.5,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
    marginBottom: widthPercentageToDP('4%'),
  },
  inputFormDisabled: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    backgroundColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('0.1%'),
    color: colors.dope_blue,
  },
  inputFormPicker: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
  },
});
