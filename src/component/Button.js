import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import colors from '../utils/colors';

const Button = ({color, children, style, onPress, onLayout}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      onLayout={onLayout}
      style={[
        styles.container,
        {backgroundColor: color ? color : colors.grey},
        style,
      ]}>
      {children}
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    padding: 8,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
