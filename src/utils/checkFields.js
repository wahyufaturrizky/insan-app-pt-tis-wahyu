import {filter} from 'lodash';

function checkFields(arrayForm) {
  const fields = filter(arrayForm, function (o) {
    if (o.required) {
      return o.value === '';
    }
  });

  return fields;
}

export default checkFields;
