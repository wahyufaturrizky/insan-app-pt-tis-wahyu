import React from "react";
import { View, ScrollView, Text, Image, TouchableOpacity, ToastAndroid, Linking, StyleSheet } from "react-native";
import moment from "moment";
import "moment/locale/id";
import Icon from 'react-native-vector-icons/FontAwesome'
import colors from '../../utils/colors'

import { heightPercentageToDP, widthPercentageToDP } from "../../utils";
import { requestAppraisalDetail } from "../../actions";
import logo from "../../assets/image/logo-trust.png";
moment.locale("id");

class HistoryAppraisList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            appraisalList: [
                {
                    noBooking: 'TR-022019-423',
                    carType: 'Avanza',
                    bookingTime: "2019-02-07T16:37:00.000Z",
                    status: 'FINISHED'
                },
                {
                    noBooking: 'TR-022019-423',
                    carType: 'Avanza',
                    bookingTime: "2019-02-07T16:37:00.000Z",
                    status: 'FINISHED'
                },
                {
                    noBooking: 'TR-022019-423',
                    carType: 'Avanza',
                    bookingTime: "2019-02-07T16:37:00.000Z",
                    status: 'FINISHED'
                },
                {
                    noBooking: 'TR-022019-423',
                    carType: 'Avanza',
                    bookingTime: "2019-02-07T16:37:00.000Z",
                    status: 'FINISHED'
                }
            ]
         };
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    {
                        this.state.appraisalList.map((appraisal, index) => (
                            <View style={styles.historyBox} key={index}>
                                <View style={styles.noOrderBox}>
                                    <Text style={styles.noOrder}>{appraisal.noBooking}</Text>

                                    <Text style={styles.statusText}>{appraisal.status}</Text>
                                </View>
                                <View style={styles.detailBook}>
                                    <Text style={styles.brandText}>{appraisal.carType}</Text>
                                    <Text style={styles.date}>{moment(appraisal.bookingTime, "YYYY-MM-DDTHH:mm").format("llll")}</Text>
                                </View>
                            </View>
                        ))
                    }
                    
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingLeft: widthPercentageToDP('5%'),
        height: heightPercentageToDP('100%'),
        backgroundColor: 'white'
    },
    historyBox: {
        paddingVertical: widthPercentageToDP('4%'),
        paddingRight: widthPercentageToDP('5%'),
        borderColor: colors.supeer_light_grayish_blue,
        borderBottomWidth: 1.5
    },
    noOrderBox: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: widthPercentageToDP('2%')
    },
    noOrder: {
        fontSize: widthPercentageToDP('4.3%'),
        color: colors.strong_blue,
        fontWeight: 'bold'
    },
    statusText: {
        color: colors.dope_blue,
        fontSize: widthPercentageToDP('4.3%'),
    },
    date: {
        color: colors.dope_blue,
        fontSize: widthPercentageToDP('4.3%')
    },
    detailBook: {
        paddingBottom: widthPercentageToDP('2%'),
    },
})

export default HistoryAppraisList;