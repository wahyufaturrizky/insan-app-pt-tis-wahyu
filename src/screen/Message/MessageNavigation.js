import React, { Component } from 'react'
import {createAppContainer} from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { Image, TouchableOpacity, View, StyleSheet } from 'react-native'
import {widthPercentageToDP,heightPercentageToDP} from '../../utils/index'
import colors from '../../utils/colors'

import MessageList from './MessageList'

const MessageNav = createStackNavigator({
    MessageList: {
      screen: MessageList,
      navigationOptions:  ({ navigation }) => ({
        title: 'Message',
        headerStyle: {
          backgroundColor: colors.strong_blue
        },
        headerTintColor: 'white',
        headerLeft: () => (<TouchableOpacity style={{marginLeft: 10}} onPress={navigation.openDrawer}>
                      <Image source={require('../../assets/image/menu.png')} style={{width:widthPercentageToDP('8%'), height: widthPercentageToDP('8%')}}/>
                    </TouchableOpacity>),
        headerTitleStyle: {
          fontSize: widthPercentageToDP('5%'),
          fontWeight: '200',
          textAlign: 'center',
          marginLeft: widthPercentageToDP('25%')
        }
    })
    },
  })

export default MessageNav