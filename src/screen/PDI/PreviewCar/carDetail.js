import React from 'react';
import {View, Text, ScrollView, StyleSheet, TextInput} from 'react-native';
import Label from '../../component/Label';
import FormSelect from '../../component/FormSelect';
import axios from "../../services/axios";

class EditSummary extends React.Component {
  constructor() {
    super();
    this.state = {
      Brand: [{ brand: "Brand tidak ada di list", id: "" }],
      Model: [{ model: "model tidak ada di list", id: "" }],
      Tipe: [],
      KategoriWarna: [],
      selectedBrand: null,
      selectedModel: null,
      selectedType: null,
    };
  }

  componentDidMount() {
    this.fetchBrand();
    this.fetchColor();
  }

  fetchBrand = () => {
    axios
      .get("/value-car/brand")
      .then(result => {
        let carBrandList = result.data.carBrand;
        console.log(carBrandList, "brand")
        this.setState({
          Brand: [...carBrandList],
          Model: [],
          Tipe: [],
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchModel = id => {
    axios
      .get(`/value-car/brand/${id}`)
      .then(result => {
        console.log(result.data.carType, "model")
        this.setState({
          Model: [...result.data.carType, { carType: "Pilih Model", id: ""}],
          Tipe: [],
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchType = id => {
    axios
      .get(`/value-car/type/${id}`)
      .then(result => {
        this.setState({
          Tipe: [...result.data.carSpec, { carSpec: "Pilih Tipe", id: "" }],
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchColor = () => {
    axios
      .get(`/misc/colors`)
      .then(result => {
        this.setState({
          KategoriWarna: result.data.kategori_warna,
        });
        // console.log(result)
      })
      .catch(err => {
        console.log(err);
      });
  };

  onChangeBrand = (index, id, field) => {
    if (id !== "") {
      this.setState({
        other: false,
      });
      this.fetchModel(id);
    } else {
      this.setState({
        other: true,
        Model: [],
        Tipe: [],
      });
    }
    this.onChangeField(index, field, id);
  };

  onChangeType = (index, id, field) => {
    if (id !== "") {
      this.fetchType(id);
    }
    this.onChangeField(index, field, id);
  };

  render() {
    const {Brand, Model, Tipe, KategoriWarna } = this.state
    return (
      <ScrollView>
        <View style={styles.content}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Brand *" />
              <FormSelect
                label='brand'
                selectedValue={this.state.selectedBrand}
                items={Brand}
                onSelectedItem={({text, id}) =>
                  this.setState({selectedBrand: text}, () => this.fetchModel(id))
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Model *" />
              <FormSelect
                label='carType'
                selectedValue={this.state.selectedModel}
                items={this.state.Model}
                onSelectedItem={({text}) =>
                  this.setState({selectedModel: text})
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Tipe *" />
              <FormSelect
                prop='type'
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Tahun *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Camry', 'Model 1', 'Model 2']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
          </View>

          {/* 
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Kategori Warna *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Nama STNK *" />
              <TextInput
                style={{
                  borderWidth: 0.5,
                  borderRadius: 3,
                  padding: 7,
                  borderColor: 'grey',
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Nilai STNK *" />
              <TextInput
                style={{
                  borderWidth: 0.5,
                  borderRadius: 3,
                  padding: 7,
                  borderColor: 'grey',
                }}
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="STNK Hidup *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Banjir *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Kecelakaan *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Wheel *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Paint *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="STNK *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Kebersihan Interior *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Nik/Form A *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}>
              <Label text="Faktur *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '49%'}}>
              <Label text="Kunci Serep *" />
              <FormSelect
                selectedValue={this.state.selectedValue}
                items={['Toyota', 'Avanza', 'Suzuki']}
                onSelectedItem={({text}) =>
                  this.setState({selectedValue: text})
                }
              />
            </View>
            <View
              style={{
                width: '49%',
              }}></View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 10,
            }}>
            <View style={{width: '98%'}}>
              <Label text="Kunci Serep *" />
              <TextInput
                numberOfLines={3}
                style={{
                  borderWidth: 0.5,
                  borderRadius: 3,
                  padding: 7,
                  borderColor: 'grey',
                }}
              />
            </View>
          </View> */}
        </View>
      </ScrollView>
    );
  }
}

export default EditSummary;

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: 'white',
    padding: 15,
  },
});
