/* eslint-disable handle-callback-err */
/* eslint-disable react-native/no-inline-styles */
import React, {Component, Fragment} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  AsyncStorage,
  ScrollView,
  TouchableOpacity,
  ToastAndroid,
  RefreshControl,
} from 'react-native';
import {connect} from 'react-redux';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils/index';
import colors from '../../utils/colors';
import moment from 'moment';
import axios from '../../services/axios';
import Modal from 'react-native-modal';
import Button from '../../component/ButtonLogout/Button';
import UploadModal from '../../component/Modal/UploadModal';
import UploadModalPhotoProfile from '../../component/Modal/UploadModalPhotoProfile';
import UploadEditPasswordModal from '../../component/Modal/UploadEditPasswordModal';
import Spinner from 'react-native-loading-spinner-overlay';
import {get} from 'lodash';

class Profile extends Component {
  constructor(props) {
    super(props);
    this._toggleEditProfilPhotoModal = this._toggleEditProfilPhotoModal.bind(
      this,
    );
    this._toggleEditProfilModal = this._toggleEditProfilModal.bind(this);
    this._toggleEditPasswordModal = this._toggleEditPasswordModal.bind(this);
    this._showImage = this._showImage.bind(this);
    this._closeImage = this._closeImage.bind(this);
    this._onPressEditProfil = this._onPressEditProfil.bind(this);
    this._onPressEditProfilPhoto = this._onPressEditProfilPhoto.bind(this);
    this._onPressEditPasswordPhoto = this._onPressEditPasswordPhoto.bind(this);
    this._getProfile = this._getProfile.bind(this);
    this.state = {
      name: '',
      email: '',
      no_telp: '',
      role: '',
      updated_at: '',
      username: '',
      api_token: '',
      img_user: null,
      pass: '',
      modalLogout: false,
      displayEditProfilModal: false,
      displayEditPhotoModal: false,
      displayEditPasswordModal: false,
      document: {},
      spinner: false,
    };
  }

  async componentDidMount() {
    this.setState({
      spinner: true,
    });
    this._getProfile();
  }

  async _getProfile() {
    const userData = await AsyncStorage.getItem('userData');
    const data = JSON.parse(userData);
    const token = get(data, 'api_token.api_token');
    const url = '/profile';
    const config = {
      method: 'get',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      const response = await axios(url, config);
      const {data} = response;
      await this.setState({
        name: data.name,
        email: data.email,
        no_telp: data.no_telp,
        role: data.role,
        updated_at: data.updated_at,
        img_user: data.img_user,
        username: data.username,
        spinner: false,
        refreshing: false,
      });
    } catch (error) {
      // eslint-disable-next-line prettier/prettier
      ToastAndroid.showWithGravity('Gagal menampilkan data profile', 200, ToastAndroid.BOTTOM);
      this.setState({spinner: false});
      this.setState({refreshing: false});
    }
  }

  async componentDidUpdate(prevProps, prevState) {
    const {displayEditProfilModal, api_token} = this.state;

    if (displayEditProfilModal !== prevState.displayEditProfilModal) {
      this._getProfile();
    }
  }

  _onPressEditProfil() {
    this.setState({
      displayEditProfilModal: true,
    });
  }

  _onPressEditProfilPhoto() {
    this.setState({
      displayEditPhotoModal: true,
    });
  }

  _onPressEditPasswordPhoto() {
    this.setState({
      displayEditPasswordModal: true,
    });
  }

  _toggleEditProfilModal() {
    this.setState({
      displayEditProfilModal: !this.state.displayEditProfilModal,
    });
  }

  _toggleEditProfilPhotoModal() {
    this.setState({
      displayEditPhotoModal: !this.state.displayEditPhotoModal,
    });
  }

  _toggleEditPasswordModal() {
    this.setState({
      displayEditPasswordModal: !this.state.displayEditPasswordModal,
    });
  }

  _showImage(imageUrl) {
    this.setState({
      imageLink: imageUrl,
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _closeImage() {
    this.setState({
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _onPressLogoutBtn = () => {
    this.setState({
      modalLogout: true,
    });
  };

  async _onLogoutFromApi() {
    const userData = await AsyncStorage.getItem('userData');
    const data = JSON.parse(userData);
    const token = get(data, 'api_token.api_token');
    const url = '/logout';
    const config = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    try {
      const response = await axios(url, config);
      await this.setState({spinner: false});
	  await AsyncStorage.removeItem('userData');
      await this.props.navigation.navigate('Login');
    } catch (error) {
      console.log(error?.response);
      await this.setState({spinner: false});
      await ToastAndroid.showWithGravity(
        'Gagal Logout',
        2000,
        ToastAndroid.BOTTOM,
      );
    }
  }

  onPressModalLogoutYes() {
    this.setState({
      spinner: true,
    });

    this.setState({modalLogout: false});
    this._onLogoutFromApi();
  }

  onPressModalLogoutNo() {
    this.setState({
      modalLogout: !this.state.modalLogout,
    });
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this._getProfile();
  }

  render() {
    const {
      name,
      email,
      no_telp,
      role,
      updated_at,
      img_user,
      username,
      displayEditProfilModal,
      displayEditPhotoModal,
      displayEditPasswordModal,
      document,
      spinner,
      refreshing,
    } = this.state;

    const imgUser = img_user
      ? {
          uri: `http://demo-kota.com/insan5/public/storage/public/user/${img_user}`,
        }
      : require('../../assets/image/Mango.png');

    return (
      <Fragment>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          <View style={{position: 'relative'}}>
            <Image
              source={require('../../assets/image/background.png')}
              style={styles.banner}
            />
            <View
              style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                position: 'absolute',
                flex: 1,
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
              }}>
              <Image source={imgUser} style={styles.avatar} />
              <Text style={styles.name}>{name}</Text>
            </View>
          </View>
          <View style={{marginTop: -widthPercentageToDP('2%')}}>
            <View style={styles.box}>
              <Text style={styles.label}>Nama</Text>
              <Text style={styles.regText}>{name}</Text>
            </View>
            <View style={styles.box}>
              <Text style={styles.label}>Username</Text>
              <Text style={styles.regText}>{username}</Text>
            </View>
            <View style={styles.box}>
              <Text style={styles.label}>Emaill</Text>
              <Text style={styles.regText}>{email}</Text>
            </View>
            <View style={styles.box}>
              <Text style={styles.label}>No.Handphone</Text>
              <Text style={styles.regText}>{no_telp}</Text>
            </View>
            <View style={styles.box}>
              <Text style={styles.label}>Role</Text>
              <Text style={styles.regText}>{role}</Text>
            </View>
            <View style={styles.box}>
              <Text style={styles.label}>Update Date</Text>
              <Text style={styles.regText}>
                {moment(updated_at, 'YYYY-MM-DDTHH:mm').format('ll')}
              </Text>
            </View>
            <View style={styles.box}>
              <TouchableOpacity
                onPress={() => this._onPressEditPasswordPhoto()}
                style={styles.logoutButton}>
                <Text style={styles.logoutText}>Edit Password</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.box}>
              <TouchableOpacity
                onPress={() => this._onPressEditProfilPhoto()}
                style={styles.logoutButton}>
                <Text style={styles.logoutText}>Edit Photo</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.box}>
              <TouchableOpacity
                onPress={() => this._onPressEditProfil()}
                style={styles.logoutButton}>
                <Text style={styles.logoutText}>Edit Profil</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.box}>
              <TouchableOpacity
                onPress={() => this._onPressLogoutBtn()}
                style={styles.logoutButton}>
                <Text style={styles.logoutText}>Logout</Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* [START] MODAL LOGOUT */}
          <Modal isVisible={this.state.modalLogout}>
            <View style={styles.modalLogout}>
              <Text style={styles.modalLogoutTitle}>Logout</Text>
              <View style={{paddingTop: 10, paddingBottom: 10}}>
                <Text style={styles.modalLogoutDesc}>
                  Apakah anda yakin ingin logout?
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  width: '100%',
                }}>
                <Button
                  onPress={() => this.onPressModalLogoutNo()}
                  text="Tidak"
                />
                <Button
                  onPress={() => this.onPressModalLogoutYes()}
                  text="Ya"
                  buttonStyle={{backgroundColor: '#FB8B34'}}
                />
              </View>
            </View>
          </Modal>
          {/* [END] MODAL LOGOUT */}
          {/* [START] MODAL EDIT PROFIL */}
          <View style={styles.containerModal}>
            {displayEditProfilModal ? (
              <UploadModal
                document={document}
                togglePhotoModal={this._toggleEditProfilModal}
                displayPhotoModal={displayEditProfilModal}
                previewPhoto={this._showImage}
              />
            ) : (
              false
            )}
            {displayEditPhotoModal ? (
              <UploadModalPhotoProfile
                document={document}
                togglePhotoModal={this._toggleEditProfilPhotoModal}
                displayPhotoModal={displayEditPhotoModal}
                previewPhoto={this._showImage}
                onRefresh={this._getProfile}
              />
            ) : (
              false
            )}
            {displayEditPasswordModal ? (
              <UploadEditPasswordModal
                document={document}
                togglePhotoModal={this._toggleEditPasswordModal}
                displayPhotoModal={displayEditPasswordModal}
                previewPhoto={this._showImage}
              />
            ) : (
              false
            )}
          </View>
          {/* [END] MODAL EDIT PROFIL */}
        </ScrollView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  banner: {
    width: widthPercentageToDP('105%'),
    height: heightPercentageToDP('30%'),
    marginLeft: -widthPercentageToDP('3%'),
    marginTop: -widthPercentageToDP('2%'),
  },
  avatar: {
    width: widthPercentageToDP('27%'),
    height: widthPercentageToDP('27%'),
    borderRadius: 10,
  },
  name: {
    color: colors.dope_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('5%'),
    marginTop: 5,
  },
  box: {
    paddingHorizontal: widthPercentageToDP('4%'),
    paddingVertical: widthPercentageToDP('4%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    marginBottom: widthPercentageToDP('1.5%'),
  },
  label: {
    color: colors.dope_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('4.5%'),
  },
  regText: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4.5%'),
  },
  logoutButton: {
    backgroundColor: colors.strong_blue,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: colors.strong_blue,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
  },
  logoutText: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  modalLogout: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    marginHorizontal: 10,
    padding: 20,
  },
  modalLogoutTitle: {
    color: '#005596',
    fontSize: 20,
    fontFamily: 'Nunito Sans',
  },
  modalLogoutDesc: {
    fontFamily: 'Nunito Sans',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});

const mapStateToProps = ({Auth}) => ({Auth});

const withConnect = connect(mapStateToProps, null);

export default withConnect(Profile);
