/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  Linking,
  FlatList,
  StyleSheet,
  RefreshControl,
} from 'react-native';
import moment from 'moment';
moment.locale('id');
import 'moment/locale/id';
import colors from '../../utils/colors';

import {widthPercentageToDP} from '../../utils';
// import {requestAppraisalDetail} from '../../actions';
import axios from '../../services/axios';
import Spinner from 'react-native-loading-spinner-overlay';
import {get} from 'lodash';

const apiUrl = '/machine/paginate/10';

class AtmList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ticketList: [],
      total: 0,
      isNext: 0,
      currentPage: 0,
      searchValue: '',
      refreshing: false,
    };
  }

  componentDidMount() {
    // this.fetchTicketList();
  }

  fetchTicketList() {
    this.setState({
      spinner: true,
    });

    const {api_token} = this.props.Auth.api_token;

    axios
      .get(apiUrl, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
        params: {
          page: 1,
        },
      })
      .then((result) => {
        const {data} = result;
        const listData = get(data, 'data');
        const total = get(data, 'total');
        const currentPage = get(data, 'current_page');
        const isNext = get(data, 'to');

        this.setState({
          ticketList: listData,
          total,
          currentPage,
          isNext,
          refreshing: false,
        });
      })
      .catch((_err) => {
        this.setState({
          spinner: false,
        });
      });
  }

  _moreFetchMachine() {
    const {api_token} = this.props.Auth.api_token;

    axios
      .get(apiUrl, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
        params: {
          page: 2,
        },
      })
      .then((result) => {
        const {data} = result;
        const listData = get(data, 'data');
        const total = get(data, 'total');
        const currentPage = get(data, 'current_page');
        const isNext = get(data, 'to');

        this.setState({
          ticketList: listData,
          total,
          currentPage,
          isNext,
          refreshing: false,
        });
      })
      .catch((_err) => {
        ToastAndroid.showWithGravity(
          'Gagal Menampilkan List ATM',
          2000,
          ToastAndroid.BOTTOM,
        );
      });
  }

  searchUpdated(value) {
    this.setState({searchValue: value});
  }

  confirmWhatsapp = (appraisalId, appraisalMisc) => {
    appraisalMisc.whatsappConfirmation.status = true;
    appraisalMisc.whatsappConfirmation.timestamp = new Date();

    axios
      .post(`appraisal/${appraisalId}/whatsapp-confirmation`, appraisalMisc)
      .then((appraisal) => {
        console.log(appraisal.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  _goToMyPosition = (lat, lon) => {
    // console.log('lat long', lat, lon);
    this.refs.Map_Ref.injectJavaScript(`
      mymap.setView([${lat}, ${lon}], 10)
      L.marker([${lat}, ${lon}]).addTo(mymap)
    `);
  };

  openWhatsapp = (
    appraisalId,
    appraisalMisc,
    salesman,
    date,
    time,
    car,
    location,
    appraiser,
  ) => {
    let salesNum = '';
    if (
      salesman.whatsapp[0] === '0' ||
      (salesman.whatsapp[0] === '6' && salesman.whatsapp[1] === '2')
    ) {
      salesNum = salesNum + '+62';
    }
    if (
      (salesman.whatsapp.indexOf('-') !== -1 && salesman.whatsapp[0] === '0') ||
      (salesman.whatsapp.indexOf(' ') !== -1 && salesman.whatsapp[0] === '0')
    ) {
      for (let i = 1; i < salesman.whatsapp.length; i++) {
        if (salesman.whatsapp[i] !== '-' || salesman.whatsapp[i] !== ' ') {
          salesNum = salesNum + salesman.whatsapp[i];
        }
      }
    } else if (
      (salesman.whatsapp.indexOf('-') !== -1 && salesman.whatsapp[0] !== '0') ||
      (salesman.whatsapp.indexOf(' ') !== -1 && salesman.whatsapp[0] !== '0')
    ) {
      for (let i = 2; i < salesman.whatsapp.length; i++) {
        if (salesman.whatsapp[i] !== '-' || salesman.whatsapp[i] !== ' ') {
          salesNum = salesNum + salesman.whatsapp[i];
        }
      }
    } else if (salesman.whatsapp[0] !== '0') {
      salesNum = salesNum + salesman.whatsapp.slice(2);
    } else {
      salesNum = salesNum + salesman.whatsapp.slice(1);
    }

    this.confirmWhatsapp(appraisalId, appraisalMisc);
    // this.fetchAppraisalList();

    Linking.openURL(
      `https://wa.me/${salesNum}?text=Bpk/Ibu ${salesman.name}\n
      Terima kasih sudah booking appraisal di Toyota Trust. Saya hendak mengkonfirmasi ulang appraisal berikut ini:

      Hari/Tanggal: ${date}
      Jam: ${time}
      Mobil: ${car}
      Detail Lokasi: ${location}

      Regards,
      ${appraiser}
      Appraiser Toyota Trust`,
    );
  };

  _onRefresh() {
    this.setState({refreshing: true});

    // console.log('this.props.Auth', this.props.Auth.api_token);
    const {api_token} = this.props.Auth.api_token;

    axios
      .get('/machine/paginate/10', {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
        params: {
          page: 1,
        },
      })
      .then((result) => {
        const {data} = result;
        const listData = get(data, 'data');
        const total = get(data, 'total');
        const currentPage = get(data, 'current_page');
        const isNext = get(data, 'to');

        this.setState({
          ticketList: listData,
          total,
          currentPage,
          isNext,
          refreshing: false,
        });
      })
      .catch((_err) => {
        ToastAndroid.showWithGravity(
          'Gagal Menampilkan List ATM',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          refreshing: false,
        });
      });
  }

  render() {
    const {spinner, refreshing, ticketList} = this.state;

    return (
      <React.Fragment>
        {/* <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        /> */}
        <View style={styles.listContainer}>
          <FlatList
            contentContainerStyle={{
              flex: 1,
              flexDirection: 'column',
              height: '100%',
              width: '100%',
            }}
            // refreshControl={
            //   <RefreshControl
            //     refreshing={refreshing}
            //     onRefresh={this._onRefresh.bind(this)}
            //   />
            // }
            initialNumToRender={10}
            onEndReached={() => this._moreFetchMachine}
            onEndReachedThreshold={0.5}
            data={[]}
            renderItem={(itemAtm, _key) => {
              const machineId = get(itemAtm, 'machine_id');
              const machineType = get(itemAtm, 'type.name_type');
              const machineSN = get(itemAtm, 'machine_sn');
              const machineBankName = get(itemAtm, 'bank.name_bank');
              const machineNameAddress = get(itemAtm, 'name_address');
              const machineStatus = get(itemAtm, 'status');

              return (
                <TouchableOpacity>
                  <View style={styles.noOrderBox}>
                    <Text style={styles.noOrder}>
                      {machineId} ({machineType} {machineSN})
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: widthPercentageToDP('3%'),
                        color:
                          machineStatus === 'Tidak Aktif'
                            ? colors.red_insan
                            : colors.green_insan,
                      }}>
                      {itemAtm.status}
                    </Text>
                  </View>
                  <View style={styles.detailBook}>
                    <Text style={styles.date}>{machineBankName}</Text>
                    <Text style={styles.brandText}>
                      <Text>{machineNameAddress}</Text>
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infoBox: {
    width: widthPercentageToDP('43.5%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderRadius: 5,
  },
  infoTitle: {
    color: colors.strong_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('10%'),
  },
  infoSubHead: {
    fontSize: widthPercentageToDP('3.5%'),
  },
  listContainer: {
    marginTop: widthPercentageToDP('4%'),
    paddingHorizontal: widthPercentageToDP('5%'),
  },
  bookingItem: {
    padding: widthPercentageToDP('4%'),
    backgroundColor: 'white',
    elevation: 4,
    shadowOffset: {width: 7, height: 5},
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 18,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('5%'),
  },
  noOrderBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: widthPercentageToDP('2%'),
  },
  noOrder: {
    fontSize: widthPercentageToDP('3%'),
    color: colors.strong_blue,
    fontWeight: 'bold',
  },
  brandText: {
    fontSize: widthPercentageToDP('4.35%'),
    fontWeight: 'bold',
    color: colors.dope_blue,
  },
  date: {
    paddingBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4.3%'),
  },
  detailBook: {
    paddingBottom: widthPercentageToDP('2%'),
    borderColor: colors.light_sapphire_bluish_gray,
    // borderBottomWidth: 1.5,
  },
  salesProfile: {
    paddingVertical: widthPercentageToDP('2%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  WAbutton: {
    width: widthPercentageToDP('40%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2.2%'),
    // backgroundColor: colors.whatsapp,
    flexDirection: 'row',
    borderRadius: 5,
  },
  WAtext: {
    color: 'white',
    fontSize: widthPercentageToDP('4.5%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  WAactive: {
    backgroundColor: colors.whatsapp,
  },
  WAinactive: {
    backgroundColor: colors.super_light_grayish_blue,
  },
  badgeBox: {
    position: 'absolute',
    top: 0,
    bottom: 100,
    zIndex: 100,
  },
  ButtonArea: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  Button: {
    width: 80,
    padding: 10,
    borderRadius: 10,
    backgroundColor: 'black',
    alignItems: 'center',
  },
  ButtonText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
  findLocationATMButton: {
    backgroundColor: colors.strong_blue,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: colors.strong_blue,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
  },
  logoutTextFindLocationButton: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  searchInput: {
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1,
  },
  containerSearch: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    marginBottom: widthPercentageToDP('5%'),
  },
  valueItem: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    padding: 10,
  },
  valueSubject: {
    color: 'rgba(0,0,0,0.5)',
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});

const mapStateToProps = ({Auth, AppraisalDetail, AppraisalData}) => ({
  Auth,
  AppraisalDetail,
  AppraisalData,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AtmList);
