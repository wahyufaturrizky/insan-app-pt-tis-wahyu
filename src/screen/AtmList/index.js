/* eslint-disable react-native/no-inline-styles */
import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import Snackbar from '../../component/Snackbar';
import styles, {Color} from './styles';
import {widthPercentageToDP, heightPercentageToDP} from '../../utils';
import {connect} from 'react-redux';
import {get} from 'lodash';
import axios from '../../services/axios';

import {
  FlatList,
  View,
  Text,
  TouchableOpacity,
  RefreshControl,
  ToastAndroid,
  ActivityIndicator,
} from 'react-native';

// Endpoint
const apiUrl = '/machine/paginate/10';

function EmptyContentList(props) {
  return (
    <View
      style={{
        width: widthPercentageToDP('90%'),
        height: heightPercentageToDP('50%'),
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Text
        style={{
          color: Color.dope_blue,
          fontSize: widthPercentageToDP('5%'),
        }}>
        Empty
      </Text>
    </View>
  );
}

function AtmList(props) {
  const {Auth} = props;
  const apiToken = get(Auth, 'api_token.api_token');
  const [refreshing, setRefreshing] = React.useState(false);
  const [isLoadMore, setisLoadMore] = React.useState(false);
  const [ticketList, setTicketList] = React.useState({
    list: [],
    isNext: false,
    total: 0,
    currentPage: 0,
  });

  React.useEffect(() => {
    _pullFromDatabase();
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  async function _pullFromDatabase(page = 1, isMore = false) {
    if (!isMore) {
      setRefreshing(true);
    } else {
      setisLoadMore(true);
    }
    const config = {
      method: 'get',
      headers: {
        Authorization: `Bearer ${apiToken}`,
      },
      params: {
        page,
      },
    };

    try {
      const response = await axios(apiUrl, config);
      const {data, to, current_page, total} = response.data[0];
      if (!isMore) {
        setTicketList({
          list: data,
          isNext: to,
          total: total,
          currentPage: current_page,
        });
      } else {
        setTicketList((prev) => ({
          list: [...prev.list, ...data],
          isNext: to,
          total: total,
          currentPage: current_page,
        }));
      }
      if (!isMore) {
        setRefreshing(false);
      } else {
        setisLoadMore(false);
      }
    } catch (error) {
      Snackbar('Gagal Menampilkan List ATM');
    }
  }

  return (
    <Fragment>
      <FlatList
        contentContainerStyle={{
          flexGrow: 1,
          marginTop: widthPercentageToDP('4%'),
          paddingHorizontal: widthPercentageToDP('4%'),
        }}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => _pullFromDatabase(1, false)}
          />
        }
        initialNumToRender={10}
        onEndReached={() => _pullFromDatabase(ticketList.currentPage + 1, true)}
        onEndReachedThreshold={0.1}
        data={ticketList.list}
        renderItem={({item}) => {
          const machineId = get(item, 'machine_id');
          const machineType = get(item, 'type.name_type');
          const machineSN = get(item, 'machine_sn');
          const machineBankName = get(item, 'bank.name_bank');
          const machineNameAddress = get(item, 'name_address');
          const machineStatus = get(item, 'status');

          return (
            <View style={styles.bookingItem}>
              <TouchableOpacity>
                <View style={styles.noOrderBox}>
                  <Text style={styles.noOrder}>
                    {machineId} ({machineType} {machineSN})
                  </Text>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: widthPercentageToDP('3%'),
                      color:
                        machineStatus === 'Tidak Aktif'
                          ? Color.red_insan
                          : Color.green_insan,
                    }}>
                    {machineStatus}
                  </Text>
                </View>
                <View style={styles.detailBook}>
                  <Text style={styles.date}>{machineBankName}</Text>
                  <Text style={styles.brandText}>
                    <Text>{machineNameAddress}</Text>
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          );
        }}
        ListEmptyComponent={<EmptyContentList />}
        ListFooterComponent={() => {
          if (!isLoadMore) {
            return null;
          }

          return (
            <View
              style={{
                position: 'relative',
                paddingBottom: heightPercentageToDP('5%'),
                width: '100%',
                borderColor: Color.very_light_grayish_blue,
              }}>
              <ActivityIndicator animating size="large" />
            </View>
          );
        }}
      />
    </Fragment>
  );
}

const mapStateToProps = ({Auth, AppraisalDetail, AppraisalData}) => ({
  Auth,
  AppraisalDetail,
  AppraisalData,
});

const withConnect = connect(mapStateToProps, null);

AtmList.propTypes = {
  Auth: PropTypes.object,
  AppraisalDetail: PropTypes.object,
  AppraisalData: PropTypes.object,
};

export default withConnect(AtmList);
