import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Linking,
  StyleSheet,
} from 'react-native';
import moment from 'moment';
import 'moment/locale/id';
import {
  resetApprasialData,
  setApprasialData,
} from '../../reducers/ApprasialData';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../utils/colors';

import {heightPercentageToDP, widthPercentageToDP} from '../../utils';
// import {requestAppraisalDetail} from '../../actions';
import axios from '../../services/axios';
import logo from '../../assets/image/logo-trust.png';
moment.locale('id');

const BookingList = [
  {
    Booking: {
      noBooking: 'TR-092018-42',
      carType: 'AGYA',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-43',
      carType: 'Alphard',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-44',
      carType: 'Avanza',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-45',
      carType: 'Fortuner',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
];

class ErrorList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ticketList: [],
    };
    // this.navigationWillFocusListener = props.navigation.addListener(
    //   'willFocus',
    //   () => {
    //     this.fetchAppraisalList();
    //   },
    // );
  }

  // componentWillUnmount() {
  //   this.navigationWillFocusListener.remove();
  //   setApprasialData([]);
  // }

  componentDidMount() {
    // console.log('DATA ==', this.props.AppraisalData);
    this.fetchTicketList();
  }

  // componentDidUpdate(prevProps) {
  //   const {AppraisalDetail} = this.props;
  //   if (prevProps.AppraisalDetail.onRequestDetailAppraisal) {
  //     if (AppraisalDetail.successDetailAppraisal) {
  //       this.props.navigation.navigate('Appraisal');
  //     } else if (AppraisalDetail.errorDetailAppraisal) {
  //       ToastAndroid.showWithGravity(
  //         'Gagal menampilkan detail appraisal',
  //         2000,
  //         ToastAndroid.BOTTOM,
  //       );
  //     } else {
  //       console.log('Something else ?', AppraisalDetail);
  //     }
  //   }
  // }

  fetchTicketList() {
    console.log('this.props.Auth', this.props.Auth.api_token);
    const {api_token} = this.props.Auth.api_token;

    axios
      .get(`/error`, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        console.log('fetchTicketList', result.data);
        // let filterResult = result.data.appraisalList.filter(
        //   (data) => data.status !== 'FINISHED',
        // );
        // console.log(filterResult);
        this.setState({
          ticketList: result.data,
        });
      })
      .catch((err) => {
        console.log('ini error', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan list ticket',
          2000,
          ToastAndroid.BOTTOM,
        );
      });
  }

  confirmWhatsapp = (appraisalId, appraisalMisc) => {
    appraisalMisc.whatsappConfirmation.status = true;
    appraisalMisc.whatsappConfirmation.timestamp = new Date();

    console.log(appraisalMisc);
    axios
      .post(`appraisal/${appraisalId}/whatsapp-confirmation`, appraisalMisc)
      .then((appraisal) => {
        console.log(appraisal.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  openWhatsapp = (
    appraisalId,
    appraisalMisc,
    salesman,
    date,
    time,
    car,
    location,
    appraiser,
  ) => {
    let salesNum = '';
    if (
      salesman.whatsapp[0] === '0' ||
      (salesman.whatsapp[0] === '6' && salesman.whatsapp[1] === '2')
    ) {
      salesNum = salesNum + '+62';
    }
    if (
      (salesman.whatsapp.indexOf('-') !== -1 && salesman.whatsapp[0] === '0') ||
      (salesman.whatsapp.indexOf(' ') !== -1 && salesman.whatsapp[0] === '0')
    ) {
      for (let i = 1; i < salesman.whatsapp.length; i++) {
        if (salesman.whatsapp[i] !== '-' || salesman.whatsapp[i] !== ' ') {
          salesNum = salesNum + salesman.whatsapp[i];
        }
      }
    } else if (
      (salesman.whatsapp.indexOf('-') !== -1 && salesman.whatsapp[0] !== '0') ||
      (salesman.whatsapp.indexOf(' ') !== -1 && salesman.whatsapp[0] !== '0')
    ) {
      for (let i = 2; i < salesman.whatsapp.length; i++) {
        if (salesman.whatsapp[i] !== '-' || salesman.whatsapp[i] !== ' ') {
          salesNum = salesNum + salesman.whatsapp[i];
        }
      }
    } else if (salesman.whatsapp[0] !== '0') {
      salesNum = salesNum + salesman.whatsapp.slice(2);
    } else {
      salesNum = salesNum + salesman.whatsapp.slice(1);
    }

    this.confirmWhatsapp(appraisalId, appraisalMisc);
    // this.fetchAppraisalList();

    Linking.openURL(
      `https://wa.me/${salesNum}?text=Bpk/Ibu ${salesman.name}\n
      Terima kasih sudah booking appraisal di Toyota Trust. Saya hendak mengkonfirmasi ulang appraisal berikut ini:

      Hari/Tanggal: ${date}
      Jam: ${time}
      Mobil: ${car}
      Detail Lokasi: ${location}

      Regards,
      ${appraiser}
      Appraiser Toyota Trust`,
    );
  };

  render() {
    const {ticketList} = this.state;
    const {user} = this.props.Auth.api_token;

    return (
      <React.Fragment>
        {/* <View style={styles.badgeBox}>
          <Image source={require('../../assets/image/badge.png')} style={{width: widthPercentageToDP('9%'), height: widthPercentageToDP('9%') }}/>
        </View> */}

        <ScrollView>
          {/* <View style={styles.infoContainer}>
            <View style={styles.infoBox}>
              <Text style={styles.infoTitle}>2</Text>
              <Text style={styles.infoSubHead}>CARD BULAN INI</Text>
            </View>

            <View style={styles.infoBox}>
              <Text style={styles.infoTitle}>3</Text>
              <Text style={styles.infoSubHead}>CARD BULAN SELESAI</Text>
            </View>
          </View> */}

          <View style={styles.listContainer}>
            {console.log('ticket', ticketList)}
            {ticketList.length !== 0 ? (
              ticketList.map((value) => (
                <View>
                  {value.map((ticket, index) => (
                    <View style={styles.bookingItem} key={ticket.id}>
                      <TouchableOpacity
                      // onPress={() =>
                      //   this.props.requestAppraisalDetail(ticket.id)
                      // }
                      >
                        <View style={styles.noOrderBox}>
                          <Text style={styles.noOrder}>{ticket.id}</Text>

                          <Text style={styles.statusText}>
                            {ticket.name_error}
                          </Text>
                        </View>
                        <View style={styles.detailBook}></View>
                      </TouchableOpacity>
                      <View style={styles.salesProfile}>
                        <View
                          style={{
                            width: widthPercentageToDP('40%'),
                            justifyContent: 'center',
                          }}>
                          <Text style={styles.brandText}>
                            <Text>Error Desc: </Text>
                          </Text>
                          <Text style={styles.date}>{ticket.description}</Text>
                          <Text style={styles.brandText}>
                            <Text>updated at: </Text>
                          </Text>
                          <Text style={styles.date}>
                            {moment(
                              ticket.updated_at,
                              'YYYY-MM-DDTHH:mm',
                            ).format('llll')}
                          </Text>
                        </View>

                        {/* <TouchableOpacity
                      comment
                      style={[
                        styles.WAbutton,
                        !appraisal.misc.whatsappConfirmation.status
                          ? styles.WAactive
                          : styles.WAinactive,
                      ]}
                      style={[styles.WAbutton, styles.WAactive]}
                      comment
                      disabled={
                        !appraisal.misc.whatsappConfirmation.status
                          ? false
                          : true
                      }
                      onPress={() =>
                        this.openWhatsapp(
                          appraisal.id,
                          appraisal.misc,
                          appraisal.Booking.SalesProfile,
                          moment(
                            appraisal.Booking.bookingTime,
                            'YYYY-MM-DD',
                          ).format('ll'),
                          moment(
                            appraisal.Booking.bookingTime,
                            'YYYY-MM-DDTHH:mm',
                          ).format('LTS'),
                          appraisal.Booking.carType,
                          appraisal.Booking.notes,
                          appraisal.Booking.Appraiser.name,
                        )
                      }>
                      <Icon
                        name="whatsapp"
                        size={widthPercentageToDP('6%')}
                        color="white"
                      />
                      <Text style={styles.WAtext}>Whatsapp</Text>
                    </TouchableOpacity> */}
                      </View>
                    </View>
                  ))}
                </View>
              ))
            ) : (
              <View
                style={{
                  width: widthPercentageToDP('90%'),
                  height: heightPercentageToDP('50%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: colors.dope_blue,
                    fontSize: widthPercentageToDP('5%'),
                  }}>
                  Empty
                </Text>
              </View>
            )}
          </View>
        </ScrollView>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infoBox: {
    width: widthPercentageToDP('43.5%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderRadius: 5,
  },
  infoTitle: {
    color: colors.strong_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('10%'),
  },
  infoSubHead: {
    fontSize: widthPercentageToDP('3.5%'),
  },
  listContainer: {
    marginTop: widthPercentageToDP('4%'),
    paddingHorizontal: widthPercentageToDP('5%'),
  },
  bookingItem: {
    padding: widthPercentageToDP('4%'),
    backgroundColor: 'white',
    elevation: 4,
    shadowOffset: {width: 7, height: 5},
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 18,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('5%'),
  },
  noOrderBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: widthPercentageToDP('2%'),
  },
  noOrder: {
    fontSize: widthPercentageToDP('4.3%'),
    color: colors.strong_blue,
    fontWeight: 'bold',
  },
  statusText: {
    color: colors.cyan_green,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('4.3%'),
  },
  brandText: {
    fontSize: widthPercentageToDP('4.35%'),
    fontWeight: 'bold',
    color: colors.dope_blue,
  },
  date: {
    paddingBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4.3%'),
  },
  detailBook: {
    paddingBottom: widthPercentageToDP('2%'),
    borderColor: colors.light_sapphire_bluish_gray,
    borderBottomWidth: 1.5,
  },
  salesProfile: {
    paddingVertical: widthPercentageToDP('2%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  WAbutton: {
    width: widthPercentageToDP('40%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2.2%'),
    // backgroundColor: colors.whatsapp,
    flexDirection: 'row',
    borderRadius: 5,
  },
  WAtext: {
    color: 'white',
    fontSize: widthPercentageToDP('4.5%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  WAactive: {
    backgroundColor: colors.whatsapp,
  },
  WAinactive: {
    backgroundColor: colors.super_light_grayish_blue,
  },
  badgeBox: {
    position: 'absolute',
    top: 0,
    bottom: 100,
    zIndex: 100,
  },
});

const mapStateToProps = ({Auth, AppraisalDetail, AppraisalData}) => ({
  Auth,
  AppraisalDetail,
  AppraisalData,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // requestAppraisalDetail
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(ErrorList);
