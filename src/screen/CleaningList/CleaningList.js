import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Linking,
  StyleSheet,
  SafeAreaView,
  RefreshControl,
  AsyncStorage,
} from 'react-native';
import moment from 'moment';
import 'moment/locale/id';
import {
  resetApprasialData,
  setApprasialData,
} from '../../reducers/ApprasialData';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../utils/colors';
import {WebView} from 'react-native-webview';
import html_script from '../../utils/html_script';
import Spinner from 'react-native-loading-spinner-overlay';

import {heightPercentageToDP, widthPercentageToDP} from '../../utils';
// import {requestAppraisalDetail} from '../../actions';
import axios from '../../services/axios';
import logo from '../../assets/image/logo-trust.png';
moment.locale('id');
import UploadModalCleaningDetail from '../../component/Modal/UploadModalCleaningDetail';
import UploadModalCleaningRegister from '../../component/Modal/UploadModalCleaningRegister';
import SearchInput, {createFilter} from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['machine.bank.name_bank', 'machine.name_address', 'id'];

const BookingList = [
  {
    Booking: {
      noBooking: 'TR-092018-42',
      carType: 'AGYA',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-43',
      carType: 'Alphard',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-44',
      carType: 'Avanza',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
  {
    Booking: {
      noBooking: 'TR-092018-45',
      carType: 'Fortuner',
      bookingTime: moment(),
    },
    status: 'Terjadwal',
    action: 'Go',
  },
];

class CleaningList extends React.Component {
  constructor(props) {
    super(props);
    this._onPressCleaningDetail = this._onPressCleaningDetail.bind(this);
    this._toggleCleaningDetailModal = this._toggleCleaningDetailModal.bind(
      this,
    );
    this._onPressCleaningRegister = this._onPressCleaningRegister.bind(this);
    this._toggleCleaningRegisterModal = this._toggleCleaningRegisterModal.bind(
      this,
    );
    this._closeImage = this._closeImage.bind(this);
    this._showImage = this._showImage.bind(this);
    this.findListSearch = this.findListSearch.bind(this);
    this.state = {
      ticketList: [],
      valueListSearch: [],
      displayCleaningRegisterModal: false,
      displayCleaningDetailModal: false,
      refreshing: false,
      listSearchOnpress: false,
      document: {},
      dataTicketDetail: {},
      dataTicketDetailLebihDetail: {},
      searchValue: '',
      spinner: false,
      api_token: '',
    };
    // this.navigationWillFocusListener = props.navigation.addListener(
    //   'willFocus',
    //   () => {
    //     this.fetchAppraisalList();
    //   },
    // );
  }

  // componentWillUnmount() {
  //   this.navigationWillFocusListener.remove();
  //   setApprasialData([]);
  // }

  async componentDidMount() {
    // console.log('DATA ==', this.props.AppraisalData);
    try {
      let userData = await AsyncStorage.getItem('userData');
      if (userData) {
        let data = JSON.parse(userData);
        this.setState({
          api_token: data.api_token.api_token,
        });
      }
    } catch (err) {
      console.error(err);
      // console.log('Failed local login');
    }
    this.fetchTicketList();
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      displayCleaningRegisterModal,
      displayCleaningDetailModal,
    } = this.state;
    if (
      displayCleaningRegisterModal !== prevState.displayCleaningRegisterModal
    ) {
      this.fetchTicketList();
    }
    if (displayCleaningDetailModal !== prevState.displayCleaningDetailModal) {
      this.fetchTicketList();
    }
    // const {AppraisalDetail} = this.props;
    // if (prevProps.AppraisalDetail.onRequestDetailAppraisal) {
    //   if (AppraisalDetail.successDetailAppraisal) {
    //     this.props.navigation.navigate('Appraisal');
    //   } else if (AppraisalDetail.errorDetailAppraisal) {
    //     ToastAndroid.showWithGravity(
    //       'Gagal menampilkan detail appraisal',
    //       2000,
    //       ToastAndroid.BOTTOM,
    //     );
    //   } else {
    //     console.log('Something else ?', AppraisalDetail);
    //   }
    // }
  }

  fetchTicketList() {
    this.setState({
      spinner: true,
    });
    const {api_token} = this.props.Auth.api_token;

    axios
      .get(`/cleaning`, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        console.log('fetchCleaningList', result.data);
        // let filterResult = result.data.appraisalList.filter(
        //   (data) => data.status !== 'FINISHED',
        // );
        // console.log(filterResult);
        this.setState({
          ticketList: result.data,
          spinner: false,
        });
      })
      .catch((err) => {
        // console.log('ini error', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan list cleaning',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  confirmWhatsapp = (appraisalId, appraisalMisc) => {
    appraisalMisc.whatsappConfirmation.status = true;
    appraisalMisc.whatsappConfirmation.timestamp = new Date();

    console.log(appraisalMisc);
    axios
      .post(`appraisal/${appraisalId}/whatsapp-confirmation`, appraisalMisc)
      .then((appraisal) => {
        console.log(appraisal.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  _goToMyPosition = (lat, lon) => {
    // console.log('tike atm lokasi', lat, lon);
    this.refs['Map_Ref'].injectJavaScript(`
      mymap.setView([${lat}, ${lon}], 10)
      L.marker([${lat}, ${lon}]).addTo(mymap)
    `);
  };

  _onPressCleaningRegister(ticket) {
    this.setState({
      displayCleaningRegisterModal: true,
      dataTicketDetail: ticket,
    });
  }

  _onPressCleaningDetail(ticket) {
    this.setState({
      spinner: true,
    });
    const {api_token} = this.state;

    axios
      .get(`/cleaning_detail/${ticket.id}`, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        console.log('fetchCleaningDetail', result.data);
        this.setState({
          dataTicketDetail: ticket,
          dataTicketDetailLebihDetail: result.data,
          displayCleaningDetailModal: true,
          spinner: false,
        });
      })
      .catch((err) => {
        console.log('ini error fetchCleaningDetail', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan cleaning tambah',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          spinner: false,
        });
      });
  }

  _toggleCleaningRegisterModal() {
    this.setState({
      displayCleaningRegisterModal: !this.state.displayCleaningRegisterModal,
    });
  }

  _toggleCleaningDetailModal() {
    this.setState({
      displayCleaningDetailModal: !this.state.displayCleaningDetailModal,
    });
  }

  _showImage(imageUrl) {
    console.log('open image', imageUrl);
    this.setState({
      imageLink: imageUrl,
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  _closeImage() {
    console.log('close image');
    this.setState({
      viewImage: !this.state.viewImage,
      displayPhotoModal: !this.state.displayPhotoModal,
    });
  }

  searchUpdated(value) {
    this.setState({searchValue: value});
  }

  _onRefresh() {
    this.setState({refreshing: true});

    // console.log('this.props.Auth', this.props.Auth.api_token);
    const {api_token} = this.props.Auth.api_token;

    axios
      .get(`/cleaning`, {
        headers: {
          Authorization: `Bearer ${api_token}`,
        },
      })
      .then((result) => {
        console.log('list cleaning', result.data);
        this.setState({
          ticketList: result.data,
          refreshing: false,
        });
      })
      .catch((err) => {
        // console.log('ini error', err.response);
        ToastAndroid.showWithGravity(
          'Gagal menampilkan list cleaning',
          2000,
          ToastAndroid.BOTTOM,
        );
        this.setState({
          refreshing: false,
        });
      });
  }

  findListSearch(value) {
    const {ticketList, searchValue} = this.state;
	const tempFind = ticketList[0]?.filter(
      createFilter(searchValue, KEYS_TO_FILTERS)
	);

    this.setState({
      valueListSearch: tempFind,
      listSearchOnpress: true,
      searchValue: '',
      ticketList: [],
    });
  }

  render() {
    const {
      ticketList,
      displayCleaningRegisterModal,
      displayCleaningDetailModal,
      document,
      dataTicketDetail,
      dataTicketDetailLebihDetail,
      searchValue,
      refreshing,
      listSearchOnpress,
      valueListSearch,
      spinner,
    } = this.state;
    const {user} = this.props.Auth.api_token;
    const filteredValueSearch = ticketList.filter(
      createFilter(searchValue, KEYS_TO_FILTERS),
    );

    return (
      <React.Fragment>
        <Spinner
          visible={spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        {/* <View style={styles.badgeBox}>
          <Image source={require('../../assets/image/badge.png')} style={{width: widthPercentageToDP('9%'), height: widthPercentageToDP('9%') }}/>
        </View> */}

        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          {/* <View style={styles.infoContainer}>
            <View style={styles.infoBox}>
              <Text style={styles.infoTitle}>2</Text>
              <Text style={styles.infoSubHead}>CARD BULAN INI</Text>
            </View>

            <View style={styles.infoBox}>
              <Text style={styles.infoTitle}>3</Text>
              <Text style={styles.infoSubHead}>CARD BULAN SELESAI</Text>
            </View>
          </View> */}

          <View style={styles.listContainer}>
            <View style={styles.bookingItem}>
              <View style={styles.containerCardCleaningTambah}>
                <Text style={styles.textStyleCleaningAdd}>
                  Buat laporan cleaning baru
                </Text>
                <TouchableOpacity
                  style={[
                    styles.ButtonContainerStyle,
                    styles.ButtonGreenColorStyle,
                  ]}
                  onPress={() => this._onPressCleaningRegister()}>
                  <Icon
                    name="pencil"
                    size={widthPercentageToDP('6%')}
                    color="white"
                  />
                  <Text style={styles.ButtonTextStyle}>Tambah</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.containerSearch}>
              <SearchInput
                onChangeText={(value) => {
                  this.searchUpdated(value);
                }}
                style={styles.searchInput}
                placeholder="Type a to search"
              />
              {searchValue ? (
                <ScrollView>
                  {filteredValueSearch[0]?.length > 0 &&
                    filteredValueSearch[0]?.map((value) => {
                      return (
                        <TouchableOpacity
                          // onPress={() => alert(value.problem)}
                          onPress={() => this.findListSearch()}
                          key={value.id}
                          style={styles.valueItem}>
                          <View>
							<Text style={styles.noOrder}>{value.id}</Text>
                            <Text>{value.machine.bank.name_bank}</Text>
                            <Text style={styles.valueSubject}>
                              {value.machine.name_address}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                </ScrollView>
              ) : null}
            </View>
            {/* {console.log('ticket', ticketList)} */}
            {ticketList.length !== 0 ? (
              ticketList.map((value) => (
                <View>
                  {value.map((ticket, index) => (
                    <View style={styles.bookingItem} key={ticket.id}>
                      <TouchableOpacity
                        // onPress={() =>
                        //   this.props.requestAppraisalDetail(ticket.id)
                        // }
                        onPress={() => this._onPressCleaningDetail(ticket)}>
                        <View style={styles.noOrderBox}>
                          <Text style={styles.noOrder}>{ticket.id}</Text>

                          <Text style={styles.statusText}></Text>
                        </View>
                        <View style={styles.detailBook}>
                          <Text style={styles.date}>
                            {ticket.machine.machine_id} (
                            {ticket.machine.machine_id}{' '}
                            {ticket.machine.type.name_type}{' '}
                            {ticket.machine.machine_sn})
                          </Text>
                          <Text style={styles.brandText}>
                            <Text>{ticket.machine.name_address}</Text>
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  ))}
                </View>
              ))
            ) : listSearchOnpress ? (
              valueListSearch.map((ticket, index) => (
                <View style={styles.bookingItem} key={ticket.id}>
                  <TouchableOpacity
                    // onPress={() =>
                    //   this.props.requestAppraisalDetail(ticket.id)
                    // }
                    onPress={() => this._onPressCleaningDetail(ticket)}>
                    <View style={styles.noOrderBox}>
                      <Text style={styles.noOrder}>{ticket.id}</Text>

                      <Text style={styles.statusText}></Text>
                    </View>
                    <View style={styles.detailBook}>
                      <Text style={styles.date}>
                        {ticket.machine.machine_id} ({ticket.machine.machine_id}{' '}
                        {ticket.machine.type.name_type}{' '}
                        {ticket.machine.machine_sn})
                      </Text>
                      <Text style={styles.brandText}>
                        <Text>{ticket.machine.name_address}</Text>
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              ))
            ) : (
              <View
                style={{
                  width: widthPercentageToDP('90%'),
                  height: heightPercentageToDP('50%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: colors.dope_blue,
                    fontSize: widthPercentageToDP('5%'),
                  }}>
                  Empty
                </Text>
              </View>
            )}
          </View>
          {/* [START] MODAL CLEANING DETAIL */}
          <View style={styles.containerModal}>
            {displayCleaningDetailModal ? (
              <UploadModalCleaningDetail
                document={document}
                dataTicketDetail={dataTicketDetail}
                dataTicketDetailLebihDetail={dataTicketDetailLebihDetail}
                togglePhotoModal={this._toggleCleaningDetailModal}
                displayPhotoModal={displayCleaningDetailModal}
                previewPhoto={this._showImage}
              />
            ) : (
              false
            )}
          </View>
          {/* [END] MODAL CLEANING DETAIL */}
          {/* [START] MODAL CLEANING REGISTER */}
          <View style={styles.containerModal}>
            {displayCleaningRegisterModal ? (
              <UploadModalCleaningRegister
                document={document}
                dataTicketDetail={dataTicketDetail}
                togglePhotoModal={this._toggleCleaningRegisterModal}
                displayPhotoModal={displayCleaningRegisterModal}
                previewPhoto={this._showImage}
              />
            ) : (
              false
            )}
          </View>
          {/* [END] MODAL CLEANING REGISTER */}
        </ScrollView>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  ButtonTextStyle: {
    color: 'white',
    fontSize: widthPercentageToDP('3.5%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  ButtonContainerStyle: {
    width: widthPercentageToDP('25%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2.2%'),
    // backgroundColor: colors.whatsapp,
    flexDirection: 'row',
    borderRadius: 5,
  },
  ButtonGreenColorStyle: {
    backgroundColor: colors.green_insan,
  },
  findLocationATMButton: {
    backgroundColor: colors.strong_blue,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: colors.strong_blue,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
  },
  textFindLocationButton: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infoBox: {
    width: widthPercentageToDP('43.5%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderRadius: 5,
  },
  infoTitle: {
    color: colors.strong_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('10%'),
  },
  infoSubHead: {
    fontSize: widthPercentageToDP('3.5%'),
  },
  listContainer: {
    marginTop: widthPercentageToDP('4%'),
    paddingHorizontal: widthPercentageToDP('5%'),
  },
  bookingItem: {
    padding: widthPercentageToDP('4%'),
    backgroundColor: 'white',
    elevation: 4,
    shadowOffset: {width: 7, height: 5},
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 18,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('5%'),
  },
  noOrderBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: widthPercentageToDP('2%'),
  },
  noOrder: {
    fontSize: widthPercentageToDP('4.3%'),
    color: colors.strong_blue,
    fontWeight: 'bold',
  },
  textStyleCleaningAdd: {
    fontSize: widthPercentageToDP('4.3%'),
    color: colors.strong_grey,
    fontWeight: 'bold',
  },
  statusText: {
    color: colors.cyan_green,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('4.3%'),
  },
  brandText: {
    fontSize: widthPercentageToDP('4.35%'),
    fontWeight: 'bold',
    color: colors.dope_blue,
  },
  date: {
    paddingBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4.3%'),
  },
  detailBook: {
    paddingBottom: widthPercentageToDP('2%'),
    // borderColor: colors.light_sapphire_bluish_gray,
    // borderBottomWidth: 1.5,
  },
  containerCardCleaningTambah: {
    paddingVertical: widthPercentageToDP('2%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  WAbutton: {
    width: widthPercentageToDP('40%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2.2%'),
    // backgroundColor: colors.whatsapp,
    flexDirection: 'row',
    borderRadius: 5,
  },
  WAtext: {
    color: 'white',
    fontSize: widthPercentageToDP('4.5%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  WAactive: {
    backgroundColor: colors.whatsapp,
  },
  WAinactive: {
    backgroundColor: colors.super_light_grayish_blue,
  },
  badgeBox: {
    position: 'absolute',
    top: 0,
    bottom: 100,
    zIndex: 100,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  searchInput: {
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1,
  },
  containerSearch: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    marginBottom: widthPercentageToDP('5%'),
  },
  valueItem: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    padding: 10,
  },
  valueSubject: {
    color: 'rgba(0,0,0,0.5)',
  },
});

const mapStateToProps = ({Auth, AppraisalDetail, AppraisalData}) => ({
  Auth,
  AppraisalDetail,
  AppraisalData,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // requestAppraisalDetail
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(CleaningList);
