import React from 'react';
import Modal from 'react-native-modal';
import {
  Alert,
  Dimensions,
  Image,
  Picker,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Signature from 'react-native-signature-canvas';
import {useForm} from 'react-hook-form';
import styles from './styles';
import inputStyle from './inputStyle';
import {widthPercentageToDP} from '../../utils';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import services from './services';
import {connect} from 'react-redux';
import Accordion from '../../component/Accordion';
import RNFetchBlob from 'rn-fetch-blob';
import axios from 'axios';
import InputDate from '../../component/Input/InputDate';

function CreateBast(props) {
  const {visible, onClose, dataWorkStart, Auth} = props;
  const {setValue, register, handleSubmit, watch} = useForm();
  const [scrollEnabled, setScrollEnabled] = React.useState(true);
  const [images, setImages] = React.useState([]);
  const [signature, setSignature] = React.useState(null);
  const [signatureService, setSignatureService] = React.useState(null);
  const {api_token} = Auth.api_token;

  React.useEffect(() => {
    register('name_merchant');
    register('mid');
    register('tid');
    register('catatan');
    register('jumlah_edc');
    register('alamat');
    register('kategori');
    register('sn_number');
    register('sn_old');
    register('adapter');
    register('sim_card');
    register('sam_card');
    register('keterangan');
    register('name_merchant_signature');
    register('phone_merchant_signature');
    register('merchant_signature');

    register('name_service_signature');
    register('service_signature');
  }, []);

  function onSubmit(data) {
    const formData = new FormData();

    formData.append('nama_merchant', data.name_merchant);
    formData.append('alamat', data.alamat);
    formData.append('jmlh_edc', data.jumlah_edc);
    formData.append('kategori', data.kategori);
    formData.append('mid', data.mid);

    formData.append('tid', data.tid);
    formData.append('adapter', data.adapter);
    formData.append('sim_card', data.sim_card);
    formData.append('sam_card', data.sam_card);
    formData.append('keterangan', data.keterangan);

    formData.append('sn_number', data.sn_number);
    formData.append('catatan', data.catatan);

    images.map((itemImage, index) => {
      const imageId = index === 0 ? '' : index;
      formData.append(`img_edc${imageId}`, itemImage);
    });

    formData.append('nama_pihak_merchant', data.name_merchant_signature);
    formData.append('hp_merchant', data.phone_merchant_signature);
    formData.append('ttd_pihak_merchant', signature);

    formData.append('nama_pihak_service', data.name_service_signature);
    formData.append('ttd_pihak_service', signatureService);

    services.CreateBast(
      {
        id: dataWorkStart.ticket.id,
        formData,
        token: api_token,
      },
      {
        onSuccess: () => {
          Alert.alert('Info', 'Bast EDC created');
          onClose();
        },
        onError: () => {
          Alert.alert('Warning', 'Failed created Bast EDC');
        },
      },
    );
  }

  function handleSignatureMerchant(signature) {
    const path = RNFetchBlob.fs.dirs.DownloadDir + '/signature.png';
    RNFetchBlob.fs
      .writeFile(
        path,
        signature.replace('data:image/png;base64,', ''),
        'base64',
      )
      .then((res) => {
        RNFetchBlob.fs.stat(path).then((res) => {
          setSignature({
            uri: 'file://' + res.path,
            path: res.path,
            type: 'image/png',
            name: res.filename,
          });
        });
      });
  }

  function handleSignature(signature) {
    const path = RNFetchBlob.fs.dirs.DownloadDir + '/signatureService.png';
    RNFetchBlob.fs
      .writeFile(
        path,
        signature.replace('data:image/png;base64,', ''),
        'base64',
      )
      .then((res) => {
        RNFetchBlob.fs.stat(path).then((res) => {
          setSignatureService({
            uri: 'file://' + res.path,
            path: res.path,
            type: 'image/png',
            name: res.filename,
          });
        });
      });
  }

  const style = `.m-signature-pad--footer
    .button {
      background-color: #2F7CD8;
      color: #FFF;
    }`;

  function openCamera() {
    const options = {
      title: 'Upload foto buat BAST EDC',
      storageOptions: {
        path: 'images',
      },
      compressImageQuality: 0.1,
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        return;
      }

      if (response.error) {
        return;
      }

      setImages((prev) => {
        return [
          ...prev,
          {
            uri: response.uri,
            type: 'image/jpeg',
            name: response.fileName,
            path: response.path,
          },
        ];
      });
    });
  }

  function onDeleteImage(index) {
    setImages((prev) => {
      return prev.splice(index, 1);
    });
  }

  function beforeExit() {
    Alert.alert('Warning', 'Are you sure?', [
      {
        text: 'Cancel',
      },
      {
        text: 'OK',
        onPress: onClose,
      },
    ]);
  }

  return (
    <Modal
      isVisible={visible}
      style={styles.photoModal}
      onBackdropPress={() => beforeExit()}>
      <View
        style={
          Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
        }>
        <View style={styles.modalContainer}>
          <View style={styles.modalHeader}>
            <TouchableOpacity onPress={() => beforeExit()}>
              <Text
                style={{
                  color: 'white',
                  fontSize: widthPercentageToDP('8%'),
                  fontFamily: 'Roboto',
                }}>
                x
              </Text>
            </TouchableOpacity>
          </View>
          <ScrollView scrollEnabled={scrollEnabled}>
            <View style={styles.modalContent}>
              <Text style={styles.modalPhotoName}>Bast EDC</Text>
              <View style={{marginTop: 12}}>
                <Text style={inputStyle.label}>Tanggal</Text>
                <InputDate onChange={(text) => setValue('tgl_edc', text)} />

                <Text style={inputStyle.label}>Nama Merchant</Text>
                <TextInput
                  placeholder="please input the merchant name"
                  style={inputStyle.inputStyle}
                  autoCapitalize="none"
                  onChangeText={(text) => setValue('name_merchant', text)}
                />

                <Text style={inputStyle.label}>MID</Text>
                <TextInput
                  style={inputStyle.inputStyle}
                  placeholder="please input the merchant MID"
                  autoCapitalize="none"
                  onChangeText={(text) => setValue('mid', text)}
                />

                <Text style={inputStyle.label}>Alamat</Text>
                <TextInput
                  style={[inputStyle.inputStyle, {textAlignVertical: 'top'}]}
                  placeholder="please input address"
                  multiline
                  numberOfLines={3}
                  autoCapitalize="none"
                  onChangeText={(text) => setValue('alamat', text)}
                />

                <Text style={inputStyle.label}>Jumlah EDC</Text>
                <TextInput
                  style={inputStyle.inputStyle}
                  placeholder="please input the Jumlah EDC"
                  autoCapitalize="none"
                  onChangeText={(text) => setValue('jumlah_edc', text)}
                />

                <Text style={inputStyle.label}>Kategori</Text>
                <Picker
                  onValueChange={(text) => setValue('kategori', text)}
                  selectedValue={watch('kategori')}>
                  <Picker.Item label="Instalasi" value="Instalasi" />
                  <Picker.Item label="Migrasi" value="Migrasi" />
                  <Picker.Item label="Replacement" value="Replacement" />
                  <Picker.Item label="PM" value="PM" />
                  <Picker.Item label="CM Thermal" value="CM Thermal" />
                  <Picker.Item label="CM Problem" value="CM Problem" />
                  <Picker.Item label="Pullout" Value="Pullout" />
                </Picker>

                <Accordion label="EDC" style={{marginVertical: 10}}>
                  <Text style={inputStyle.label}>TID</Text>
                  <TextInput
                    style={inputStyle.inputStyle}
                    placeholder="please input tid"
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('tid', text)}
                  />

                  <Text style={inputStyle.label}>SN Number Baru</Text>
                  <TextInput
                    style={inputStyle.inputStyle}
                    placeholder="please input SN Number Baru"
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('sn_number', text)}
                  />
                  <Text style={inputStyle.label}>SN Number Lama</Text>
                  <TextInput
                    style={inputStyle.inputStyle}
                    placeholder="please input SN Number Lama"
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('sn_old', text)}
                  />

                  <Text style={inputStyle.label}>Adapter</Text>
                  <TextInput
                    style={inputStyle.inputStyle}
                    placeholder="please input adapter"
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('adapter', text)}
                  />

                  <Text style={inputStyle.label}>SIM Card</Text>
                  <TextInput
                    style={inputStyle.inputStyle}
                    placeholder="please input SIM Card"
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('sim_card', text)}
                  />

                  <Text style={inputStyle.label}>SAM Card</Text>
                  <TextInput
                    style={inputStyle.inputStyle}
                    placeholder="please input SAM Card"
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('sam_card', text)}
                  />

                  <Text style={inputStyle.label}>Keterangan</Text>
                  <TextInput
                    style={inputStyle.inputStyle}
                    placeholder="please input Keterangan"
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('keterangan', text)}
                  />
                </Accordion>

                <Text style={inputStyle.label}>Catatan</Text>
                <TextInput
                  placeholder="Catatan"
                  multiline
                  autoCapitalize="none"
                  style={inputStyle.inputStyle}
                  onChangeText={(text) => setValue('catatan', text)}
                />
                <Text style={inputStyle.label}>Images</Text>

                <View
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    marginBottom: 20,
                  }}>
                  {images.map((itemImage, index) => (
                    <View
                      style={[
                        {
                          display: 'flex',
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                          paddingRight: 10,
                        },
                        inputStyle.inputStyle,
                      ]}>
                      <View
                        style={{
                          display: 'flex',
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}>
                        <Image
                          source={{uri: itemImage?.uri}}
                          style={{
                            width: 60,
                            height: 60,
                            resizeMode: 'cover',
                            marginRight: 10,
                          }}
                        />
                        <Text style={inputStyle.label}>{itemImage?.name}</Text>
                      </View>
                      <TouchableOpacity onPress={() => onDeleteImage(index)}>
                        <Icon name="close" color="red" size={30} />
                      </TouchableOpacity>
                    </View>
                  ))}

                  <TouchableOpacity
                    style={[styles.button, styles.uploadPhotoButtonActive]}
                    onPress={() => openCamera()}>
                    <Icon
                      name="camera"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Tambah Photo</Text>
                  </TouchableOpacity>
                </View>

                <Text style={inputStyle.label}>Tanda Tangan Merchant</Text>
                <View style={{height: 300, marginBottom: 15}}>
                  <Signature
                    descriptionText="Sign"
                    onEmpty={() => console.log('empty')}
                    onBegin={() => setScrollEnabled(false)}
                    onEnd={() => setScrollEnabled(true)}
                    onOK={handleSignatureMerchant}
                    webStyle={style}
                    clearText="Clear"
                    confirmText="Save"
                  />
                </View>
                <Text style={inputStyle.label}>Name Signature Merchant</Text>
                <TextInput
                  style={inputStyle.inputStyle}
                  placeholder="please input name merchant"
                  autoCapitalize="none"
                  onChangeText={(text) =>
                    setValue('name_merchant_signature', text)
                  }
                />
                <Text style={inputStyle.label}>Phone Signature Merchant</Text>
                <TextInput
                  style={inputStyle.inputStyle}
                  placeholder="please input phone merchant"
                  autoCapitalize="none"
                  onChangeText={(text) =>
                    setValue('phone_merchant_signature', text)
                  }
                />

                <Text style={inputStyle.label}>
                  Tanda Tangan Merchant Service
                </Text>
                <View style={{height: 300, marginBottom: 10}}>
                  <Signature
                    descriptionText="Sign"
                    onEmpty={() => console.log('empty')}
                    onBegin={() => setScrollEnabled(false)}
                    onEnd={() => setScrollEnabled(true)}
                    onOK={handleSignature}
                    webStyle={style}
                    clearText="Clear"
                    confirmText="Save"
                  />
                </View>
                <Text style={inputStyle.label}>Name Merchant Service</Text>
                <TextInput
                  style={inputStyle.inputStyle}
                  placeholder="please input name merchant service"
                  autoCapitalize="none"
                  onChangeText={(text) =>
                    setValue('name_service_signature', text)
                  }
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  marginTop: 20,
                  marginBottom: 30,
                }}>
                <TouchableOpacity
                  style={[styles.viewPhotoButton, styles.viewPhotoButtonActive]}
                  onPress={() => onClose()}>
                  <Icon
                    name="close"
                    size={widthPercentageToDP('6%')}
                    color="white"
                  />
                  <Text style={styles.buttonText}>Batalkan</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[
                    styles.uploadPhotoButton,
                    styles.uploadPhotoButtonActive,
                  ]}
                  onPress={handleSubmit(onSubmit)}>
                  <Icon
                    name="save"
                    size={widthPercentageToDP('6%')}
                    color="white"
                  />
                  <Text style={styles.buttonText}>Simpan</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
}

const mapStateToProps = ({Auth}) => {
  return {
    Auth,
  };
};

export default connect(mapStateToProps, null)(CreateBast);
