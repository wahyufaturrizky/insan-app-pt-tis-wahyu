import axios from 'axios';

async function createBastEDC(params, callback) {
  const url = 'https://demo-kota.com/insan5/public/api/bast/' + params.id;
  const config = {
    method: 'post',
    data: params.formData,
    headers: {
      Authorization: `Bearer ${params.token}`,
      'Content-Type': 'multipart/form-data',
    },
  };

  try {
    const response = await axios(url, config);
    await callback?.onSuccess(response);
  } catch (error) {
    await callback?.onError(error.response);
  }
}

async function createReportBastEDC(params, callback) {
  const url = 'https://demo-kota.com/insan5/public/api/report/' + params.id;
  const config = {
    method: 'post',
    data: params.formData,
    headers: {
      Authorization: `Bearer ${params.token}`,
      'Content-Type': 'multipart/form-data',
    },
  };

  try {
    const response = await axios(url, config);
    await callback?.onSuccess(response);
  } catch (error) {
    await callback?.onError(error.response);
  }
}

export default {
  CreateBast: createBastEDC,
  CreateReportBastEDC: createReportBastEDC,
};
