/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {connect} from 'react-redux';
import Snackbar from '../../component/Snackbar';
import {get} from 'lodash';
import axios from '../../services/axios';
import {
  FlatList,
  View,
  Text,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import {widthPercentageToDP} from '../../utils';
import styles, {Color} from './styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import EmptyList from '../../component/EmptyList';
import UploadModalTicketConfirm from '../../component/Modal/UploadModalTicketConfirm';
import UploadModalTicketWorkStart from '../../component/Modal/UploadModalTicketWorkStart';
import UploadModalTicketPending from '../../component/Modal/UploadModalTicketPending';
import CreateBast from "../CreateBast";
import CreateReport from '../Report';

const arrayStatus = {
  'WORK START': Color.blue_insan,
  OPEN: Color.red_insan,
  CONFIRM: Color.orange_insan,
  PENDING: Color.blue_insan,
  CLOSE: Color.green_insan,
};

function checkStatus(arrayIdentifier, status = 'OPEN') {
  const keyIdentifier = Object.keys(arrayIdentifier);

  return keyIdentifier.indexOf(status) >= 0;
}

function pickerColor(arrayIdentifier, status = 'WORK START') {
  const keyIdentifier = Object.keys(arrayIdentifier);
  let resultColor = '';
  keyIdentifier.map((itemIdentifier) => {
    if (itemIdentifier === status) {
      resultColor = arrayIdentifier[status];
      return arrayIdentifier[status];
    }

    return null;
  });

  return resultColor;
}

const apiUrl = '/problem/paginate/10';

function ListProblem(props) {
  const {Auth} = props;
  const document = {};
  const apiToken = get(Auth, 'api_token.api_token');
  //STATE MODAL
  const [ticketConfirm, setTickeConfirm] = React.useState({
    modal: false,
    information: null,
  });
  const [ticketWorkStart, setTicketWorkStart] = React.useState({
    modal: false,
    information: null,
  });
  const [ticketPending, setTicketPending] = React.useState({
    modal: false,
    information: null,
  });
  const [bastEDC, setBastEDC] = React.useState({
    modal: false,
    information: null,
  });
  const [report, setReport] = React.useState({
    modal: false,
    information: null,
  });
  const [imageModal, setImageModal] = React.useState({
    imageLink: false,
    viewImage: false,
    displayPhotoModal: false,
  });
  const [refreshing, setRefreshing] = React.useState(false);
  const [isLoadMore, setisLoadMore] = React.useState(false);
  const [ticketList, setTicketList] = React.useState({
    list: [],
    isNext: false,
    total: 0,
    currentPage: 0,
  });

  React.useEffect(() => {
    _pullFromDatabase();
  }, []);

  async function _pullFromDatabase(page = 1, isMore = false) {
    if (!isMore) {
      setRefreshing(true);
    } else {
      setisLoadMore(true);
    }
    const config = {
      method: 'get',
      headers: {
        Authorization: `Bearer ${apiToken}`,
      },
      params: {
        page,
      },
    };

    try {
      const response = await axios(apiUrl, config);
      const {data, to, current_page, total} = response.data[0];
      if (!isMore) {
        setTicketList({
          list: data,
          isNext: to,
          total: total,
          currentPage: current_page,
        });
      } else {
        setTicketList((prev) => ({
          list: [...prev.list, ...data],
          isNext: to,
          total: total,
          currentPage: current_page,
        }));
      }
      if (!isMore) {
        setRefreshing(false);
      } else {
        setisLoadMore(false);
      }
    } catch (error) {
      Snackbar('Gagal Menampilkan List ATM');
    }
  }

  function showImage(imageUrl) {
    setImageModal((prev) => ({
      imageLink: imageUrl,
      viewImage: !prev.viewImage,
      displayPhotoModal: !prev.displayPhotoModal,
    }));
  }

  return (
    <View>
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => _pullFromDatabase(1, false)}
          />
        }
        contentContainerStyle={{
          flexGrow: 1,
          marginTop: widthPercentageToDP('4%'),
          paddingHorizontal: widthPercentageToDP('4%'),
        }}
        data={ticketList.list}
        initialNumToRender={10}
        onEndReached={() => _pullFromDatabase(ticketList.currentPage + 1, true)}
        onEndReachedThreshold={0.5}
        renderItem={({item}) => {
          const ticketId = get(item, 'ticket_id');
          const noProblem = get(item, 'id', 'Empty No. Problem');
          const ticketProblem = get(item, 'problem_status');
          const ticketNextAction = get(item, 'kelanjutan_problem', null);
          const machineId = get(item, 'ticket.machine.machine_id');
          const machineType = get(item, 'ticket.maintence_type');
          const machineSN = get(item, 'ticket.machine.machine_sn');
          const textProblem = get(item, 'problem');
          const color = pickerColor(arrayStatus, ticketProblem);
          const btnConfirm = ticketProblem === 'OPEN';
          const btnWorkStart = ticketProblem === 'CONFIRM';
          const btnPending = ticketProblem === 'WORK START';
          const colorButton =
            ticketProblem === 'OPEN'
              ? styles.ButtonOrangeStyle
              : styles.ButtonInactive;
          const colorButtonWorkStart = btnWorkStart
            ? styles.ButtonBlueColortyle
            : styles.ButtonInactive;
          const colorButtonSecondary = btnPending
            ? styles.ButtonBlueColortyle
            : styles.ButtonInactive;

          return (
            <View style={styles.bookingItem}>
              <View style={styles.noOrderBox}>
                <Text style={styles.noOrder}>
                  Ticket : {ticketId} / Problem: {noProblem}
                </Text>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: widthPercentageToDP('3%'),
                    color,
                  }}>
                  {ticketProblem} {ticketNextAction}
                </Text>
              </View>
              <View style={styles.detailBook}>
                <Text style={styles.date}>
                  {machineId} ({machineType} {machineSN})
                </Text>
                <Text style={styles.brandText}>
                  <Text>{textProblem}</Text>
                </Text>
              </View>
              <View style={styles.salesProfile}>
                <TouchableOpacity
                  style={[styles.ButtonContainerStyle, colorButton]}
                  disabled={!btnConfirm}
                  onPress={() =>
                    setTickeConfirm({modal: true, information: item})
                  }>
                  <Icon
                    name="check"
                    size={widthPercentageToDP('6%')}
                    color="white"
                  />
                  <Text style={styles.ButtonTextStyle}>Confirm</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.ButtonContainerStyle, colorButtonWorkStart]}
                  disabled={!btnWorkStart}
                  onPress={() =>
                    setTicketWorkStart({modal: true, information: item})
                  }>
                  <Icon
                    name="clock-o"
                    size={widthPercentageToDP('6%')}
                    color="white"
                  />
                  <Text style={styles.ButtonTextStyle}>Work Start</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.ButtonContainerStyle, colorButtonSecondary]}
                  disabled={!btnPending}
                  onPress={() =>
                    setTicketPending({modal: true, information: item})
                  }>
                  <Icon
                    name="gears"
                    size={widthPercentageToDP('6%')}
                    color="white"
                  />
                  <Text style={styles.ButtonTextStyle}>Work Finish</Text>
                </TouchableOpacity>
              </View>
              <View
                style={{display: 'flex', flexDirection: 'row', width: '100%'}}>
                <TouchableOpacity
                  style={[
                    styles.ButtonContainerStyle,
                    styles.ButtonBlueColortyle,
                    {marginRight: 20},
                  ]}
                  disabled={!bastEDC}
                  onPress={() => setBastEDC({modal: true, information: item})}>
                  <Icon
                    name="plus"
                    size={widthPercentageToDP('6%')}
                    color="white"
                  />
                  <Text style={styles.ButtonTextStyle}>Bast EDC</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.ButtonContainerStyle, styles.ButtonBlueColortyle]}
                  disabled={!report}
                  onPress={() => setReport({modal: true, information: item})}>
                  <Icon
                    name="file"
                    size={widthPercentageToDP('5%')}
                    color="white"
                  />
                  <Text style={styles.ButtonTextStyle}>Report</Text>
                </TouchableOpacity>
              </View>
            </View>
          );
        }}
        ListEmptyComponent={<EmptyList />}
      />
      <View>
        {ticketConfirm.modal && (
          <UploadModalTicketConfirm
            document={document}
            dataConfirm={ticketConfirm.information}
            togglePhotoModal={() =>
              setTickeConfirm((prev) => ({modal: !prev.modal}))
            }
            displayPhotoModal={ticketConfirm.modal}
            onSuccess={() => _pullFromDatabase(1)}
          />
        )}
      </View>
      <View>
        {ticketWorkStart.modal && (
          <UploadModalTicketWorkStart
            document={document}
            dataWorkStart={ticketWorkStart.information}
            togglePhotoModal={() =>
              setTicketWorkStart((prev) => ({modal: !prev.modal}))
            }
            displayPhotoModal={ticketWorkStart.modal}
            previewPhoto={showImage}
            onSuccess={() => _pullFromDatabase(1)}
          />
        )}
      </View>
      <View>
        {ticketPending.modal && (
          <UploadModalTicketPending
            document={document}
            dataWorkStart={ticketPending.information}
            togglePhotoModal={() => {
              setTicketPending((prev) => ({modal: !prev.modal}));
            }}
            displayPhotoModal={ticketPending.modal}
            previewPhoto={showImage}
            onSuccess={() => _pullFromDatabase(1)}
          />
        )}
      </View>
      <View>
        {bastEDC.modal && (
          <CreateBast
            visible={bastEDC.modal}
            dataWorkStart={bastEDC.information}
            onClose={() => setBastEDC({modal: false, information: null})}
          />
        )}
      </View>
      <View>
        {report.modal && (
          <CreateReport
            visible={report.modal}
            dataWorkStart={report.information}
            onClose={() => setReport({ modal: false, information: null })}
          />
        )}
      </View>
    </View>
  );
}

const mapStateToProps = ({Auth, AppraisalDetail, AppraisalData}) => ({
  Auth,
  AppraisalDetail,
  AppraisalData,
});

const withConnect = connect(mapStateToProps, null);

export default withConnect(ListProblem);
