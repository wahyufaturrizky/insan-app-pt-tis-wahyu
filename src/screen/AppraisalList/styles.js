import {StyleSheet} from 'react-native';
import {widthPercentageToDP} from '../../utils';
import Color from '../../utils/colors';
export {default as Color} from '../../utils/colors';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infoBox: {
    width: widthPercentageToDP('43.5%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderRadius: 5,
  },
  infoTitle: {
    color: Color.strong_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('10%'),
  },
  infoSubHead: {
    fontSize: widthPercentageToDP('3.5%'),
  },
  listContainer: {
    marginTop: widthPercentageToDP('4%'),
    paddingHorizontal: widthPercentageToDP('5%'),
  },
  bookingItem: {
    padding: widthPercentageToDP('4%'),
    backgroundColor: 'white',
    elevation: 4,
    shadowOffset: {width: 7, height: 5},
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 18,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('5%'),
  },
  noOrderBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: widthPercentageToDP('2%'),
  },
  noOrder: {
    fontSize: widthPercentageToDP('3%'),
    color: Color.strong_blue,
    fontWeight: 'bold',
  },
  brandText: {
    fontSize: widthPercentageToDP('4.35%'),
    fontWeight: 'bold',
    color: Color.dope_blue,
  },
  date: {
    paddingBottom: widthPercentageToDP('4%'),
    color: Color.dope_blue,
    fontSize: widthPercentageToDP('4.3%'),
  },
  detailBook: {
    paddingBottom: widthPercentageToDP('2%'),
    borderColor: Color.light_sapphire_bluish_gray,
    borderBottomWidth: 1.5,
  },
  salesProfile: {
    paddingVertical: widthPercentageToDP('2%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  ButtonContainerStyle: {
    width: widthPercentageToDP('25%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2.2%'),
    // backgroundColor: Color.whatsapp,
    flexDirection: 'row',
    borderRadius: 5,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  ButtonTextStyle: {
    color: 'white',
    fontSize: widthPercentageToDP('3.5%'),
    fontWeight: 'bold',
    marginLeft: 3,
  },
  WAactive: {
    backgroundColor: Color.whatsapp,
  },
  ButtonInactive: {
    backgroundColor: Color.super_light_grayish_blue,
  },
  ButtonOrangeStyle: {
    backgroundColor: Color.orange_insan,
  },
  ButtonBlueColortyle: {
    backgroundColor: Color.blue_insan,
  },
  badgeBox: {
    position: 'absolute',
    top: 0,
    bottom: 100,
    zIndex: 100,
  },
  findLocationATMButton: {
    backgroundColor: Color.strong_blue,
    marginTop: widthPercentageToDP('4%'),
    borderRadius: 6,
    borderColor: Color.strong_blue,
    borderWidth: 2,
    width: '100%',
    paddingVertical: widthPercentageToDP('2%'),
    alignItems: 'center',
  },
  textFindLocationButton: {
    color: '#fff',
    fontWeight: '800',
    textAlign: 'center',
    fontSize: widthPercentageToDP('5%'),
  },
  searchInput: {
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1,
  },
  containerSearch: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    marginBottom: widthPercentageToDP('5%'),
  },
  valueItem: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    padding: 10,
  },
  valueSubject: {
    color: 'rgba(0,0,0,0.5)',
  },
});
export default styles;
