import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { HeaderBackButton } from "react-navigation-stack";
import Datepicker from "react-native-datepicker";
import { Text, View, ScrollView, TextInput, Picker, StyleSheet } from "react-native";
import RadioGroup from "react-native-radio-buttons-group";
import _ from 'lodash'

import axios from "../../services/axios";
import { updateAppraisalData } from "../../actions/appraisal_actions";
import colors from '../../utils/colors'
import { widthPercentageToDP, heightPercentageToDP } from '../../utils/index'

class Summary extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: () => <HeaderBackButton onPress={() => navigation.navigate("AppraisalList")} />,
    title: "Summary",
  });

  state = {
    other: false,
    Brand: [],
    Model: [],
    Tipe: [],
    Transmisi: [
      { label: "AT", id: "AT" },
      { label: "MT", id: "MT" },
    ],
    KategoriWarna: [],
    "STNK Hidup": [{ label: "Masih Hidup", id: "hidup" }, { label: "Sudah Mati", id: "mati" }],
    "Kategori Warna": [{ label: "Black/White", id: 0 }, { label: "Non Black/White", id: 1 }],
    // "Kategori Warna": [],
    Kecelakaan: [
      { label: "Tidak ada Kecelakaan", id: 0 },
      { label: "Kecelakaan, Tidak Kena Rangka Utama", id: 1 },
      { label: "Kecelakaan, Kena Rangka Utama", id: 2 },
    ],
    Banjir: [{ label: "Tidak Kena Banjir", id: 0 }, { label: "Kena Banjir", id: 1 }],
    Paint: [
      { label: "Tidak Ada", id: 0 },
      { label: "1 Panel", id: 1 },
      { label: "2 Panel", id: 2 },
      { label: "3 Panel", id: 3 },
      { label: "4 Panel", id: 4 },
      { label: "5 Panel", id: 5 },
      { label: "6 Panel", id: 6 },
      { label: "7 Panel", id: 7 },
      { label: "All Body", id: 8 },
    ],
    Wheel: [
      { label: "Tidak Perlu Ganti Ban", id: 0 },
      { label: "1 Ban", id: 1 },
      { label: "2 Ban", id: 2 },
      { label: "3 Ban", id: 3 },
      { label: "4 Ban", id: 4 },
      { label: "5 Ban", id: 5 },
    ],
    "Kebersihan Interior": [
      {label: "Bersih",
      id: 0},
      {label: "Kotor Min",
      id: 1},
      {label: "Kotor Med",
      id: 2},
      {label: "Kotor Max",
      id: 3}
    ],
    STNK: [{ label: "Ada", id: 0 }, { label: "Tidak Ada", id: 1 }],
    Faktur: [{ label: "Ada", id: 0 }, { label: "Tidak Ada", id: 1 }],
    "Kunci Serep": [
      { label: "Ada", id: 0 },
      { label: "Tidak Ada (Biasa)", id: 1 },
      { label: "Tidak Ada (Keyless)", id: 2 },
    ],
    "NIK/Form A": [{ label: "Ada", id: 0 }, { label: "Tidak Ada", id: 1 }],
    "Kepemilikan STNK": [{ label: "Pribadi", id: 0 }, { label: "Perusahaan", id: 1 }],
    Tahun: [
      {label: "2005", 
      id: 2005},
      {label: "2006", 
      id: 2006},
      {label: "2007", 
      id: 2007},
      {label: "2008", 
      id: 2008},
      {label: "2009", 
      id: 2009},
      {label: "2010", 
      id: 2010},
      {label: "2011", 
      id: 2011},
      {label: "2012", 
      id: 2012},
      {label: "2013", 
      id: 2013},
      {label: "2014", 
      id: 2014},
      {label: "2015", 
      id: 2015},
      {label: "2016", 
      id: 2016},
      {label: "2017", 
      id: 2017},
      {label: "2018", 
      id: 2018},
      {label: "2019", 
      id: 2019},
      {label: "2020", 
      id: 2020}
    ],
    'Mobil Terawat': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Sealant': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Matic': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Solar': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Rangka': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    Mesin: [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'BPKB': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'KTP Sesuai STNK': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Masih Leasing': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Polis Asuransi': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Buku Manual': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Buku Servis': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Surat Pelepasan Hak': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'TDP': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'SIUP': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'NPWP': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Domisili': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Surat Perpanjangan STNK': [
      {label: 'Ya',
      id: 1},
      {label: 'Tidak',
      id: 1}
    ],
    'Kesimpulan': [
      {label: 'Layak',
      id: 1},
      {label: 'Tidak Layak',
      id: 1}
    ],
    carDetail: []
  };

  componentDidMount() {
    let { carSummary } = this.props.AppraisalDetail;
    const { updateAppraisalData } = this.props;
    if (this.props.AppraisalDetail.carSummary[0].value === "") {
      this.setState({
        other: true,
      });
    }


    // this.setState({
    //   carDetail: this.props.AppraisalDetail.carDetail,
    // })

    this.fetchBrand();
    this.fetchColor();

    let idx = _.findIndex(carSummary, {fieldName: "STNK Hidup"})

    updateAppraisalData("carSummary", idx, {
      fieldName: "STNK Hidup",
      value: "aktif",
      type: "select"
    });

    carSummary.map((item, idx) => {
      if(item.type == 'select'){
        if(item.fieldName != "Brand" && item.fieldName != "Tipe" && item.fieldName != "Model"){
          if(item.value == ""){
            let val = this.state[item.fieldName][0]
            updateAppraisalData("carSummary", idx, {
              fieldName: item.fieldName,
              value: val.id,
              type: "select"
            });
          }
        } else if(item.fieldName == "Brand"){
          if(item.value != ""){
            this.fetchType(item.value)
          }
        } else if(item.fieldName == "Model"){
          if(item.value != 0){
            this.fetchSpec(item.value)
          }
        }
      }
    })
    
  }

  fetchBrand = () => {
    axios
      .get("/value-car/brand")
      .then(result => {
        let carBrandList = result.data.carBrand;
        this.setState({
          Brand: [...carBrandList, { brand: "Brand tidak ada di list", id: "" }],
          Model: [],
          Tipe: [],
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchType = id => {
    axios
      .get(`/value-car/brand/${id}`)
      .then(result => {
        this.setState({
          Model: [...result.data.carType, { carType: "Pilih Model", id: ""}],
          Tipe: [],
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchSpec = id => {
    axios
      .get(`/value-car/type/${id}`)
      .then(result => {
        this.setState({
          Tipe: [...result.data.carSpec, { carSpec: "Pilih Tipe", id: "" }],
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchColor = () => {
    axios
      .get(`/misc/colors`)
      .then(result => {
        this.setState({
          KategoriWarna: result.data.kategori_warna,
        });
        // this.state["Kategori Warna"] = result.data.fieldName
        console.log(result)
      })
      .catch(err => {
        console.log(err);
      });
  };

  onChangeBrand = (index, id, field) => {
    if (id !== "") {
      this.setState({
        other: false,
      });
      this.fetchType(id);
    } else {
      this.setState({
        other: true,
        Model: [],
        Tipe: [],
      });
    }
    this.onChangeField(index, field, id);
  };

  onChangeType = (index, id, field) => {
    if (id !== "") {
      this.fetchSpec(id);
    }
    this.onChangeField(index, field, id);
  };

  onChangeField(index, field, value) {
    const { updateAppraisalData } = this.props;
    if ( field.fieldName === "Nilai STNK" || field.fieldName === "Penawaran Sebelumnya" || field.fieldName === "Ekspektasi Customer") {
      let newValue = value.split(".").join("")
      field.value = Number(newValue).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    } else {
      field.value = value;
    }
    console.log("SUMMARY", field)
    updateAppraisalData("carSummary", index, field);
  }

  onChangeFieldDetail(index, field, value) {
    const { updateAppraisalData } = this.props;
    if ( field.fieldName === "Nilai STNK" || field.fieldName === "Penawaran Sebelumnya" || field.fieldName === "Ekspektasi Customer") {
      let newValue = value.split(".").join("")
      field.value = Number(newValue).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    } else {
      field.value = value;
    }
    console.log("oit", field)
    updateAppraisalData("carDetail", index, field);
    // console.log("oit", value)
  }

  ValueSelectChange = (index, value, field) => {
    console.log(value)
    if (field.fieldName === "Brand") {
      let { carSummary } = this.props.AppraisalDetail;
      let data = _.find(this.state.Brand, {id: value})
      let other = _.find(carSummary, {fieldName: "Other"})
      let otheridx = _.findIndex(carSummary, {fieldName: "Other"})

      // console.log("== Others ", other)

      var fieldDetail = {
        fieldName: "Merk Mobil",
        value: field.value,
        type: field.type
      }

      // console.log("== BRAND CHANGED")
      this.onChangeField(otheridx, other, "")
      this.onChangeFieldDetail(3, fieldDetail, `${data.brand}`)
      this.onChangeBrand(index, value, field);
    } else if (field.fieldName === "Model") {
      let data = _.find(this.state[field.fieldName], {id: value})

      var fieldDetail = {
        fieldName: "Model",
        value: field.value,
        type: field.type
      }

      console.log("model", fieldDetail)
      this.onChangeType(index, value, field);

      
      if(data != undefined){
        this.onChangeFieldDetail(4, fieldDetail, `${data.carType}`)
      } else {
        this.onChangeFieldDetail(4, fieldDetail, "")
      }
    } else if(field.fieldName === "Tahun") {
      let data = _.find(this.state[field.fieldName], {id: value})
      this.onChangeFieldDetail(6, field, `${data.label}`)
      this.onChangeField(index, field, value)
    } else if(field.fieldName === "Tipe") {
      let data = _.find(this.state[field.fieldName], {id: value})
      var fieldDetail = {
        fieldName: "Type",
        value: field.value,
        type: field.type
      }

      console.log("Tipe", data)
      this.onChangeField(index, field, value)
      if(data != undefined){
        this.onChangeFieldDetail(5, fieldDetail, `${data.carSpec}`)
      } else {
        this.onChangeFieldDetail(5, fieldDetail, "")
      }
    } else if(field.fieldName === "Kategori Warna") {
      let data = _.find(this.state.KategoriWarna, {id: value})
      // this.fetchColor()
      var fieldDetail = {
        fieldName: "Warna Eksterior",
        code: field.code,
        value: field.value,
        type: field.type
      }

      this.onChangeFieldDetail(10, fieldDetail, `${data.label}`)
      this.onChangeField(index, field, value)
    } else {
      this.onChangeField(index, field, value);
    }

    // if(field.fieldName === "Tahun"){
    //   let data = _.find(this.state[field.fieldName], {id: value})

    //   this.onChangeFieldDetail(index, field, data.label)
    // }
  };

  labelSelect = field => {
    if (field === "Model") {
      return <Picker.Item key={0} label={"Model tidak ada di list"} value="0" />;
    } else if (field === "Tipe") {
      return <Picker.Item key={0} label={"Tipe tidak ada di list"} value="0" />;
    } else {
      return <Picker.Item key={0} label={"Data Kosong"} value="0" />;
    }
  };

  chooseLabel = (option, field) => {
    if (field === "Brand") {
      return <Picker.Item key={option.id} label={option.brand} value={option.id} />;
    } else if (field === "Model") {
      return <Picker.Item key={option.id} label={`${option.carType}`} value={option.id} />;
    } else if (field === "Tipe") {
      return <Picker.Item key={option.id} label={`${option.carSpec}`} value={option.id} />;
    } else {
      return <Picker.Item key={option.id} label={option.label} value={option.id} />;
    }
  };

  chooseInputBox = (detail, carSummary, index) => {
    if (detail.fieldName === "Other") {
      return (
        <TextInput
          autoCapitalize="words"
          placeholder="(Cth: Toyota Harrier G/AT)"
          style={styles.inputForm}
          editable={ this.state.other ? true: false}
          keyboardType={detail.type === "number" ? "number-pad" : "default"}
          onChangeText={text => this.onChangeField(index, detail, text)}
          defaultValue={carSummary[index]["value"]}
          returnKeyType="next"
        />
      );
    } else if (detail.fieldName === "Nilai STNK") {
      return (
        <TextInput
          autoCapitalize="words"
          placeholder="Jumlah Pajak yang harus dibayar"
          style={styles.inputForm}
          keyboardType={detail.type === "number" ? "number-pad" : "default"}
          onChangeText={text => this.onChangeField(index, detail, text)}
          defaultValue={carSummary[index]["value"]}
          returnKeyType="next"
        />
      );
    } else {
      return (
        <TextInput
          autoCapitalize="words"
          style={styles.inputForm}
          keyboardType={detail.type === "number" ? "number-pad" : "default"}
          onChangeText={text => this.onChangeField(index, detail, text)}
          defaultValue={carSummary[index]["value"]}
          returnKeyType="next"
        />
      );
    }
  };
å

  render() {
    let { carSummary, carDetail } = this.props.AppraisalDetail;
    // const { carDetail } = this.state;
    // console.log("WARNA ",this.state.KategoriWarna)
    return (
      <View style={styles.container}>
        {
          this.state.KategoriWarna.length > 0 && (
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                {carDetail.map((detail, index) => index%2 == 0 && detail.fieldName != "Model" && detail.fieldName != "Merk Mobil" && detail.fieldName != "Type" && detail.fieldName != "Tahun" && detail.fieldName != "Warna Eksterior" && (
                  <View style={styles.inputGroup} key={index}>
                    {detail.fieldName === "E-mail Customer" ? (
                      <Text style={styles.inputLabel}>{`${detail.fieldName}`}</Text>
                    ) : detail.fieldName === "Tahun STNK" ? <Text style={styles.inputLabel}>Pajak STNK *</Text> 
                    : (
                      <Text style={styles.inputLabel}>{detail.fieldName} *</Text>
                    )}
                    {["Tahun STNK", "STNK 5 Tahunan"].includes(detail.fieldName) ? (
                      <Datepicker
                        style={styles.inputForm}
                        mode="date"
                        date={detail.value}
                        placeholder=""
                        androidMode="spinner"
                        confirmBtnText="Oke"
                        cancelBtnText="Batal"
                        onDateChange={date => this.onChangeFieldDetail(index, detail, date)}
                        format="YYYY-MM-DD"
                        showIcon={false}
                        customStyles={{
                          dateInput: {
                            borderWidth: 0,
                            textAlign: "left",
                            alignItems: "flex-start",
                            paddingLeft: 3,
                          },
                        }}
                      />
                    ) : (
                      <View>
                        <TextInput
                          autoCapitalize="words"
                          style={styles.inputForm}
                          keyboardType={["Tahun", "Jarak Tempuh", "No. HP Customer"].includes(detail.fieldName) ? "number-pad" : "default"}
                          onChangeText={text => this.onChangeFieldDetail(index, detail, text)}
                          defaultValue={carDetail[index]["value"]}
                          returnKeyType="next"
                        />
                        {!!this.state.nameError && <Text style={{ color: red }}>{this.state.nameError}</Text>}
                      </View>
                    )}
                  </View>
                ))}

                {carSummary.map((detail, index) => index%2 == 0 && detail.fieldName != "STNK Hidup" &&  (
                  <View style={styles.inputGroup} key={index}>
                    {
                      index === 18 ? <Text style={{fontSize: 23, marginBottom: 5}}>General Info Section</Text> : null
                    } 
                    {
                      index === 24 ? <Text style={{fontSize: 23, marginBottom: 5}}>Dokumentasi</Text> : null
                    }
                    {
                      index === 30 ? <Text style={{fontSize: 23, marginBottom: 5}}>PT (original)</Text> : null
                    }
                    { detail.fieldName === "Other" && !this.state.other || detail.fieldName === "Penawaran Sebelumnya" || detail.fieldName === "Ekspektasi Customer" ? (
                      <Text style={styles.inputLabel}>{`${detail.fieldName} (Opsional)`}</Text>
                    ) : (
                      <Text style={styles.inputLabel}>{detail.fieldName} *</Text>
                    )}
                    {detail.type === "date" ? (
                      <Datepicker
                        style={styles.inputForm}
                        mode="date"
                        date={detail.value}
                        placeholder=""
                        androidMode="spinner"
                        confirmBtnText="Oke"
                        cancelBtnText="Batal"
                        onDateChange={date => this.onChangeField(index, detail, date)}
                        format="YYYY-MM-DD"
                        showIcon={false}
                        customStyles={{
                          dateInput: {
                            borderWidth: 0,
                            textAlign: "left",
                            alignItems: "flex-start",
                            paddingLeft: 3,
                          },
                        }}
                      />
                    ) : null}
                    {detail.type === "text" || detail.type === "number" ? (
                      <View>
                        {this.chooseInputBox(detail, carSummary, index)}
                        {!!this.state.nameError && <Text style={{ color: red }}>{this.state.nameError}</Text>}
                      </View>
                    ) : null}
                    {detail.type === "radio" ? (
                      <View style={{ flex: 1, alignItems: "flex-start" }}>
                        <RadioGroup
                          radioButtons={[
                            {
                              label: "Yes",
                              value: true,
                            },
                            {
                              label: "No",
                              value: false,
                            },
                          ]}
                          flexDirection="row"
                          onPress={data => this.onChangeField(index, detail, data)}
                        />
                      </View>
                    ) : null}
                    {detail.type === "select" ? (
                      <View style={styles.inputForm}>
                        {
                          detail.fieldName === "Kategori Warna" ?
                          (
                            <Picker
                              selectedValue={detail.value}
                              
                              mode="dropdown"
                              onValueChange={value => this.ValueSelectChange(index, value, detail)}
                            >
                              {this.state.KategoriWarna.length > 0
                                ? (<Picker.Item key={0} label={`Pilih ${detail.fieldName}`} value="0" />,
                                  this.state.KategoriWarna.map(option => this.chooseLabel(option, detail.fieldName)))
                                : this.labelSelect(detail.fieldName)}
                            </Picker>
                          ) :
                          (
                            <Picker
                            selectedValue={detail.value}
                            
                            mode="dropdown"
                            onValueChange={value => this.ValueSelectChange(index, value, detail)}
                          >
                            {/* {
                              detail.fieldName !== "Brand" &&
                              <Picker.Item key={0} label={`Pilih ${detail.fieldName}`} value="" />

                            } */}
                            {this.state[detail.fieldName].length > 0
                              ? (<Picker.Item key={0} label={`Pilih ${detail.fieldName}`} value="0" />,
                                this.state[detail.fieldName].map(option => this.chooseLabel(option, detail.fieldName)))
                              : this.labelSelect(detail.fieldName)}
                          </Picker>
                          )
                        }
                      </View>
                    ) : null}
                  </View>
                ))}
                </View>
                <View style={{flex: 1}}>
                  
                {carDetail.map((detail, index) => index%2 != 0 && detail.fieldName != "Model" && detail.fieldName != "Merk Mobil" && detail.fieldName != "Type" && detail.fieldName != "Tahun" && detail.fieldName != "Warna Eksterior" && (
                  <View style={styles.inputGroup} key={index}>
                    {detail.fieldName === "E-mail Customer" ? (
                      <Text style={styles.inputLabel}>{`${detail.fieldName} (Opsional)`}</Text>
                    ) : (
                      <Text style={styles.inputLabel}>{detail.fieldName} *</Text>
                    )}
                    {["Tahun STNK", "STNK 5 Tahunan"].includes(detail.fieldName) ? (
                      <Datepicker
                        style={styles.inputForm}
                        mode="date"
                        date={detail.value}
                        placeholder=""
                        androidMode="spinner"
                        confirmBtnText="Oke"
                        cancelBtnText="Batal"
                        onDateChange={date => this.onChangeFieldDetail(index, detail, date)}
                        format="YYYY-MM-DD"
                        showIcon={false}
                        customStyles={{
                          dateInput: {
                            borderWidth: 0,
                            textAlign: "left",
                            alignItems: "flex-start",
                            paddingLeft: 3,
                          },
                        }}
                      />
                    ) : (
                      <View>
                        <TextInput
                          autoCapitalize="words"
                          style={styles.inputForm}
                          keyboardType={["Tahun", "Jarak Tempuh", "No. HP Customer"].includes(detail.fieldName) ? "number-pad" : "default"}
                          onChangeText={text => this.onChangeFieldDetail(index, detail, text)}
                          defaultValue={carDetail[index]["value"]}
                          returnKeyType="next"
                        />
                        {!!this.state.nameError && <Text style={{ color: red }}>{this.state.nameError}</Text>}
                      </View>
                    )}
                  </View>
                ))}
                {carSummary.map((detail, index) => index%2 != 0 && detail.fieldName != "STNK Hidup" && (
                  <View style={styles.inputGroup} key={index}>
                    {
                      index === 18 ? <Text style={{fontSize: 23, marginBottom: 5}}>General Info Section</Text> : null
                    } 
                    {
                      index === 24 ? <Text style={{fontSize: 23, marginBottom: 5}}>Dokumentasi</Text> : null
                    }
                    {
                      index === 30 ? <Text style={{fontSize: 23, marginBottom: 5}}>PT (original)</Text> : null
                    }
                    
                    { detail.fieldName === "Other" && !this.state.other || detail.fieldName === "Penawaran Sebelumnya" || detail.fieldName === "Ekspektasi Customer" ? (
                      <Text style={styles.inputLabel}>{`${detail.fieldName} (Opsional)`}</Text>
                    ) : detail.fieldName === "Kategori Warna" ? <Text style={styles.inputLabel}>Warna Exterior *</Text>
                    :
                    (
                      <Text style={styles.inputLabel}>{detail.fieldName} *</Text>
                    )}
                    {detail.type === "date" ? (
                      <Datepicker
                        style={styles.inputForm}
                        mode="date"
                        date={detail.value}
                        placeholder=""
                        androidMode="spinner"
                        confirmBtnText="Oke"
                        cancelBtnText="Batal"
                        onDateChange={date => this.onChangeField(index, detail, date)}
                        format="YYYY-MM-DD"
                        showIcon={false}
                        customStyles={{
                          dateInput: {
                            borderWidth: 0,
                            textAlign: "left",
                            alignItems: "flex-start",
                            paddingLeft: 3,
                          },
                        }}
                      />
                    ) : null}
                    {detail.type === "text" || detail.type === "number" ? (
                      <View>
                        {this.chooseInputBox(detail, carSummary, index)}
                        {!!this.state.nameError && <Text style={{ color: red }}>{this.state.nameError}</Text>}
                      </View>
                    ) : null}
                    {detail.type === "radio" ? (
                      <View style={{ flex: 1, alignItems: "flex-start" }}>
                        <RadioGroup
                          radioButtons={[
                            {
                              label: "Yes",
                              value: true,
                            },
                            {
                              label: "No",
                              value: false,
                            },
                          ]}
                          flexDirection="row"
                          onPress={data => this.onChangeField(index, detail, data)}
                        />
                      </View>
                    ) : null}
                    {detail.type === "select" ? (
                      <View style={styles.inputForm}>
                        {
                          detail.fieldName === "Kategori Warna" ?
                          (
                            <Picker
                              selectedValue={detail.value}
                              
                              mode="dropdown"
                              onValueChange={value => this.ValueSelectChange(index, value, detail)}
                            >
                              {this.state.KategoriWarna.length > 0
                                ? (<Picker.Item key={0} label={`Pilih ${detail.fieldName}`} value="0" />,
                                  this.state.KategoriWarna.map(option => this.chooseLabel(option, detail.fieldName)))
                                : this.labelSelect(detail.fieldName)}
                            </Picker>
                          ) 
                          :
                          (
                            <Picker
                              selectedValue={detail.value}
                              
                              mode="dropdown"
                              onValueChange={value => this.ValueSelectChange(index, value, detail)}
                            >
                              {this.state[detail.fieldName].length > 0
                                ? (<Picker.Item key={0} label={`Pilih ${detail.fieldName}`} value="0" />,
                                  this.state[detail.fieldName].map(option => this.chooseLabel(option, detail.fieldName)))
                                : this.labelSelect(detail.fieldName)}
                            </Picker>
                          )
                        }
                      </View>
                    ) : null}
                  </View>
                ))}
                </View>
              </View>
              
            </ScrollView>
          )
        }
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    paddingTop: 20,
    backgroundColor: "#fff",
  },
  title: {
    fontSize: 20,
    fontWeight: "600",
    color: "#000",
  },
  inputGroup: {
    marginBottom: 15,
    marginLeft: 10,
  },
  inputForm: {
    borderWidth: 1.5,
    borderColor: colors.light_sapphire_bluish_gray,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('4%'),
    color: colors.dope_blue
  },
  inputLabel: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4%'),
    marginBottom: widthPercentageToDP('1%'),
    fontFamily: 'Roboto',
  },
  other: {
    color: colors.dope_blue
  }
});

const mapStateToProps = ({ Auth, AppraisalDetail }) => ({ Auth, AppraisalDetail });
const mapDispatchToProps = dispatch => bindActionCreators({ updateAppraisalData }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Summary);
