import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";

import { widthPercentageToDP, heightPercentageToDP } from "../../utils";

class InputPhoto extends Component {
  render() {
    const { photo, onPressPhoto } = this.props;
    return (
      <View style={styles.inputContainer}>
        <View style={styles.photoContainer}>
          <Text style={styles.photoText}>{photo.name}</Text>
        </View>
        <TouchableOpacity onPress={() => onPressPhoto(photo.name)} style={styles.iconContainer}>
          <Icon name="camera" size={30} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  inputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    paddingHorizontal: widthPercentageToDP("3%"),
    paddingVertical: heightPercentageToDP("1%"),
    borderWidth: 1,
    borderColor: "#000",
  },
  photoContainer: {
    flex: 1,
    justifyContent: "center",
  },
  iconContainer: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "center",
  },
  photoText: {
    fontWeight: "600",
    fontSize: 18,
  },
};

export default InputPhoto;
