import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Linking,
  StyleSheet,
} from 'react-native';
import moment from 'moment';
import 'moment/locale/id';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../utils/colors';

import {heightPercentageToDP, widthPercentageToDP} from '../../utils';
import {requestAppraisalDetail} from '../../actions';
import axios from '../../services/axios';
import logo from '../../assets/image/logo-trust.png';
moment.locale('id');

class HandoverList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      handoverList: [],
    };
    this.navigationWillFocusListener = props.navigation.addListener(
      'willFocus',
      () => {
        this.fetchHandoverList();
      },
    );
  }

  componentWillUnmount() {
    this.navigationWillFocusListener.remove();
  }

  componentDidUpdate(prevProps) {
    const {AppraisalDetail} = this.props;
    if (prevProps.AppraisalDetail.onRequestDetailAppraisal) {
      if (AppraisalDetail.successDetailAppraisal) {
        this.props.navigation.navigate('Appraisal');
      } else if (AppraisalDetail.errorDetailAppraisal) {
        ToastAndroid.showWithGravity(
          'Gagal menampilkan detail appraisal',
          2000,
          ToastAndroid.BOTTOM,
        );
      } else {
        console.log('Something else ?', AppraisalDetail);
      }
    }
  }

  async fetchHandoverList() {
    try {
      const {user} = this.props.Auth;
      let {data} = await axios.get(`/user/${user.id}/handover`);
      this.setState({
        handoverList: data.handovers,
      });
    } catch (err) {
      console.log('ini error', JSON.stringify(err));
      ToastAndroid.showWithGravity(
        'Gagal menampilkan list booking',
        2000,
        ToastAndroid.BOTTOM,
      );
    }
  }

  openDetailHandover(bookingId) {
    this.props.navigation.navigate('InputDetailHandover', {bookingId});
  }

  render() {
    const {handoverList} = this.state;
    const {user} = this.props.Auth;

    return (
      <ScrollView>
        <View style={styles.infoContainer}>
          <View style={styles.infoBox}>
            <Text style={styles.infoTitle}>2</Text>
            <Text style={styles.infoSubHead}>CARD BULAN INI</Text>
          </View>

          <View style={styles.infoBox}>
            <Text style={styles.infoTitle}>3</Text>
            <Text style={styles.infoSubHead}>CARD BULAN SELESAI</Text>
          </View>
        </View>

        <View style={styles.listContainer}>
          {handoverList.length !== 0 ? (
            handoverList.map((handover) => (
              <View style={styles.bookingItem} key={handover.id}>
                <TouchableOpacity
                  onPress={() => this.openDetailHandover(handover.Booking.id)}>
                  <View style={styles.noOrderBox}>
                    <Text style={styles.noOrder}>
                      {handover.Booking.noBooking}
                    </Text>

                    <Text style={styles.statusText}>{handover.status}</Text>
                  </View>
                  <View style={styles.detailBook}>
                    <Text style={styles.brandText}>
                      {handover.Booking.carType}
                    </Text>
                    <Text style={styles.date}>
                      {moment(
                        handover.Booking.bookingTime,
                        'YYYY-MM-DDTHH:mm',
                      ).format('llll')}
                    </Text>
                  </View>
                </TouchableOpacity>
                <View style={styles.salesProfile}>
                  <View
                    style={{
                      width: widthPercentageToDP('40%'),
                      justifyContent: 'center',
                    }}>
                    <Text style={styles.date}>
                      {handover.Booking.SalesProfile.name}
                    </Text>
                  </View>
                </View>
              </View>
            ))
          ) : (
            <View
              style={{
                width: widthPercentageToDP('90%'),
                height: heightPercentageToDP('50%'),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: colors.dope_blue,
                  fontSize: widthPercentageToDP('5%'),
                }}>
                Tidak Ada Daftar Appraisal
              </Text>
            </View>
          )}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoContainer: {
    paddingHorizontal: widthPercentageToDP('5%'),
    paddingVertical: widthPercentageToDP('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  infoBox: {
    width: widthPercentageToDP('43.5%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2%'),
    backgroundColor: 'white',
    borderRadius: 5,
  },
  infoTitle: {
    color: colors.strong_blue,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('10%'),
  },
  infoSubHead: {
    fontSize: widthPercentageToDP('3.5%'),
  },
  listContainer: {
    marginTop: widthPercentageToDP('4%'),
    paddingHorizontal: widthPercentageToDP('5%'),
  },
  bookingItem: {
    padding: widthPercentageToDP('4%'),
    backgroundColor: 'white',
    elevation: 4,
    shadowOffset: {width: 7, height: 5},
    shadowColor: 'grey',
    shadowOpacity: 0.5,
    shadowRadius: 18,
    borderRadius: 5,
    marginBottom: widthPercentageToDP('5%'),
  },
  noOrderBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: widthPercentageToDP('2%'),
  },
  noOrder: {
    fontSize: widthPercentageToDP('4.3%'),
    color: colors.strong_blue,
    fontWeight: 'bold',
  },
  statusText: {
    color: colors.cyan_green,
    fontWeight: 'bold',
    fontSize: widthPercentageToDP('4.3%'),
  },
  brandText: {
    fontSize: widthPercentageToDP('4.35%'),
    fontWeight: 'bold',
    color: colors.dope_blue,
  },
  date: {
    color: colors.dope_blue,
    fontSize: widthPercentageToDP('4.3%'),
  },
  detailBook: {
    paddingBottom: widthPercentageToDP('2%'),
    borderColor: colors.light_sapphire_bluish_gray,
    borderBottomWidth: 1.5,
  },
  salesProfile: {
    paddingVertical: widthPercentageToDP('2%'),
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  WAbutton: {
    width: widthPercentageToDP('40%'),
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: widthPercentageToDP('2.2%'),
    // backgroundColor: colors.whatsapp,
    flexDirection: 'row',
    borderRadius: 5,
  },
  WAtext: {
    color: 'white',
    fontSize: widthPercentageToDP('4.5%'),
    fontWeight: 'bold',
    marginLeft: 10,
  },
  WAactive: {
    backgroundColor: colors.whatsapp,
  },
  WAinactive: {
    backgroundColor: colors.super_light_grayish_blue,
  },
});

const mapStateToProps = ({Auth, AppraisalDetail}) => ({Auth, AppraisalDetail});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({requestAppraisalDetail}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HandoverList);
