import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import handoverList from './handoverList';
import inputHandoverDetail from './inputHandoverDetail';

const HandoverNav = createStackNavigator(
  {
    handoverList: {
      screen: handoverList,
      navigationOptions: ({navigation}) => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'handoverList',
    headerMode: 'none',
  },
);

export default HandoverNav;
