import React from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import Accordion from '../../component/Accordion';
import styles from './styles';
import inputStyle from './inputStyle';

function PartReplacement(props) {
  const {idPart, onChange} = props;
  const [allField, setAllField] = React.useState(false);
  const [partReplacement, setPartReplacement] = React.useState({
    partName: false,
    partRemovedNumber: false,
    partRemovedSerialNumber: false,
    partReplacingNumber: false,
    partReplacingSerialNumber: false,
  });

  return (
    <Accordion label={'Part' + ' ' + idPart} style={{marginTop: 10}}>
      <Text style={inputStyle.label}>Part Name</Text>
      <TextInput
        placeholder="Part Name"
        style={inputStyle.inputStyle}
        autoCapitalize="none"
        onChangeText={(text) =>
          setPartReplacement((prev) => {
            return {...prev, partName: text};
          })
        }
      />

      <Text style={inputStyle.label}>Part Removed Number</Text>
      <TextInput
        placeholder="Part Removed Number"
        style={inputStyle.inputStyle}
        autoCapitalize="none"
        onChangeText={(text) =>
          setPartReplacement((prev) => {
            return {...prev, partRemovedNumber: text};
          })
        }
      />

      <Text style={inputStyle.label}>Part Removed Serial Number</Text>
      <TextInput
        placeholder="Part Removed Serial Number"
        style={inputStyle.inputStyle}
        autoCapitalize="none"
        onChangeText={(text) =>
          setPartReplacement((prev) => {
            return {...prev, partRemovedSerialNumber: text};
          })
        }
      />

      <Text style={inputStyle.label}>Part Replacing Number</Text>
      <TextInput
        placeholder="Part Replacing Number"
        style={inputStyle.inputStyle}
        autoCapitalize="none"
        onChangeText={(text) =>
          setPartReplacement((prev) => {
            return {...prev, partReplacingNumber: text};
          })
        }
      />

      <Text style={inputStyle.label}>Part Replacing Serial Number</Text>
      <TextInput
        placeholder="Part Replacing Serial Number"
        style={inputStyle.inputStyle}
        autoCapitalize="none"
        onChangeText={(text) =>
          setPartReplacement((prev) => {
            return {...prev, partReplacingSerialNumber: text};
          })
        }
      />

      <TouchableOpacity
        style={[
          styles.uploadPhotoButton,
          styles.uploadPhotoButtonActive,
          {width: '100%'},
        ]}
        onPress={() => onChange(partReplacement)}>
        <Text style={styles.buttonText}>Add</Text>
      </TouchableOpacity>
    </Accordion>
  );
}

export default PartReplacement;
