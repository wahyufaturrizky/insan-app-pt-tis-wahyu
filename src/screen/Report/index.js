import React from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Alert,
  TextInput,
  Picker,
} from 'react-native';
import Modal from 'react-native-modal';
import inputStyle from './inputStyle';
import styles from './styles';
import {useForm, Controller} from 'react-hook-form';
import {widthPercentageToDP} from '../../utils';
import Accordion from '../../component/Accordion';
import Switch from '../../component/Switch';
import component from './component';
import PartReplacement from './PartReplacement';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Signature from 'react-native-signature-canvas';
import {connect} from 'react-redux';
import moment from 'moment';
import {uniqWith, get} from 'lodash';
import RNFetchBlob from 'rn-fetch-blob';
import services from '../CreateBast/services';
import InputDate from '../../component/Input/InputDate';
import {Input} from 'react-native-elements';

function Report(props) {
  const {visible, onClose, dataWorkStart, Auth} = props;
  const {register, setValue, watch, handleSubmit, control} = useForm();
  const [partReplacement, setPartReplacement] = React.useState([]);
  const [enableScroll, setEnableScroll] = React.useState(true);
  const [signatureMerchant, setSignatureMerchant] = React.useState('');
  const [signatureService, setSignatureService] = React.useState('');
  const {api_token} = Auth.api_token;

  React.useEffect(() => {
    register('ticketNumber');
    register('reportedProblem');
    register('reportedBy');
    register('changeLabel');
    register('customer');
    register('location');
    register('itsBranch');
    register('machineType');
    register('equipment');
    register('machineId');
    register('maintenance');
    register('warranty');
    register('onCall');
    register('activity');
    register('problemReceivedDate');
    register('problemReceivedTime');
    register('problemReceivedZone');
    register('problemReceivedServerCode');
    register('appointmentDate');
    register('appointmentTime');
    register('appointmentZone');
    register('appointmentServerCode');
    register('departureDate');
    register('departureTime');
    register('departureZone');
    register('departureServerCode');
    register('arrivalDate');
    register('arrivalTime');
    register('arrivalZone');
    register('arrivalServerCode');
    register('workstartDate');
    register('workstartTime');
    register('workstartZone');
    register('workstartServerCode');
    register('workfinishDate');
    register('workfinishTime');
    register('workfinishZone');
    register('workfinishServerCode');
    register('arrivalOfficeDate');
    register('arrivalOfficeTime');
    register('arrivalOfficeZone');
    register('arrivalOfficeServerCode');
    register('travel');
    register('numberCall');
    register('jobStatus');
    register('voltage');
    register('grounding');
    register('temperature');
    register('ups');
    register('ac');
    register('itTransformer');
    register('temperature');
    register('waitingTime');
    register('waitingComunication');
    register('waitingSpareTime');
    register('note');
    register('action');
  }, []);

  function onSubmit(data) {
    const formData = new FormData();

    const objectKeyForm = Object.keys(data);

    formData.append('reported_problem', get(data, 'reportedProblem', ''));
    formData.append('reported_by', get(data, 'reportedBy', ''));
    formData.append('changeable', get(data, 'changeLevel', ''));
    formData.append('name_customer', get(data, 'customer', ''));
    formData.append('location', get(data, 'location', ''));
    formData.append('machine_type', get(data, 'machineType', ''));
    formData.append('machine_id', get(data, 'machineId', ''));
    formData.append('warranty', get(data, 'warranty', ''));
    formData.append('on_call', get(data, 'onCall', ''));
    formData.append('maintance', get(data, 'maintenance', ''));
    formData.append('activity', get(data, 'activity', ''));
    formData.append('date_problem_received', get(data, 'problemReceivedDate', ''));
    formData.append('time_problem_received', get(data, 'problemReceivedDate', ''));
    formData.append('zone_problem_received', get(data, 'problemReceivedZone', ''));
    // eslint-disable-next-line prettier/prettier
    formData.append('service_code_problem_received', get(data, 'problemReceivedServerCode', ''));
    formData.append('date_appointment', get(data, 'appointmentDate', ''));
    formData.append('time_appointment', get(data, 'appointmentTime', ''));
    formData.append('zone_appointment', get(data, 'appointmentZone', ''));
    // eslint-disable-next-line prettier/prettier
    formData.append('service_code_appointment', get(data, 'appointmentServerCode', ''));
    formData.append('date_departure', get(data, 'departureDate', ''));
    formData.append('time_departure', get(data, 'departureTime', ''));
    formData.append('zone_departure', get(data, 'departureZone', ''));
    // eslint-disable-next-line prettier/prettier
    formData.append('service_code_departure', get(data, 'departureServerCode', ''));
    formData.append('date_arrival', get(data, 'arrivalDate', ''));
    formData.append('time_arrival', get(data, 'arrivalTime', ''));
    formData.append('zone_arrival', get(data, 'arrivalZone', ''));
    formData.append('service_code_arrival', get(data, 'arrivalServerCode', ''));
    formData.append('date_workstart', get(data, 'workstartDate', ''));
    formData.append('time_workstart', get(data, 'workstartTime', ''));
    formData.append('zone_workstart', get(data, 'workstartZone', ''));
    // eslint-disable-next-line prettier/prettier
    formData.append('service_code_workstart', get(data, 'workstartServerCode', ''));
    formData.append('date_workfinish', get(data, 'workfinishDate', ''));
    formData.append('time_workfinish', get(data, 'workfinishDate', ''));
    formData.append('zone_workfinish', get(data, 'workfinishDate', ''));
    // eslint-disable-next-line prettier/prettier
    formData.append('service_code_workfinish', get(data, 'workfinishServerCode', ''));
    formData.append('date_arrival_office', get(data, 'arrivalOfficeDate', ''));
    formData.append('time_arrival_office', get(data, 'arrivalOfficeDate', ''));
    formData.append('zone_arrival_office', get(data, 'arrivalOfficeDate', ''));
    // eslint-disable-next-line prettier/prettier
    formData.append('service_code_arrival_office', get(data, 'arrivalOfficeServerCode'));
    formData.append('travel', get(data, 'travel', ''));
    formData.append('number_call', get(data, 'numberCall', ''));
    formData.append('job_status', get(data, 'jobStatus', ''));
    formData.append('voltage', get(data, 'voltage', ''));
    formData.append('grounding', get(data, 'grounding', ''));
    formData.append('temperature', get(data, 'temperature', ''));
    formData.append('ups', get(data, 'ups', ''));
    formData.append('ac', get(data, 'ac', ''));
    formData.append('it', get(data, 'itTransformer', ''));
    formData.append('waiting_customer_time', get(data, 'waitingTime', ''));
    formData.append('waiting_communication_time', get(data, 'waitingComunication', ''));
    formData.append('waiting_sparepart_time', get(data, 'waitingSpareTime', ''));
    formData.append('note', get(data, 'note', ''));
    formData.append('action', get(data, 'action', ''));

    const deviceCheck = [];
    const adjust = [];
    const clean = [];
    const replace = [];

    component.PCModule.map((itemField) => {
      if (objectKeyForm.indexOf(itemField.slug) >= 0) {
        if (data[itemField.slug]) {
          deviceCheck.push(itemField.name);
          // formData.append('name[]', itemField.name);
        }

        if (data[itemField.slug] === 'adjust') {
          adjust.push(true);
          clean.push(false);
          replace.push(false);
          // formData.append('adjust[]', true);
        }

        if (data[itemField.slug] === 'clean') {
          clean.push(true);
          adjust.push(false);
          replace.push(false);
          // formData.append('clean[]', true);
        }

        if (data[itemField.slug] === 'replace') {
          replace.push(true);
          clean.push(false);
          adjust.push(false);
          // formData.append('replace[]', true);
        }
      }
    });

    component.UpperCompartmentModule.map((itemField) => {
      if (objectKeyForm.indexOf(itemField.slug) >= 0) {
        if (data[itemField.slug]) {
          // formData.append('name[]', itemField.name);
          deviceCheck.push(itemField.name);
        }

        if (data[itemField.slug] === 'adjust') {
          adjust.push(true);
          clean.push(false);
          replace.push(false);
          // formData.append('adjust[]', true);
        }

        if (data[itemField.slug] === 'clean') {
          clean.push(true);
          adjust.push(false);
          replace.push(false);
          // formData.append('clean[]', true);
        }

        if (data[itemField.slug] === 'replace') {
          replace.push(true);
          clean.push(false);
          adjust.push(false);
          // formData.append('replace[]', true);
        }
      }
    });

    component.LowerCompartmentModule.map((itemField) => {
      if (objectKeyForm.indexOf(itemField.slug) >= 0) {
        if (data[itemField.slug]) {
          // formData.append('name[]', itemField.name);
          deviceCheck.push(itemField.name);
        }

        if (data[itemField.slug] === 'adjust') {
          adjust.push(true);
          clean.push(false);
          replace.push(false);
          // formData.append('adjust[]', true);
        }

        if (data[itemField.slug] === 'clean') {
          clean.push(true);
          adjust.push(false);
          replace.push(false);
          // formData.append('clean[]', true);
        }

        if (data[itemField.slug] === 'replace') {
          replace.push(true);
          clean.push(false);
          adjust.push(false);
          // formData.append('replace[]', true);
        }
      }
    });

    component.CMDModule.map((itemField) => {
      if (objectKeyForm.indexOf(itemField.slug) >= 0) {
        if (data[itemField.slug]) {
          // formData.append('name[]', itemField.name);
          deviceCheck.push(itemField.name);
        }

        if (data[itemField.slug] === 'adjust') {
          adjust.push(true);
          clean.push(false);
          replace.push(false);
          // formData.append('adjust[]', true);
        }

        if (data[itemField.slug] === 'clean') {
          clean.push(true);
          adjust.push(false);
          replace.push(false);
          // formData.append('clean[]', true);
        }

        if (data[itemField.slug] === 'replace') {
          replace.push(true);
          clean.push(false);
          adjust.push(false);
          // formData.append('replace[]', true);
        }
      }
    });

    formData.append('name', deviceCheck ? deviceCheck.join(',') : '');
    formData.append('adjust', adjust ? adjust.join(',') : '');
    formData.append('clean', clean ? clean.join(',') : '');
    formData.append('replace', replace ? replace.join(',') : '');

    const uniqReplacement = uniqWith(partReplacement, function (o) {
      return o.numberPart;
    });

    const numberPart = [];
    const partName = [];
    const removedSerial = [];
    const removedPart = [];
    const replacingSerial = [];
    const replacingPart = [];

    uniqReplacement.map((itemField, index) => {
      numberPart.push(index + 1);
      partName.push(itemField.partName);
      removedSerial.push(itemField.partRemovedSerialNumber);
      removedPart.push(itemField.partRemovedNumber);
      replacingSerial.push(itemField.partReplacingSerialNumber);
      replacingPart.push(itemField.partReplacingNumber);

      // formData.append('number_part[]', index + 1);
      // // eslint-disable-next-line prettier/prettier
      // formData.append('part_name[]', itemField.partName);
      // // eslint-disable-next-line prettier/prettier
      // formData.append('removed_serial[]', itemField.partRemovedSerialNumber);
      // // eslint-disable-next-line prettier/prettier
      // formData.append('removed_part[]', itemField.partRemovedNumber);
      // // eslint-disable-next-line prettier/prettier
      // formData.append('replacing_part[]', itemField.partReplacingNumber);
      // // eslint-disable-next-line prettier/prettier
      // formData.append('replacing_serial[]', itemField.partReplacingSerialNumber);
    });

    formData.append('number_part', numberPart.join(','));
    formData.append('removed_serial', removedSerial.join(','));
    formData.append('removed_part', removedPart.join(','));
    formData.append('replacing_serial', replacingSerial.join(','));
    formData.append('replacing_part', replacingPart.join(','));


    // for (let i = 0; i < 5; i++) {
    //   formData.append('number_part[]', i + 1);
    //   // eslint-disable-next-line prettier/prettier
    //   formData.append('part_name[]', uniqReplacement[i] ? uniqReplacement[i].partName : '');
    //   // eslint-disable-next-line prettier/prettier
    //   formData.append('removed_serial[]', uniqReplacement[i] ? uniqReplacement[i].partRemovedSerialNumber : '');
    //   // eslint-disable-next-line prettier/prettier
    //   formData.append('removed_part[]', uniqReplacement[i] ? uniqReplacement[i].partRemovedNumber : '');
    //   // eslint-disable-next-line prettier/prettier
    //   formData.append('replacing_part[]', uniqReplacement[i] ? uniqReplacement[i].partReplacingNumber : '');
    //   // eslint-disable-next-line prettier/prettier
    //   formData.append('replacing_serial[]', uniqReplacement[i] ? uniqReplacement[i].partReplacingSerialNumber : '');
    // }

    formData.append('ttd_engineer', signatureService);
    formData.append('ttd_customer', signatureMerchant);

    services.CreateReportBastEDC(
      {formData, id: dataWorkStart.ticket.id, token: api_token},
      {
        onSuccess: () => {
          Alert.alert('Info', 'FE Report created');
          // onClose();
        },
        onError: (error) => {
          Alert.alert('Warning', 'Failed created FE Report');
          console.log(error);
        },
      },
    );
  }

  function itemReplacement() {
    const items = [];

    for (let i = 0; i < 5; i++) {
      items.push(
        <PartReplacement
          idPart={i}
          onChange={(state) => setPartReplacement((prev) => [...prev, state])}
        />,
      );
    }

    return items;
  }

  const style = `.m-signature-pad--footer
    .button {
      background-color: #2F7CD8;
      color: #FFF;
    }`;

  function beforeExit() {
    Alert.alert('Warning', 'Are you sure?', [
      {
        text: 'Cancel',
      },
      {
        text: 'OK',
        onPress: onClose,
      },
    ]);
  }

  function onSignatureMerchant(signature) {
    const path =
      RNFetchBlob.fs.dirs.DownloadDir +
      `/reportSignature_${new Date().getDate()}_${new Date().getSeconds()}.png`;
    RNFetchBlob.fs
      .createFile(
        path,
        signature.replace('data:image/png;base64,', ''),
        'base64',
      )
      .then(() => {
        RNFetchBlob.fs.stat(path).then((res) => {
          setSignatureMerchant({
            uri: 'file://' + res.path,
            path: res.path,
            type: 'image/png',
            name: res.filename,
          });
        });
      });
  }

  function onSignatureServices(signature) {
    const path =
      RNFetchBlob.fs.dirs.DownloadDir +
      `/reportSignatureService_${new Date().getDate()}_${new Date().getSeconds()}.png`;
    RNFetchBlob.fs
      .createFile(
        path,
        signature.replace('data:image/png;base64,', ''),
        'base64',
      )
      .then(() => {
        RNFetchBlob.fs.stat(path).then((res) => {
          setSignatureService({
            uri: 'file://' + res.path,
            path: res.path,
            type: 'image/png',
            name: res.filename,
          });
        });
      });
  }

  return (
    <Modal
      isVisible={visible}
      onBackdropPress={() => beforeExit()}
      style={styles.photoModal}>
      <View
        style={
          Dimensions.get('window').height <= 360 ? styles.scrollableModal : {}
        }>
        <View style={styles.modalContainer}>
          <View style={styles.modalHeader}>
            <TouchableOpacity onPress={() => beforeExit()}>
              <Text
                style={{
                  color: 'white',
                  fontSize: widthPercentageToDP('8%'),
                  fontFamily: 'Roboto',
                }}>
                x
              </Text>
            </TouchableOpacity>
          </View>
          <ScrollView
            scrollEnabled={enableScroll}
            style={{backgroundColor: '#fff'}}>
            <View style={styles.modalContent}>
              <Text style={styles.modalPhotoName}>FIElD ENGINEER REPORT</Text>
              <View style={{marginTop: 10}}>
                <Accordion label="Ticket Information">
                  <Text style={inputStyle.label}>Reported Problem</Text>
                  <TextInput
                    placeholder="reported problem"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('reportedProblem', text)}
                  />

                  <Text style={inputStyle.label}>Reported By</Text>
                  <TextInput
                    placeholder="reported by"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('reportedBy', text)}
                  />

                  <Text style={inputStyle.label}>Changeable</Text>
                  <Picker
                    enabled
                    onValueChange={(value) => setValue('changeLabel', value)}
                    selectedValue={watch('changeLabel')}>
                    <Picker.Item label="Please select" value="" />
                    <Picker.Item label="Yes" value="yes" />
                    <Picker.Item label="No" value="no" />
                  </Picker>
                </Accordion>

                <Accordion label="Machine Information" style={{marginTop: 10}}>
                  <Text style={inputStyle.label}>Customer</Text>
                  <TextInput
                    placeholder="customer"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('customer', text)}
                  />

                  <Text style={inputStyle.label}>Location</Text>
                  <TextInput
                    placeholder="location"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('location', text)}
                  />

                  <Text style={inputStyle.label}>ITS Branch</Text>
                  <TextInput
                    placeholder="its branch"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('itsBranch', text)}
                  />

                  <Text style={inputStyle.label}>Machine Type</Text>
                  <TextInput
                    placeholder="machine type"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('machineType', text)}
                  />

                  <Text style={inputStyle.label}>SSB No./Equipment</Text>
                  <TextInput
                    placeholder="equipment"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('equipment', text)}
                  />

                  <Text style={inputStyle.label}>Machine ID</Text>
                  <TextInput
                    placeholder="machine ID"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('machineId', text)}
                  />

                  <Switch
                    label="Maintenance"
                    onChange={(text) => setValue('maintenance', text)}
                    value={watch('maintenance')}
                  />
                  <Switch
                    label="Warranty"
                    onChange={(text) => setValue('warranty', text)}
                    value={watch('warranty')}
                  />
                  <Switch
                    label="On Call"
                    onChange={(text) => setValue('onCall', text)}
                    value={watch('onCall')}
                  />
                </Accordion>

                <Accordion label="Activity" style={{marginTop: 10}}>
                  <Controller
                    name="actitivy"
                    defaultValue=""
                    control={control}
                    render={({onChange, value}) => (
                      <Picker
                        onValueChange={(text) => onChange(text)}
                        selectedValue={value}>
                        <Picker.Item label="Please Select" value="" />
                        <Picker.Item
                          label="Preventive Maintenance"
                          value="Preventive Maintenance"
                        />
                        <Picker.Item
                          label="Corrective Maintenance"
                          value="Corrective Maintenance"
                        />
                        <Picker.Item label="Activation" value="Activation" />
                        <Picker.Item
                          label="Pre-Installation"
                          value="Pre-Installation"
                        />
                        <Picker.Item label="Project/UAT" value="Project/UAT" />
                        <Picker.Item
                          label="Upgrade/Update"
                          value="Upgrade/Update"
                        />
                      </Picker>
                    )}
                  />
                </Accordion>

                <Accordion label="Assignment" style={{marginTop: 10}}>
                  <Text style={inputStyle.label}>Problem received</Text>
                  <InputDate
                    mode="date"
                    onChange={(text) => setValue('problemReceivedDate', text)}
                  />
                  <InputDate
                    mode="time"
                    format="HH:mm"
                    onChange={(text) => setValue('problemReceivedTime', text)}
                  />
                  <TextInput
                    placeholder="Zone"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) =>
                      setValue('problemReceivedZone', text)
                    }
                  />
                  <TextInput
                    placeholder="Server Code"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) =>
                      setValue('problemReceivedServerCode', text)
                    }
                  />

                  <Text style={inputStyle.label}>Appointment time</Text>
                  <InputDate
                    mode="date"
                    onChange={(text) => setValue('appointmentDate', text)}
                  />
                  <InputDate
                    mode="time"
                    format="HH:mm"
                    onChange={(text) => setValue('appointmentTime', text)}
                  />
                  <TextInput
                    placeholder="Zone"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('appointmentZone', text)}
                  />
                  <TextInput
                    placeholder="Server Code"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) =>
                      setValue('appointmentServerCode', text)
                    }
                  />

                  <Text style={inputStyle.label}>Departure time</Text>
                  <InputDate
                    mode="date"
                    onChange={(text) => setValue('departureDate', text)}
                  />
                  <InputDate
                    mode="time"
                    format="HH:mm"
                    onChange={(text) => setValue('departureTime', text)}
                  />
                  <TextInput
                    placeholder="Zone"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('departureZone', text)}
                  />
                  <TextInput
                    placeholder="Server Code"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) =>
                      setValue('departureServerCode', text)
                    }
                  />

                  <Text style={inputStyle.label}>Arrival time</Text>
                  <InputDate
                    mode="date"
                    onChange={(text) => setValue('arrivalDate', text)}
                  />
                  <InputDate
                    mode="time"
                    format="HH:mm"
                    onChange={(text) => setValue('arrivalTime', text)}
                  />
                  <TextInput
                    placeholder="Zone"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('arrivalZone', text)}
                  />
                  <TextInput
                    placeholder="Server Code"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('arrivalServerCode', text)}
                  />

                  <Text style={inputStyle.label}>Work start time</Text>
                  <InputDate
                    mode="date"
                    onChange={(text) => setValue('workstartDate', text)}
                  />
                  <InputDate
                    mode="time"
                    format="HH:mm"
                    onChange={(text) => setValue('workstartTime', text)}
                  />
                  <TextInput
                    placeholder="Zone"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('workstartZone', text)}
                  />
                  <TextInput
                    placeholder="Server Code"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) =>
                      setValue('workstartServerCode', text)
                    }
                  />

                  <Text style={inputStyle.label}>Work finish time</Text>
                  <InputDate
                    mode="date"
                    onChange={(text) => setValue('workfinishDate', text)}
                  />
                  <InputDate
                    mode="time"
                    format="HH:mm"
                    onChange={(text) => setValue('workfinishTime', text)}
                  />
                  <TextInput
                    placeholder="Zone"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('workfinishZone', text)}
                  />
                  <TextInput
                    placeholder="Server Code"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) =>
                      setValue('workfinishServerCode', text)
                    }
                  />

                  <Text style={inputStyle.label}>Arrival Office time</Text>
                  <InputDate
                    mode="date"
                    onChange={(text) => setValue('arrivalOfficeDate', text)}
                  />
                  <InputDate
                    mode="time"
                    format="HH:mm"
                    onChange={(text) => setValue('arrivalOfficeTime', text)}
                  />
                  <TextInput
                    placeholder="Zone"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('arrivalOfficeZone', text)}
                  />
                  <TextInput
                    placeholder="Server Code"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) =>
                      setValue('arrivalOfficeServerCode', text)
                    }
                  />
                </Accordion>

                <Accordion label="Other Status" style={{marginTop: 10}}>
                  <Text style={inputStyle.label}>Travel</Text>
                  <TextInput
                    placeholder="Travel"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('travel', text)}
                  />

                  <Text style={inputStyle.label}>Number Call</Text>
                  <TextInput
                    placeholder="Number Call"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('numberCall', text)}
                  />
                </Accordion>

                <Accordion label="Job Status" style={{marginTop: 10}}>
                  <Picker
                    onValueChange={(text) => setValue('jobStatus', text)}
                    selectedValue={watch('jobStatus')}>
                    <Picker.Item label="Please Select" value="" />
                    <Picker.Item label="Pending" value="pending" />
                    <Picker.Item label="Completed" value="completed" />
                  </Picker>
                </Accordion>

                <Accordion label="Environment" style={{marginTop: 10}}>
                  <Text style={inputStyle.label}>Voltage</Text>
                  <TextInput
                    placeholder="Voltage"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('voltage', text)}
                  />

                  <Text style={inputStyle.label}>Grounding</Text>
                  <TextInput
                    placeholder="Grounding"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('grounding', text)}
                  />

                  <Text style={inputStyle.label}>Temperature</Text>
                  <TextInput
                    placeholder="Temperature"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('temperature', text)}
                  />

                  <Text style={inputStyle.label}>UPS</Text>
                  <Picker
                    onValueChange={(value) => setValue('ups', value)}
                    selectedValue={watch('ups')}>
                    <Picker.Item label="Please Select" value="" />
                    <Picker.Item label="Ready" value="ready" />
                    <Picker.Item label="Not Ready" value="not-ready" />
                  </Picker>

                  <Text style={inputStyle.label}>AC</Text>
                  <Picker
                    onValueChange={(value) => setValue('ac', value)}
                    selectedValue={watch('ac')}>
                    <Picker.Item label="Please Select" value="" />
                    <Picker.Item label="Ready" value="ready" />
                    <Picker.Item label="Not Ready" value="not-ready" />
                  </Picker>

                  <Text style={inputStyle.label}>It Transformer</Text>
                  <Picker
                    onValueChange={(value) => setValue('itTransformer', value)}
                    selectedValue={watch('itTransformer')}>
                    <Picker.Item label="Please Select" value="" />
                    <Picker.Item label="Ready" value="ready" />
                    <Picker.Item label="Not Ready" value="not-ready" />
                  </Picker>

                  <Text style={inputStyle.label}>Note</Text>
                  <TextInput
                    placeholder="Note"
                    style={[inputStyle.inputStyle, {textAlignVertical: 'top'}]}
                    multiline
                    numberOfLines={3}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('temperature', text)}
                  />
                </Accordion>

                <Accordion label="Waiting Time" style={{marginTop: 10}}>
                  <Text style={inputStyle.label}>Waiting customers time</Text>
                  <InputDate
                    format="HH:mm"
                    placeholder="HH:mm - HH:mm"
                    mode="time"
                    onChange={(text) => setValue('waitingTime', text)}
                  />

                  <Text style={inputStyle.label}>
                    Waiting communication time
                  </Text>
                  <InputDate
                    format="HH:mm"
                    placeholder="HH:mm - HH:mm"
                    mode="time"
                    onChange={(text) => setValue('waitingComunication', text)}
                  />

                  <Text style={inputStyle.label}>Waiting spare time</Text>
                  <InputDate
                    format="HH:mm"
                    placeholder="HH:mm - HH:mm"
                    mode="time"
                    onChange={(text) => setValue('waitingSpareTime', text)}
                  />

                  <Text style={inputStyle.label}>Note</Text>
                  <TextInput
                    placeholder="Note"
                    style={[inputStyle.inputStyle, {textAlignVertical: 'top'}]}
                    multiline
                    numberOfLines={3}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('note', text)}
                  />
                </Accordion>

                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 20}}>Device Check</Text>
                </View>

                <Accordion label="PC Module" style={{marginTop: 10}}>
                  {component.PCModule.map((itemPcModule) => (
                    <View style={{marginBottom: 10}}>
                      <Text style={inputStyle.label}>{itemPcModule.name}</Text>
                      <Controller
                        name={itemPcModule.slug}
                        control={control}
                        defaultValue=""
                        render={({onChange, onBlur, value}) => (
                          <Picker
                            onValueChange={(value) => onChange(value)}
                            selectedValue={value}>
                            <Picker.Item label="Please Select" value="" />
                            <Picker.Item label="Adjust" value="adjust" />
                            <Picker.Item label="Clean" value="clean" />
                            <Picker.Item label="Replace" value="replace" />
                          </Picker>
                        )}
                      />
                    </View>
                  ))}
                </Accordion>

                <Accordion
                  label="Upper Compartment Module"
                  style={{marginTop: 10}}>
                  {component.UpperCompartmentModule.map((itemPcModule) => (
                    <View style={{marginBottom: 10}}>
                      <Text style={inputStyle.label}>{itemPcModule.name}</Text>
                      <Controller
                        name={itemPcModule.slug}
                        control={control}
                        defaultValue=""
                        render={({onChange, onBlur, value}) => (
                          <Picker
                            onValueChange={(value) => onChange(value)}
                            selectedValue={value}>
                            <Picker.Item label="Please Select" value="" />
                            <Picker.Item label="Adjust" value="adjust" />
                            <Picker.Item label="Clean" value="clean" />
                            <Picker.Item label="Replace" value="replace" />
                          </Picker>
                        )}
                      />
                    </View>
                  ))}
                </Accordion>

                <Accordion
                  label="Lower Compartment Module"
                  style={{marginTop: 10}}>
                  {component.LowerCompartmentModule.map((itemPcModule) => (
                    <View style={{marginBottom: 10}}>
                      <Text style={inputStyle.label}>{itemPcModule.name}</Text>
                      <Controller
                        name={itemPcModule.slug}
                        control={control}
                        defaultValue=""
                        render={({onChange, onBlur, value}) => (
                          <Picker
                            onValueChange={(value) => onChange(value)}
                            selectedValue={value}>
                            <Picker.Item label="Please Select" value="" />
                            <Picker.Item label="Adjust" value="adjust" />
                            <Picker.Item label="Clean" value="clean" />
                            <Picker.Item label="Replace" value="replace" />
                          </Picker>
                        )}
                      />
                    </View>
                  ))}
                </Accordion>

                <Accordion label="CMD Module" style={{marginTop: 10}}>
                  {component.CMDModule.map((itemPcModule) => (
                    <View style={{marginBottom: 10}}>
                      <Text style={inputStyle.label}>{itemPcModule.name}</Text>
                      <Controller
                        name={itemPcModule.slug}
                        control={control}
                        defaultValue=""
                        render={({onChange, onBlur, value}) => (
                          <Picker
                            onValueChange={(value) => onChange(value)}
                            selectedValue={value}>
                            <Picker.Item label="Please Select" value="" />
                            <Picker.Item label="Adjust" value="adjust" />
                            <Picker.Item label="Clean" value="clean" />
                            <Picker.Item label="Replace" value="replace" />
                          </Picker>
                        )}
                      />
                    </View>
                  ))}
                </Accordion>

                <Accordion label="Part Replacement" style={{marginTop: 10}}>
                  {itemReplacement()}
                </Accordion>

                <Accordion label="Notes" style={{marginTop: 10}}>
                  <Text style={inputStyle.label}>Action Text</Text>
                  <TextInput
                    placeholder="Action Text"
                    style={[inputStyle.inputStyle, {textAlignVertical: 'top'}]}
                    multiline
                    numberOfLines={3}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('action', text)}
                  />
                </Accordion>

                <Accordion label="Signature Engineer" style={{marginTop: 10}}>
                  <View style={{height: 300, marginBottom: 15}}>
                    <Signature
                      descriptionText="Sign"
                      onEmpty={() => console.log('empty')}
                      onBegin={() => setEnableScroll(false)}
                      onEnd={() => setEnableScroll(true)}
                      onOK={onSignatureMerchant}
                      webStyle={style}
                      clearText="Clear"
                      confirmText="Save"
                    />
                  </View>
                  <Text style={inputStyle.label}>Engineer Name</Text>
                  <TextInput
                    placeholder="Engineer Name"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('temperature', text)}
                  />
                </Accordion>

                <Accordion label="Signature Customer" style={{marginTop: 10}}>
                  <View style={{height: 300, marginBottom: 15}}>
                    <Signature
                      descriptionText="Sign"
                      onEmpty={() => console.log('empty')}
                      onBegin={() => setEnableScroll(false)}
                      onEnd={() => setEnableScroll(true)}
                      onOK={onSignatureServices}
                      webStyle={style}
                      clearText="Clear"
                      confirmText="Save"
                    />
                  </View>
                  <Text style={inputStyle.label}>Customer Name</Text>
                  <TextInput
                    placeholder="Customer Name"
                    style={inputStyle.inputStyle}
                    autoCapitalize="none"
                    onChangeText={(text) => setValue('temperature', text)}
                  />
                </Accordion>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 20,
                    marginBottom: 30,
                  }}>
                  <TouchableOpacity
                    style={[
                      styles.viewPhotoButton,
                      styles.viewPhotoButtonActive,
                    ]}
                    onPress={() => onClose()}>
                    <Icon
                      name="close"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Batalkan</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      styles.uploadPhotoButton,
                      styles.uploadPhotoButtonActive,
                    ]}
                    onPress={handleSubmit(onSubmit)}>
                    <Icon
                      name="save"
                      size={widthPercentageToDP('6%')}
                      color="white"
                    />
                    <Text style={styles.buttonText}>Simpan</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
}

const mapStateToProps = ({Auth}) => {
  return {
    Auth,
  };
};

export default connect(mapStateToProps, null)(Report);
