const pcModule = [
  {
    name: 'PC',
    slug: 'pc',
  },
  {
    name: 'Mother Board',
    slug: 'mother_board',
  },
  {
    name: 'Floppy Disk',
    slug: 'floppy_disk',
  },
  {
    name: 'Hardisk',
    slug: 'hardisk',
  },
  {
    name: 'CD / DVD-ROM',
    slug: 'cd',
  },
  {
    name: 'Rocket Port',
    slug: 'rocket_port',
  },
  {
    name: 'Fitwin Card',
    slug: 'fitwin_card',
  },
  {
    name: 'Enthernet',
    slug: 'enthernet',
  },
  {
    name: 'Cable Date UTP',
    slug: 'cable_utp',
  },
  {
    name: 'Connector Cable',
    slug: 'connector_cable',
  },
  {
    name: 'Display Card',
    slug: 'display_card',
  },
  {
    name: 'Others',
    slug: 'other_pc_module',
  },
];
const upperCompartmentModule = [
  {
    name: 'Printer Journal',
    slug: 'printer_journal',
  },
  {
    name: 'Printer Receipt',
    slug: 'printer_receipt',
  },
  {
    name: 'IDDU / DIP / Card Reader',
    slug: 'card_reader',
  },
  {
    name: 'Microtouch Controller',
    slug: 'microtouch_controller',
  },
  {
    name: 'Protective Screen',
    slug: 'protective_screen',
  },
  {
    name: 'Monitor / LCD / Touchscreen',
    slug: 'monitor',
  },
  {
    name: 'Power Distributor',
    slug: 'power_distributor',
  },
  {
    name: 'Power Supply Unit',
    slug: 'power_supply_unit',
  },
  {
    name: 'Combination Keys',
    slug: 'combination_keys',
  },
  {
    name: 'Handle Fase / Fascle',
    slug: 'handle_fase',
  },
  {
    name: 'Camera',
    slug: 'camera',
  },
  {
    name: 'Others',
    slug: 'other_upper',
  },
];
const lowerCompartmentModule = [
  {
    name: 'Shutter Module',
    slug: 'shutter_module',
  },
  {
    name: 'Special Electronic',
    slug: 'special_electronic',
  },
  {
    name: 'Sofkey DOC / NDC',
    slug: 'sofkey_doc',
  },
  {
    name: 'EPP / Aplha Combi Keyboard',
    slug: 'epp/aplha',
  },
  {
    name: 'MDMS-Extrator',
    slug: 'mdms',
  },
  {
    name: 'Dispenser Controller',
    slug: 'dispenser_controller',
  },
  {
    name: 'V-Module',
    slug: 'v_module',
  },
  {
    name: 'Distributor Board',
    slug: 'distributor_board',
  },
  {
    name: 'Stacker Unit',
    slug: 'stacker_unit',
  },
  {
    name: 'Extractor unit',
    slug: 'extractor_unit',
  },
  {
    name: 'Cassette / Reject',
    slug: 'cassette',
  },
  {
    name: 'Key Look Cassette',
    slug: 'key_look',
  },
];
const CMDModule = [
  {
    name: 'Rear Pass',
    slug: 'rear-pass',
  },
  {
    name: 'Inner Pass',
    slug: 'inner-pass',
  },
  {
    name: 'Metal Detector Board',
    slug: 'metal_detector_board',
  },
  {
    name: 'On Board Test Panel',
    slug: 'onboardtest',
  },
  {
    name: 'Controller Board',
    slug: 'controller_board',
  },
  {
    name: 'Flat Pass',
    slug: 'flat_pass',
  },
  {
    name: 'Flat Pass Drive',
    slug: 'flat_pass_drive',
  },
  {
    name: 'Special Electronic',
    slug: 'special_electronic',
  },
  {
    name: 'RM2 Power Supply Unit',
    slug: 'rm2_power_supply',
  },
  {
    name: 'Escrow',
    slug: 'escrow',
  },
  {
    name: 'BIM',
    slug: 'bim',
  },
  {
    name: 'Front Pass',
    slug: 'front_pass',
  },
];

export default {
  PCModule: pcModule,
  UpperCompartmentModule: upperCompartmentModule,
  LowerCompartmentModule: lowerCompartmentModule,
  CMDModule,
};
