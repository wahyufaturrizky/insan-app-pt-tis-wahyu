import _ from "lodash";
import { AsyncStorage } from "react-native";

const defaultState = {
  spinner: false,
  successUpdateAppraisal: true,
};
export default (state = defaultState, action) => {
  const { payload } = action;
  switch (action.type) {
    case "APPRAISAL_DETAIL_REQUEST":
      return { onRequestDetailAppraisal: true };
    case "APPRAILSAL_DETAIL_REQUEST_SUCCESS":
      return { successDetailAppraisal: true, ...payload };
    case "APRAISAL_DETAIL_REQUEST_FAILED":
      return { errorDetailAppraisal: true };

    case "APPRAISAL_UPDATE_REQUEST":
      return {
        ...state,
        spinner: true,
        onRequestUpdateAppraisal: true,
      };
    case "APPRAISAL_UPDATE_REQUEST_SUCCESS":
      return {
        ...state,
        spinner: false,
        successUpdateAppraisal: true,
        ...payload,
        updateApa: action.updateApa,
      };
    case "APPRAISAL_UPDATE_REQUEST_FAILED":
      return {
        ...state,
        spinner: false,
        failedInputCarDetail: true,
      };

    case "APPRAISAL_DETAIL_SAVE_STORAGE":
      AsyncStorage.setItem(`appraisal-${state.id}`, state);
      return state;
    case "APPRAISAL_DETAIL_UPLOAD":
      return {
        ...state,
        [payload.photoCategory]: { ...state[payload.photoCategory], [payload.photoPart]: payload.photoDetail },
      };

    case "UPLOAD_PHOTO_PROGRESS":
      return {
        ...state,
        spinner: true,
        progresUpload: true,
        uploadProgress: payload.uploadProgress,
        displayUploadModal: true,
      };
    case "APPRAISAL_UPLOAD_REQUEST":
      return {
        ...state,
        onRequestUploadPhoto: true,
        message: "",
      };
    case "APPRAISAL_UPLOAD_REQUEST_SUCCESS":
      return {
        ...state,
        successUploadPhoto: true,
        spinner: false,
        progresUpload: false,
        message: "Foto Berhasil Diupload",
        ...payload,
      };
    case "APPRAISAL_UPLOAD_REQUEST_FAILED":
      return {
        ...state,
        errorUploadPhoto: true,
        spinner: false,
        progresUpload: false,
        message: "Foto Gagal Diupload",
      };

    case "APPRAISAL_UPDATE_DATA":
      return {
        ...state,
        [payload.partName]: state[payload.partName].map(
          (item, index) => (index === payload.index ? payload.data : item),
        ),
      };

    case "APPRAISAL_FINISH_REQUEST":
      return { ...state, onRequestFinishAppraisal: true };
    case "APPRAISAL_FINISH_REQUEST_SUCCESS":
      return { ...payload };
    case "APPRAISAL_FINISH_REQUEST_FAILED":
      return { errorFinishAppraisal: true };

    default:
      return state;
  }
};
