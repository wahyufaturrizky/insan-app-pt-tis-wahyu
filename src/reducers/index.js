import { combineReducers } from "redux";
import Auth from "./Auth";
import AppraisalDetail from "./AppraisalDetail";
import AppraisalData from "./ApprasialData";
import PDI from "./PDI"
import PDIData from "./PDIData"

export default combineReducers({
  Auth,
  AppraisalDetail,
  AppraisalData,
  PDI,
  PDIData
});
